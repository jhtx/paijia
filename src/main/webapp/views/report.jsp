<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (int)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>卡客途安轮卫管家</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<%=path %>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/font-awesome.css" rel="stylesheet">
<link href="<%=path %>/css/google.css" rel="stylesheet">
<link href="<%=path %>/css/style.css" rel="stylesheet" type="text/css">
<link href="<%=path %>/css/pages/signin.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="76x76" href="<%=path %>/img/logo.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=path %>/img/logo.png">
<style type="text/css">
html,body{
min-height:100%;
}
</style>
</head>
<body style="margin:0 auto;height: 100%" id="body">
<!-- header -->
<jsp:include page="header.jsp"></jsp:include>
<!-- main -->
<div class="main" id="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span12">
          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>车辆日检调查</h3>
              <button type="submit" class="btn btn-primary" id="add" style="float: right;margin-top:6px;margin-right:5px;">添加</button>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>时间</th>
                    <th>车辆</th>
                    <th class="td-actions">操作</th>
                  </tr>
                </thead>
                <tbody id="tb">
                	
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
  <br>
  <br>
  <br>
</div>
<!-- /main -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<%=path %>/js/jquery-1.7.2.min.js"></script> 
<script src="<%=path %>/js/excanvas.min.js"></script> 
<script src="<%=path %>/js/chart.min.js" type="text/javascript"></script> 
<script src="<%=path %>/js/bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="<%=path %>/js/full-calendar/fullcalendar.min.js"></script>
<script src="<%=path %>/js/base.js"></script> 
<script src="<%=path %>/js/format.js"></script> 
<script type="text/javascript">
var minheight = $(window).height();
$("#body").css({
  'min-height':minheight +'px'
})
var reports = ${reports };
console.log(reports);
if(reports.length>0){
	for(var o in reports){
		$("#tb").prepend('<tr><td>'+reports[o].time+'</td><td>'+reports[o].licenseNo+'</td><td class="td-actions"><a href="javascript:void(0);" onclick="detail('+reports[o].id+');"  class="btn btn-small btn-success">详情</a></td></tr>');
	}
}
$("#add").click(function(){
	location.href="<%=path %>/report/toadd.html"
});
function detail(id){
	location.href="<%=path %>/report/detail.html?id="+id;
}
</script>
</body>
</html>