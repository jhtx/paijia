package com.yff.tuan.mapper;

import com.yff.tuan.model.CoFleet;
import com.yff.tuan.model.CoFleetExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CoFleetMapper {
    int countByExample(CoFleetExample example);

    int deleteByExample(CoFleetExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CoFleet record);

    int insertSelective(CoFleet record);

    List<CoFleet> selectByExample(CoFleetExample example);

    CoFleet selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CoFleet record, @Param("example") CoFleetExample example);

    int updateByExample(@Param("record") CoFleet record, @Param("example") CoFleetExample example);

    int updateByPrimaryKeySelective(CoFleet record);

    int updateByPrimaryKey(CoFleet record);
}