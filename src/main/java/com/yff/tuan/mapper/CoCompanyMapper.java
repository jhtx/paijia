package com.yff.tuan.mapper;

import com.yff.tuan.model.CoCompany;
import com.yff.tuan.model.CoCompanyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CoCompanyMapper {
    int countByExample(CoCompanyExample example);

    int deleteByExample(CoCompanyExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CoCompany record);

    int insertSelective(CoCompany record);

    List<CoCompany> selectByExample(CoCompanyExample example);

    CoCompany selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CoCompany record, @Param("example") CoCompanyExample example);

    int updateByExample(@Param("record") CoCompany record, @Param("example") CoCompanyExample example);

    int updateByPrimaryKeySelective(CoCompany record);

    int updateByPrimaryKey(CoCompany record);
}