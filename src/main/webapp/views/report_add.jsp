<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (int)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>卡客途安轮卫管家</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<%=path %>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/fileinput.min.css" rel="stylesheet" type="text/css"">
<link href="<%=path %>/css/google.css" rel="stylesheet">
<link href="<%=path %>/css/style.css" rel="stylesheet" type="text/css">
<link href="<%=path %>/css/font-awesome.css" rel="stylesheet">
<link href="<%=path %>/css/bootstrap-select.min.css" rel="stylesheet">
<link href="<%=path %>/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<link rel="apple-touch-icon" sizes="76x76" href="<%=path %>/img/logo.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=path %>/img/logo.png">
<style type="text/css">
html,body{
min-height:100%;
}
.file-input{
float: left;
}
</style>
</head>
<body style="margin:0 auto;height: 100%" id="body">
<!-- header -->
<jsp:include page="header.jsp"></jsp:include>
<!-- main -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span12">
          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> 
              <i class="icon-th-list"></i>
              <h3>车辆日检调查</h3>
              <!-- <button type="submit" class="btn btn-primary" onclick="" style="float: right;margin-top:6px;margin-right:5px;">添加</button> -->
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
            	<div class="tabbable">
					<!-- <ul class="nav nav-tabs">
					  <li>
					    <a href="#formcontrols" data-toggle="tab">Form Controls</a>
					  </li>
					  <li  class="active"><a href="#jscontrols" data-toggle="tab">JS Controls</a></li>
					</ul>
					<br> -->
					<div class="tab-content">
						<form id="form01" action="<%=path%>/report/add" class="form-horizontal" method="POST" enctype="multipart/form-data">
								<br />
								<div class="control-group">											
									<label class="control-label" for="name" style="font-weight: bold;">车牌号<span style="color: red">*</span></label>
									<div class="controls">
										<select id="carId" name="carId" class="span6">
											<c:forEach var="c" items="${cars}">
												<option value="${c.id }" >${c.licenseNo }</option>
											</c:forEach>
										</select>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="no" style="font-weight: bold;">姓名<span style="color: red">*</span></label>
									<div class="controls">${user.name }</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="no" style="font-weight: bold;">手机号<span style="color: red">*</span></label>
									<div class="controls">${user.phone }</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="contactor" style="font-weight: bold;">一级标签<span style="color: red">*</span></label>
									<div class="controls">
										<select id="labelId" name="labelId" class="span6">
											<c:forEach var="c" items="${clabels}">
												<option value="${c.id }" >${c.name }</option>
											</c:forEach>
										</select>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="contactor" style="font-weight: bold;">二级标签<span style="color: red">*</span></label>
									<div class="controls">
										<select id="labelTypeId" name="labelTypeId" class="span6">
											<c:forEach var="c" items="${clabelTypes}">
												<option value="${c.id }"  >${c.name }</option>
											</c:forEach>
										</select>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="contactor" style="font-weight: bold;">描述<span style="color: red">*</span></label>
									<div class="controls">
										<textarea id="content" name="content" placeholder="限50字" cols="30" rows="4" maxlength="50"></textarea> 
										<span class="un">还可以输入<font id="xxxx">50</font>个字</span>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="no" style="font-weight: bold;">上传照片</label>
									<div class="controls span6" style="margin-left: 20px;">
												<input type="file" style="float: left;" id="myfile" name="myfile" data-ref="url" class="col-sm-10 myfile" value="" multiple/>
                                				<input type="hidden" name="url" value="">
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">	
								</div> 
								<div class="form-actions" style="text-align: center;">
									<input type="hidden" id="userId" name="userId" value="${user.id }">
									<button type="button"  id="sub" class="btn btn-primary">提交</button> 
									<button type="button" class="btn" onclick="location.href='<%=path%>/car/query.html'">取消</button>
								</div> <!-- /form-actions -->
						</form>
					</div>
				</div>
			 </div> <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
  <br>
  <br>
  <br>
</div>
<!-- /main -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<%=path %>/js/jquery-1.7.2.min.js"></script> 
<script src="<%=path %>/js/excanvas.min.js"></script>
<script src="<%=path %>/js/bootstrap.js"></script>
<script src="<%=path %>/js/base.js"></script>
<script src="<%=path %>/js/bootstrap-select.min.js"></script>
<script src="<%=path %>/js/bootstrap-datetimepicker.min.js"></script>
<script src="<%=path %>/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="<%=path %>/js/fileinput.min.js"></script>
<script src="<%=path %>/js/zh.js"></script>
<script type="text/javascript">
var minheight = $(window).height();
$("#body").css({
  'min-height':minheight +'px'
})

$('.selectpicker').selectpicker({
       'selectedText': 'cat'
});

$("#labelId").change(function(){
	$.ajax({
		   url:'<%=path%>/report/queryLabelType?labelId='+$("#labelId").val(),
		   type:'POST', //GET
		   async:true,    //或false,是否异步
		   success:function(data){
		       if(data.resCode=="0"){
		    	  $("#labelTypeId").empty();
		    	  var jarray = data.data;
		    	  for(var o in jarray){
		    		  $("#labelTypeId").append("<option value='"+jarray[o].id+"' >"+jarray[o].name+"</option>"); 
		    	  }
			   }
			},
			error : function(xhr) {
				console.log('错误');
				console.log(xhr);
			},
		});
});

$(".myfile").fileinput({
    uploadUrl:"<%=path %>/uploadFile",//上传的地址
    showUpload: false, //是否显示上传按钮,跟随文本框的那个
    showRemove : false, //显示移除按钮,跟随文本框的那个
    showCaption: false,//是否显示标题,就是那个文本框
    showPreview : true, //是否显示预览,不写默认为true
    dropZoneEnabled: false,//是否显示拖拽区域，默认不写为true，但是会占用很大区域
    autoReplace:true,
    browseClass: "btn btn-primary",
    layoutTemplates :{
        actionDelete:'', //去除上传预览的缩略图中的删除图标
        actionUpload:'',//去除上传预览缩略图中的上传图片；
        actionZoom:''   //去除上传预览缩略图中的查看详情预览的缩略图标。
    },
	maxFileCount: 1, //表示允许同时上传的最大文件个数
	enctype: 'multipart/form-data',
	validateInitialCount:true,
	previewFileIcon: "",
	previewFileIconSettings: null,
	customPreviewTags:"",
	allowedFileExtensions:"",
	msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
	allowedFileTypes: ['image'],//配置允许文件上传的类型
	allowedPreviewTypes : [ 'image' ],//配置所有的被预览文件类型
	allowedPreviewMimeTypes : [ 'jpg', 'png', 'gif' ],//控制被预览的所有mime类型
	language : 'zh'
}); 

$('textarea').each(function() {  
	var ta = $(this), p = ta.parent(), ml = parseInt(ta.attr('maxlength')),  
	v = ta.val(), h = ta.attr('placeholder');  
	if (v == h) v = '';  
	if (h && ml) {  
		//var sp = '<span style="bottom: 10px;position: absolute;right: -10px;">'+v.length+'/'+ml+'</span>';  
		p.css({'position': 'relative'});  
		//ta.before(sp);  
		ta.bind('click keyup', function() {  
			var m = $(this), v1 = m.val();  
			if (v1.length > ml) {  
				m.val(v1.substring(0, ml))  
			}  
			//m.prev().text(m.val().length + '/' + ml);  
			$("#xxxx").text(ml-m.val().length);  
		});  
	}  
}); 

$("#sub").click(function(){
	if($("#labelId").val()==""){
		alert("请选择一级标签");
		return;
	}
	if($("#labelTypeId").val()==""){
		alert("请选择二级标签");
		return;
	}
	if($("#content").val()==""){
		alert("请添加描述");
		return;
	}
	/* if($("#myfile").val()==""){
		alert("请上传图片");
		return;
	} */
	$("#form01").submit();
});
var msg = ${msg };
if(msg==true){
	alert("提交成功！");
	window.location.href="<%=path %>/report/query.html";
}
</script>
</body>
</html>