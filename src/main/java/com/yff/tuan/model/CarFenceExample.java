package com.yff.tuan.model;

import java.util.ArrayList;
import java.util.List;

public class CarFenceExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CarFenceExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Integer value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Integer value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Integer value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Integer value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Integer> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Integer> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Integer value1, Integer value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andLatIsNull() {
            addCriterion("lat is null");
            return (Criteria) this;
        }

        public Criteria andLatIsNotNull() {
            addCriterion("lat is not null");
            return (Criteria) this;
        }

        public Criteria andLatEqualTo(Double value) {
            addCriterion("lat =", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatNotEqualTo(Double value) {
            addCriterion("lat <>", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatGreaterThan(Double value) {
            addCriterion("lat >", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatGreaterThanOrEqualTo(Double value) {
            addCriterion("lat >=", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatLessThan(Double value) {
            addCriterion("lat <", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatLessThanOrEqualTo(Double value) {
            addCriterion("lat <=", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatIn(List<Double> values) {
            addCriterion("lat in", values, "lat");
            return (Criteria) this;
        }

        public Criteria andLatNotIn(List<Double> values) {
            addCriterion("lat not in", values, "lat");
            return (Criteria) this;
        }

        public Criteria andLatBetween(Double value1, Double value2) {
            addCriterion("lat between", value1, value2, "lat");
            return (Criteria) this;
        }

        public Criteria andLatNotBetween(Double value1, Double value2) {
            addCriterion("lat not between", value1, value2, "lat");
            return (Criteria) this;
        }

        public Criteria andLngIsNull() {
            addCriterion("lng is null");
            return (Criteria) this;
        }

        public Criteria andLngIsNotNull() {
            addCriterion("lng is not null");
            return (Criteria) this;
        }

        public Criteria andLngEqualTo(Double value) {
            addCriterion("lng =", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotEqualTo(Double value) {
            addCriterion("lng <>", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngGreaterThan(Double value) {
            addCriterion("lng >", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngGreaterThanOrEqualTo(Double value) {
            addCriterion("lng >=", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngLessThan(Double value) {
            addCriterion("lng <", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngLessThanOrEqualTo(Double value) {
            addCriterion("lng <=", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngIn(List<Double> values) {
            addCriterion("lng in", values, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotIn(List<Double> values) {
            addCriterion("lng not in", values, "lng");
            return (Criteria) this;
        }

        public Criteria andLngBetween(Double value1, Double value2) {
            addCriterion("lng between", value1, value2, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotBetween(Double value1, Double value2) {
            addCriterion("lng not between", value1, value2, "lng");
            return (Criteria) this;
        }

        public Criteria andStreetIsNull() {
            addCriterion("street is null");
            return (Criteria) this;
        }

        public Criteria andStreetIsNotNull() {
            addCriterion("street is not null");
            return (Criteria) this;
        }

        public Criteria andStreetEqualTo(String value) {
            addCriterion("street =", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetNotEqualTo(String value) {
            addCriterion("street <>", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetGreaterThan(String value) {
            addCriterion("street >", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetGreaterThanOrEqualTo(String value) {
            addCriterion("street >=", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetLessThan(String value) {
            addCriterion("street <", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetLessThanOrEqualTo(String value) {
            addCriterion("street <=", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetLike(String value) {
            addCriterion("street like", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetNotLike(String value) {
            addCriterion("street not like", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetIn(List<String> values) {
            addCriterion("street in", values, "street");
            return (Criteria) this;
        }

        public Criteria andStreetNotIn(List<String> values) {
            addCriterion("street not in", values, "street");
            return (Criteria) this;
        }

        public Criteria andStreetBetween(String value1, String value2) {
            addCriterion("street between", value1, value2, "street");
            return (Criteria) this;
        }

        public Criteria andStreetNotBetween(String value1, String value2) {
            addCriterion("street not between", value1, value2, "street");
            return (Criteria) this;
        }

        public Criteria andNorthIsNull() {
            addCriterion("north is null");
            return (Criteria) this;
        }

        public Criteria andNorthIsNotNull() {
            addCriterion("north is not null");
            return (Criteria) this;
        }

        public Criteria andNorthEqualTo(Double value) {
            addCriterion("north =", value, "north");
            return (Criteria) this;
        }

        public Criteria andNorthNotEqualTo(Double value) {
            addCriterion("north <>", value, "north");
            return (Criteria) this;
        }

        public Criteria andNorthGreaterThan(Double value) {
            addCriterion("north >", value, "north");
            return (Criteria) this;
        }

        public Criteria andNorthGreaterThanOrEqualTo(Double value) {
            addCriterion("north >=", value, "north");
            return (Criteria) this;
        }

        public Criteria andNorthLessThan(Double value) {
            addCriterion("north <", value, "north");
            return (Criteria) this;
        }

        public Criteria andNorthLessThanOrEqualTo(Double value) {
            addCriterion("north <=", value, "north");
            return (Criteria) this;
        }

        public Criteria andNorthIn(List<Double> values) {
            addCriterion("north in", values, "north");
            return (Criteria) this;
        }

        public Criteria andNorthNotIn(List<Double> values) {
            addCriterion("north not in", values, "north");
            return (Criteria) this;
        }

        public Criteria andNorthBetween(Double value1, Double value2) {
            addCriterion("north between", value1, value2, "north");
            return (Criteria) this;
        }

        public Criteria andNorthNotBetween(Double value1, Double value2) {
            addCriterion("north not between", value1, value2, "north");
            return (Criteria) this;
        }

        public Criteria andSouthIsNull() {
            addCriterion("south is null");
            return (Criteria) this;
        }

        public Criteria andSouthIsNotNull() {
            addCriterion("south is not null");
            return (Criteria) this;
        }

        public Criteria andSouthEqualTo(Double value) {
            addCriterion("south =", value, "south");
            return (Criteria) this;
        }

        public Criteria andSouthNotEqualTo(Double value) {
            addCriterion("south <>", value, "south");
            return (Criteria) this;
        }

        public Criteria andSouthGreaterThan(Double value) {
            addCriterion("south >", value, "south");
            return (Criteria) this;
        }

        public Criteria andSouthGreaterThanOrEqualTo(Double value) {
            addCriterion("south >=", value, "south");
            return (Criteria) this;
        }

        public Criteria andSouthLessThan(Double value) {
            addCriterion("south <", value, "south");
            return (Criteria) this;
        }

        public Criteria andSouthLessThanOrEqualTo(Double value) {
            addCriterion("south <=", value, "south");
            return (Criteria) this;
        }

        public Criteria andSouthIn(List<Double> values) {
            addCriterion("south in", values, "south");
            return (Criteria) this;
        }

        public Criteria andSouthNotIn(List<Double> values) {
            addCriterion("south not in", values, "south");
            return (Criteria) this;
        }

        public Criteria andSouthBetween(Double value1, Double value2) {
            addCriterion("south between", value1, value2, "south");
            return (Criteria) this;
        }

        public Criteria andSouthNotBetween(Double value1, Double value2) {
            addCriterion("south not between", value1, value2, "south");
            return (Criteria) this;
        }

        public Criteria andEastIsNull() {
            addCriterion("east is null");
            return (Criteria) this;
        }

        public Criteria andEastIsNotNull() {
            addCriterion("east is not null");
            return (Criteria) this;
        }

        public Criteria andEastEqualTo(Double value) {
            addCriterion("east =", value, "east");
            return (Criteria) this;
        }

        public Criteria andEastNotEqualTo(Double value) {
            addCriterion("east <>", value, "east");
            return (Criteria) this;
        }

        public Criteria andEastGreaterThan(Double value) {
            addCriterion("east >", value, "east");
            return (Criteria) this;
        }

        public Criteria andEastGreaterThanOrEqualTo(Double value) {
            addCriterion("east >=", value, "east");
            return (Criteria) this;
        }

        public Criteria andEastLessThan(Double value) {
            addCriterion("east <", value, "east");
            return (Criteria) this;
        }

        public Criteria andEastLessThanOrEqualTo(Double value) {
            addCriterion("east <=", value, "east");
            return (Criteria) this;
        }

        public Criteria andEastIn(List<Double> values) {
            addCriterion("east in", values, "east");
            return (Criteria) this;
        }

        public Criteria andEastNotIn(List<Double> values) {
            addCriterion("east not in", values, "east");
            return (Criteria) this;
        }

        public Criteria andEastBetween(Double value1, Double value2) {
            addCriterion("east between", value1, value2, "east");
            return (Criteria) this;
        }

        public Criteria andEastNotBetween(Double value1, Double value2) {
            addCriterion("east not between", value1, value2, "east");
            return (Criteria) this;
        }

        public Criteria andWestIsNull() {
            addCriterion("west is null");
            return (Criteria) this;
        }

        public Criteria andWestIsNotNull() {
            addCriterion("west is not null");
            return (Criteria) this;
        }

        public Criteria andWestEqualTo(Double value) {
            addCriterion("west =", value, "west");
            return (Criteria) this;
        }

        public Criteria andWestNotEqualTo(Double value) {
            addCriterion("west <>", value, "west");
            return (Criteria) this;
        }

        public Criteria andWestGreaterThan(Double value) {
            addCriterion("west >", value, "west");
            return (Criteria) this;
        }

        public Criteria andWestGreaterThanOrEqualTo(Double value) {
            addCriterion("west >=", value, "west");
            return (Criteria) this;
        }

        public Criteria andWestLessThan(Double value) {
            addCriterion("west <", value, "west");
            return (Criteria) this;
        }

        public Criteria andWestLessThanOrEqualTo(Double value) {
            addCriterion("west <=", value, "west");
            return (Criteria) this;
        }

        public Criteria andWestIn(List<Double> values) {
            addCriterion("west in", values, "west");
            return (Criteria) this;
        }

        public Criteria andWestNotIn(List<Double> values) {
            addCriterion("west not in", values, "west");
            return (Criteria) this;
        }

        public Criteria andWestBetween(Double value1, Double value2) {
            addCriterion("west between", value1, value2, "west");
            return (Criteria) this;
        }

        public Criteria andWestNotBetween(Double value1, Double value2) {
            addCriterion("west not between", value1, value2, "west");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}