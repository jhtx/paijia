package com.yff.tuan.model;

import java.util.Date;

public class TireCheck {
    private Integer id;

    private String productId;

    private String tyreNo;

    private Double pressure;

    private Double temperature;

    private Integer pressureWarning;

    private Integer temperatureWarning;

    private Integer airWarning;

    private Integer signalWarning;

    private Date checkTime;
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTyreNo() {
        return tyreNo;
    }

    public void setTyreNo(String tyreNo) {
        this.tyreNo = tyreNo;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Integer getPressureWarning() {
        return pressureWarning;
    }

    public void setPressureWarning(Integer pressureWarning) {
        this.pressureWarning = pressureWarning;
    }

    public Integer getTemperatureWarning() {
        return temperatureWarning;
    }

    public void setTemperatureWarning(Integer temperatureWarning) {
        this.temperatureWarning = temperatureWarning;
    }

    public Integer getAirWarning() {
        return airWarning;
    }

    public void setAirWarning(Integer airWarning) {
        this.airWarning = airWarning;
    }

    public Integer getSignalWarning() {
        return signalWarning;
    }

    public void setSignalWarning(Integer signalWarning) {
        this.signalWarning = signalWarning;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    
}