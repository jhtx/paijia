package com.yff.tuan.mapper;

import com.yff.tuan.model.CoPosition;
import com.yff.tuan.model.CoPositionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CoPositionMapper {
    int countByExample(CoPositionExample example);

    int deleteByExample(CoPositionExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CoPosition record);

    int insertSelective(CoPosition record);

    List<CoPosition> selectByExample(CoPositionExample example);

    CoPosition selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CoPosition record, @Param("example") CoPositionExample example);

    int updateByExample(@Param("record") CoPosition record, @Param("example") CoPositionExample example);

    int updateByPrimaryKeySelective(CoPosition record);

    int updateByPrimaryKey(CoPosition record);
}