<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (int)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>卡客途安轮卫管家</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<%=path %>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/font-awesome.css" rel="stylesheet">
<link href="<%=path %>/css/google.css" rel="stylesheet">
<link href="<%=path %>/css/style.css" rel="stylesheet" type="text/css">
<link href="<%=path %>/css/pages/signin.css" rel="stylesheet" type="text/css">
<link href="<%=path %>/css/reveal.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="76x76" href="<%=path %>/img/logo.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=path %>/img/logo.png">
<style type="text/css">
html,body{
min-height:100%;
}
</style>
</head>
<body style="margin:0 auto;height: 100%" id="body">
<!-- header -->
<jsp:include page="header.jsp"></jsp:include>
<!-- main -->
<div class="main" id="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span12">
          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>车辆日检故障汇总</h3>
              <button type="submit" class="btn btn-primary" id="go_back" style="float: right;margin-top:6px;margin-right:5px;">返回</button>
           	<button type="submit" class="btn btn-primary" onclick="jQuery.print('#content')" style="float: right;margin-top:6px;margin-right:5px;">打印</button>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
            	<form action="<%=path %>/data/report/query" method="post" id="form01">
            		<span style="float: left;margin-top: 9px;margin-left: 10px;">车队:</span>
       				<select id="fleetId" name="fleetId" class="span2" style="float: left;;margin-top: 6px;width: 200px;margin-left: 10px;">
       					<c:forEach var="f" items="${fleets }">
       						<option value="${f.id }" <c:if test="${fleetId==f.id }">selected</c:if>>${f.name }</option>
       					</c:forEach>
       				</select>
       				<input type="hidden" name="id" value="${id }">
       			</form>
       			<button type="submit" class="btn btn-primary" id="qy" style="float: left;margin-left:20px;margin-top: -12px;">查询</button>
            </div>
            <div class="widget-content" id="content">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th style="text-align: center;font-size: 20px;" colspan="7"><span style="float: left;font-size: 10px;font-weight: normal;">${checkDate }</span>车辆日检故障汇总</th>
                  </tr>
                  <tr>
                    <th width="12%">车牌号</th>
                    <th width="12%">姓名</th>
                    <th width="10%">手机号</th>
                    <th width="15%">一级标签</th>
                    <th width="15%">二级标签</th>
                    <th width="28%">内容</th>
                    <th width="10%">图片</th>
                  </tr>
                </thead>
                <tbody id="tb">
                	<c:forEach items="${reports }" var="r" varStatus="status">
                	<tr style="font-size: 8px;">
	                    <td>${r.licenseNo }</<td>
	                    <td>${r.userName }</<td>
	                    <td>${r.userPhone }</<td>
	                    <td>${r.labelName }</<td>
	                    <td>${r.labelTypeName }</<td>
	                    <td>${r.content }</<td>
	                    <td>
	                    	<c:if test="${!empty r.filePath}">
	                    		<a href="#" class="big-link" onclick="show('${status.index+1}')" data-reveal-id="myModal" data-animation="none">图片</a>
	                    	</c:if>
	                    	<input type="hidden" id="fp${status.index+1}" value="${r.filePath }">
	                    </td>
                   </tr>
                   </c:forEach>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <div id="myModal" class="reveal-modal">
			<img width="400px;" height="300px" id="mg" src="data:image/jpeg;base64,${report.filePath }">
			<a class="close-reveal-modal">&#215;</a>
	</div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
  <br>
  <br>
  <br>
</div>
<!-- /main -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<%=path %>/js/jquery-1.7.2.min.js"></script> 
<script src="<%=path %>/js/excanvas.min.js"></script> 
<script src="<%=path %>/js/chart.min.js" type="text/javascript"></script> 
<script src="<%=path %>/js/bootstrap.js"></script>
<script src="<%=path %>/js/base.js"></script> 
<script src="<%=path %>/js/jquery.reveal.js"></script>
<script src="<%=path %>/js/html5-3.6-respond-1.1.0.min.js"></script>
<script src="<%=path %>/js/jQuery.print.js"></script>
<script type="text/javascript">
var minheight = $(window).height();
$("#body").css({
  'min-height':minheight +'px'
})
$("#go_back").click(function(){
	window.location.href="<%=path %>/data/report";
});
$("#print").click(function(){
        //var a = $('a')[0];
        var a = $("<a href='<%=path %>/data/report/print' target='_blank'>打印</a>").get(0);
        var e = document.createEvent('MouseEvents');
        e.initEvent( 'click', true, true );
        a.dispatchEvent(e);
});
$("#qy").click(function(){
       $("#form01").submit();
});
function show(id){
	$("#mg").attr('src',"data:image/jpeg;base64,"+$("#fp"+id).val()); 
}
</script>
</body>
</html>