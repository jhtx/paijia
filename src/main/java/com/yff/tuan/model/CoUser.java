package com.yff.tuan.model;

import java.util.Date;

public class CoUser {
    private Integer id;

    private String name;

    private String password;

    private String nickName;

    private String no;

    private String address;

    private String contactor;

    private String phone;

    private String idcard;

    private Integer positionId;
    
    private String positionName;

    private Integer role;

    private Integer companyId;
    
    private String companyName;

    private Integer fleetId;
    
    private String fleetName;

    private Integer parentId;

    private Integer warn;

    private Date createDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactor() {
        return contactor;
    }

    public void setContactor(String contactor) {
        this.contactor = contactor;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public Integer getPositionId() {
        return positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getFleetId() {
        return fleetId;
    }

    public void setFleetId(Integer fleetId) {
        this.fleetId = fleetId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getWarn() {
        return warn;
    }

    public void setWarn(Integer warn) {
        this.warn = warn;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

	public String getPositionName() {
		return positionName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public String getFleetName() {
		return fleetName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setFleetName(String fleetName) {
		this.fleetName = fleetName;
	}
    
    
}