package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.CarMapper;
import com.yff.tuan.mapper.CoRoleMapper;
import com.yff.tuan.mapper.CmodelMapper;
import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarExample;
import com.yff.tuan.model.CarExample.Criteria;
import com.yff.tuan.model.CoRole;
import com.yff.tuan.model.CoRoleExample;
import com.yff.tuan.model.Cmodel;
import com.yff.tuan.model.CmodelExample;
import com.yff.tuan.service.BrandService;
import com.yff.tuan.service.CarService;
import com.yff.tuan.service.ModelService;
import com.yff.tuan.service.RoleService;
import com.yff.tuan.util.Page;

@Component
public class CoRoleServiceImpl implements RoleService {
	@Autowired
	CoRoleMapper coRoleMapper;
	

	@Override
	public List<CoRole> query(CoRoleExample example) {
		return coRoleMapper.selectByExample(example);
	}


}
