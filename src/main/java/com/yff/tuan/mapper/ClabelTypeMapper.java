package com.yff.tuan.mapper;

import com.yff.tuan.model.ClabelType;
import com.yff.tuan.model.ClabelTypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ClabelTypeMapper {
    int countByExample(ClabelTypeExample example);

    int deleteByExample(ClabelTypeExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ClabelType record);

    int insertSelective(ClabelType record);

    List<ClabelType> selectByExample(ClabelTypeExample example);

    ClabelType selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ClabelType record, @Param("example") ClabelTypeExample example);

    int updateByExample(@Param("record") ClabelType record, @Param("example") ClabelTypeExample example);

    int updateByPrimaryKeySelective(ClabelType record);

    int updateByPrimaryKey(ClabelType record);
}