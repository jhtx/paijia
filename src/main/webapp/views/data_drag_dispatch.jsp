<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (int)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>卡客途安轮卫管家</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<%=path %>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/font-awesome.css" rel="stylesheet">
<link href="<%=path %>/css/google.css" rel="stylesheet">
<link href="<%=path %>/css/style.css" rel="stylesheet" type="text/css">
<link href="<%=path %>/css/pages/signin.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="76x76" href="<%=path %>/img/logo.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=path %>/img/logo.png">
<style type="text/css">
html,body{
min-height:100%;
}
</style>
</head>
<body style="margin:0 auto;height: 100%" id="body">
<!-- header -->
<jsp:include page="header.jsp"></jsp:include>
<!-- main -->
<div class="main" id="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span12">
          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>车辆</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                  	<th>状态</th>
                    <th>车牌号</th>
                    <th>挂车匹配</th>
                  </tr>
                </thead>
                <tbody>
                	<form id="form01" class="form-horizontal" method="POST">
                	<c:forEach items="${cars }" var="c">
                	<tr>
                		<td><c:if test="${c.state==0 }">围栏中</c:if>
	                    	<c:if test="${c.state==1 }">行驶中</c:if>
	                    	<c:if test="${c.state==2 }">静止</c:if>
	                    </td>
	                    <td>${c.licenseNo }</td>
	                    <td>
	                    	<c:if test="${c.matchCarId==0 }">未匹配</c:if>
	                    	<c:if test="${c.matchCarId!=0 }">${c.matchLicenseNo }</c:if>
	                    </td>
                  	</tr>
                	</c:forEach>
                	<input type="hidden" name="id" id="id" value="">
                	</form>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
  <br>
  <br>
  <br>
</div>
<!-- /main -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<%=path %>/js/jquery-1.7.2.min.js"></script> 
<script src="<%=path %>/js/excanvas.min.js"></script> 
<script src="<%=path %>/js/chart.min.js" type="text/javascript"></script> 
<script src="<%=path %>/js/bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="<%=path %>/js/full-calendar/fullcalendar.min.js"></script>
<script src="<%=path %>/js/base.js"></script> 
<script type="text/javascript">
var minheight = $(window).height();
$("#body").css({
  'min-height':minheight +'px'
})
$("#add").click(function(){
	location.href="<%=path %>/car/toadd.html"
});
function detail(id){
	$("#id").val(id);
	$("#form01").attr('action',"<%=path %>/check/detail.html");
	$("#form01").submit();
}
function update(id){
	$("#id").val(id);
	$("#form01").attr('action',"<%=path %>/car/toadd.html");
	$("#form01").submit();
}
function del(id){
	if(confirm("确定删除？")){
		$.ajax({
		    url:'<%=path %>/car/del.html',
		    type:'POST', //GET
		    async:true,    //或false,是否异步
		    data: {id:id},
		    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
		    success:function(data){
		        if(data.resCode=="0"){
		        	alert("删除成功！");
		        	window.location.href="<%=path %>/car/query.html";
		        }else{
		        	 $("#msg").html(data.resMsg);
		        }
		    },
		    error:function(xhr){},
		});
	}
}
</script>
</body>
</html>