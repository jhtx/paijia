<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (int)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>卡客途安轮卫管家</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<%=path %>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/font-awesome.css" rel="stylesheet">
<link href="<%=path %>/css/google.css" rel="stylesheet">
<link href="<%=path %>/css/style.css" rel="stylesheet" type="text/css">
<link href="<%=path %>/css/pages/signin.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="76x76" href="<%=path %>/img/logo.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=path %>/img/logo.png">
<style type="text/css">
html,body{
min-height:100%;
}
</style>
</head>
<body style="margin:0 auto;height: 100%" id="body">
<!-- header -->
<jsp:include page="header.jsp"></jsp:include>
<!-- main -->
<div class="main" id="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span12">
          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>一体车里程查询</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
            <form id="form01" class="form-horizontal" method="POST">
            <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <td>
                    	<div style="float: left">开始时间:<input type="text" class="span2" id="startDate" name="startDate" value='<fmt:formatDate value="${startDate }" pattern="yyyy-MM-dd"/>' >&nbsp;&nbsp;</div>
              			<div style="float: left">结束时间:<input type="text" class="span2" id="endDate" name="endDate" value='<fmt:formatDate value="${endDate }" pattern="yyyy-MM-dd"/>' ></div>
              			<div style="float: left">&nbsp;车队:
              				<select id="fleetId" name="fleetId" class="span2">
              					<option value="">全部</option>
              					<c:forEach var="f" items="${fleets }">
              						<option value="${f.id }" <c:if test="${fleetId==f.id }">selected</c:if>>${f.name }</option>
              					</c:forEach>
              				</select>
              			</div>
              			<div style="float: left">&nbsp;车牌号:<input type="text" style="width: 80px" id="licenseNo" name="licenseNo" value="${licenseNo }" ></div>
              			<div style="float: left">&nbsp;车辆类型:
              				<select id="typeSizeId" name="typeSizeId" class="span2">
              					<option value="">全部</option>
              					<c:forEach var="t" items="${typeSizes }">
              						<option value="${t.id }" <c:if test="${typeSizeId==t.id }">selected</c:if> >${t.name }</option>
              					</c:forEach>
              				</select>
              			</div>
              			<button type="submit" class="btn btn-primary" id="qy" style="float: left;margin-left:20px;">查询</button>
              		 </td>
                    </tr>
                </thead>
             </table>
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>车牌号</th>
                    <th>车辆类型</th>
                    <th>总行驶里程</th>
                  </tr>
                </thead>
                <tbody>
                	
                	<c:forEach items="${matches }" var="m">
	                	<tr>
		                    <td>${m.licenseNo }</td>
		                    <td>${m.typName }</td>
		                    <td>${m.miles }</td>
	                  	</tr>
                	</c:forEach>
                	<tr >
	                    <td colspan="2" style="text-align: right;border-bottom:  1px solid #dddddd ;">里程合计</td>
	                    <td  colspan="2" style="border-bottom:  1px solid #dddddd ;">${miles }</td>
                  	</tr>
                	<input type="hidden" name="productId" id="productId" value="">
                	
                </tbody>
              </table>
              </form>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
  <br>
  <br>
  <br>
</div>
<!-- /main -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<%=path %>/js/jquery-1.7.2.min.js"></script> 
<script src="<%=path %>/js/excanvas.min.js"></script> 
<script src="<%=path %>/js/chart.min.js" type="text/javascript"></script> 
<script src="<%=path %>/js/bootstrap.js"></script>
<script src="<%=path %>/js/bootstrap-datetimepicker.min.js"></script>
<script src="<%=path %>/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script language="javascript" type="text/javascript" src="<%=path %>/js/full-calendar/fullcalendar.min.js"></script>
<script src="<%=path %>/js/base.js"></script> 
<script type="text/javascript">
var minheight = $(window).height();
$("#body").css({
  'min-height':minheight +'px'
});
$('#startDate').datetimepicker({  
	format: 'yyyy-mm-dd',  
    autoclose: true,
    todayBtn: true,
    language:'zh-CN',
    minView: "month"
}); 
$('#endDate').datetimepicker({  
	format: 'yyyy-mm-dd',  
    autoclose: true,
    todayBtn: true,
    language:'zh-CN',
    minView: "month"
}); 
$("#qy").click(function(){
	$("#productId").val(id);
	$("#form01").attr('action',"<%=path %>/data/whole/query.html");
	$("#form01").submit();
});
function detail(id){
	$("#productId").val(id);
	$("#form01").attr('action',"<%=path %>/data/whole/detail.html");
	$("#form01").submit();
}

</script>
</body>
</html>