<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>卡客途安轮卫管家</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<%=path %>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/font-awesome.css" rel="stylesheet">
<link href="<%=path %>/css/google.css" rel="stylesheet">
<link href="<%=path %>/css/style.css" rel="stylesheet" type="text/css">
<link href="<%=path %>/css/pages/signin.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="76x76" href="<%=path %>/img/logo.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=path %>/img/logo.png">
</head>
<body>
	<!-- <div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> 
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span>
				</a> 
				<a class="brand" href="index.html"> Bootstrap Admin Template </a>
				<div class="nav-collapse">
					<ul class="nav pull-right">
						<li class="">
							<a href="signup.html" class=""> Don't havean account? </a>
						</li>
						<li class="">
							<a href="index.html" class=""><i class="icon-chevron-left"></i>Back to Homepage</a>
						</li>
					</ul>
				</div>
				/.nav-collapse
			</div>
			/container
		</div>
		/navbar-inner
	</div> -->
	
	<!-- /navbar -->
	<div class="account-container" style="margin-top:200px;">
		<div class="content clearfix">
			<form action="<%=path %>/login.html" method="post">
				<h2 align="center" style="margin: 20px auto 50px auto;"><%-- <img style="max-width: 60px;" src="<%=path %>/img/logo.png" alt="派迦科技"> --%>卡客途安轮卫管家</h2>
				<div class="login-fields">
					<div class="field">
							<input class="form-control" type="text" name="name" id="name" placeholder="请输入用户名">
					</div>
					<!-- /field -->
					<div class="field">
						<div class="input-group">
							<input class="form-control" type="password" name="password" id="password" placeholder="请输入密码">
						</div>
					</div>
					<!-- /password -->
				</div>
				<!-- /login-fields -->
				<div class="login-actions" style="text-align: center">
					<label class="choice" style="color: red">${msg }</label>
					<button class="button btn btn-success btn-large" >登陆</button>
				</div>
				<!-- .actions -->
			</form>
		</div>
		<!-- /content -->
	</div>
	<!-- /account-container -->
	<!-- <div class="login-extra">
		<a href="#">Reset Password</a>
	</div> -->
	<!-- /login-extra -->
	<script src="<%=path %>/js/jquery-1.7.2.min.js"></script>
	<script src="<%=path %>/js/bootstrap.js"></script>
	<script src="<%=path %>/js/signin.js"></script>
</body>
</html>