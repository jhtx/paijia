package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.CarWarning;

public interface WarningService {
	public abstract CarWarning findCarWarning(List<Integer> carIds);
	
	public abstract List<CarWarning> queryCarWarning(List<Integer> carIds);
	
	public abstract boolean insert(CarWarning carBind);
	
	public abstract int updateCarWarning(CarWarning carBind);
	
	public abstract boolean update(Integer id);
	
}
