package com.yff.tuan.util;

import java.text.DecimalFormat;

public class MathUtil {
	public static  double parseDouble(double d){
		DecimalFormat df=new DecimalFormat(".##");
		return Double.valueOf(df.format(d));
	}
}
