<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (int)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>卡客途安轮卫管家</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<%=path %>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/google.css" rel="stylesheet">
<link href="<%=path %>/css/style.css" rel="stylesheet" type="text/css">
<link href="<%=path %>/css/font-awesome.css" rel="stylesheet">
<link href="<%=path %>/css/bootstrap-select.min.css" rel="stylesheet">
<link href="<%=path %>/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<link rel="apple-touch-icon" sizes="76x76" href="<%=path %>/img/logo.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=path %>/img/logo.png">
<style type="text/css">
html,body{
min-height:100%;
}
</style>
</head>
<body style="margin:0 auto;height: 100%" id="body">
<!-- header -->
<jsp:include page="header.jsp"></jsp:include>
<!-- main -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span12">
          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> 
              <i class="icon-th-list"></i>
              <h3>车队</h3>
              <!-- <button type="submit" class="btn btn-primary" onclick="" style="float: right;margin-top:6px;margin-right:5px;">添加</button> -->
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
            	<div class="tabbable">
					<!-- <ul class="nav nav-tabs">
					  <li>
					    <a href="#formcontrols" data-toggle="tab">Form Controls</a>
					  </li>
					  <li  class="active"><a href="#jscontrols" data-toggle="tab">JS Controls</a></li>
					</ul>
					<br> -->
					<div class="tab-content">
						<form id="form01" class="form-horizontal" method="POST">
								<br />
								<div class="control-group">											
									<label class="control-label" for="name">车辆编号<span style="color: red">*</span></label>
									<div class="controls">
										<input type="text" class="span6" id="no" name="no" value="${car.no }">
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="no">车队<span style="color: red">*</span></label>
									<div class="controls">
										<select name="fleetId" id="fleetId" class="span6">
											<option value="">请选择</option>
											<c:forEach items="${fleets }" var="f">
												<option value="${f.id }"  <c:if test="${f.id==car.fleetId }">selected</c:if> >${f.name }</option>
											</c:forEach>
										</select>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="no">车牌号<span style="color: red">*</span></label>
									<div class="controls">
										<input type="text" class="span6" id="licenseNo" name="licenseNo" value="${car.licenseNo }">
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<%-- <div class="control-group">											
									<label class="control-label" for="type">车辆类型</label>
									<div class="controls">
										<select name="typeSize" id="typeSize" class="span6">
											<c:forEach items="${typeSizes }" var="t">
												<option value="${t.id }" <c:if test="${car.typeSizeId==t.id }">selected</c:if> >${t.name }</option>
											</c:forEach>
										</select>
									</div> <!-- /controls -->				
								</div> <!-- /control-group --> --%>
								<div class="control-group">											
									<label class="control-label" for="contactor">车辆类型<span style="color: red">*</span></label>
									<div class="controls">
										<c:choose>
											<c:when test="${!empty car.typeSizeId }">
													<c:forEach var="t" items="${typeSizes}">
														<c:if test="${car.typeSizeId==t.id }">${t.name }</c:if>
													</c:forEach>
													<input type="hidden" class="span6" id="typeSize" name="typeSize" value="${car.typeSizeId }">
											</c:when>
											<c:otherwise>
												<select id="typeSize" name="typeSize" class="span6">
													<c:forEach var="t" items="${typeSizes}">
														<option value="${t.id }" <c:if test="${car.typeSizeId==t.id }">selected</c:if> >${t.name }</option>
													</c:forEach>
												</select>
											</c:otherwise>
										</c:choose>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="contactor">车辆品牌<span style="color: red">*</span></label>
									<div class="controls">
										<select id="brand" name="brand" class="span6">
											<c:forEach var="b" items="${brands}">
												<option value="${b.id }" <c:if test="${car.brandId==b.id }">selected</c:if> >${b.name }</option>
											</c:forEach>
										</select>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="contactor">车辆型号<span style="color: red">*</span></label>
									<div class="controls">
										<input type="hidden" id="cmid" value='${car.modelId}'>
										<select id="model" name="model" class="span6">
											<c:forEach var="m" items="${models}">
												<option value="${m.id }" <c:if test="${car.modelId==m.id }">selected</c:if> >${m.name }</option>
											</c:forEach>
										</select>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="no">已行驶里程<span style="color: red">*</span></label>
									<div class="controls">
										<input type="text" class="span6" id="miles" name="miles" value="${car.miles }">
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="contactor">第一轴轮胎规格<span style="color: red">*</span></label>
									<div class="controls">
										<select id="frTyreSize" name="frTyreSize" class="selectpicker bla bla bli span6"  multiple data-live-search="true" title="请选择">
											<c:forEach var="m" items="${tyreSizes}">
												<option value="${m.id }">${m.name }</option>
											</c:forEach>
										</select>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="contactor">第二轴轮胎规格<span style="color: red">*</span></label>
									<div class="controls">
										<select id="seTyreSize" name="seTyreSize" class="selectpicker bla bla bli span6"  multiple data-live-search="true" title="请选择">
											<c:forEach var="m" items="${tyreSizes}">
												<option value="${m.id }">${m.name }</option>
											</c:forEach>
										</select>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="contactor">第三轴轮胎规格<span style="color: red">*</span></label>
									<div class="controls">
										<select id="thTyreSize" name="thTyreSize" class="selectpicker bla bla bli span6"  multiple data-live-search="true" title="请选择">
											<c:forEach var="m" items="${tyreSizes}">
												<option value="${m.id }">${m.name }</option>
											</c:forEach>
										</select>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								
								
								<div class="control-group">											
									<label class="control-label" for="phone">上牌日期<span style="color: red">*</span></label>
									<div class="controls">
										<input type="text" class="span6" id="licensingDate" name="licensingDate" value='<fmt:formatDate value="${car.licensingDate }" pattern="yyyy-MM-dd"/>' >
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="phone">保险续费日期<span style="color: red">*</span></label>
									<div class="controls">
										<input type="text" class="span6" id="insureDate" name="insureDate" value='<fmt:formatDate value="${car.insureDate }" pattern="yyyy-MM-dd"/>' >
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="phone">年检日期<span style="color: red">*</span></label>
									<div class="controls">
										<input type="text" class="span6" id="checkDate" name="checkDate" value='<fmt:formatDate value="${car.checkDate }" pattern="yyyy-MM-dd"/>' >
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="phone">报废日期<span style="color: red">*</span></label>
									<div class="controls">
										<input type="text" class="span6" id="scrapDate" name="scrapDate" value='<fmt:formatDate value="${car.scrapDate }" pattern="yyyy-MM-dd"/>' >
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">		
									<label class="control-label" for="type">状态</label>
									<div class="controls">
										<select name="status" id="status" class="span6">
											<option value="1" <c:if test="${car.status==1 }">selected</c:if> >营运</option>
											<option value="2" <c:if test="${car.status==2 }">selected</c:if> >停驶</option>
											<option value="3" <c:if test="${car.status==3 }">selected</c:if> >已报废</option>
											<option value="3" <c:if test="${car.status==4 }">selected</c:if> >待出售</option>
											<option value="3" <c:if test="${car.status==5 }">selected</c:if> >出租</option>
											<option value="3" <c:if test="${car.status==6 }">selected</c:if> >其他</option>
										</select>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">										
									<label class="control-label" for="type">产权归属</label>
									<div class="controls">
										<select name="owner" id="owner" class="span6">
											<option value="1" <c:if test="${car.owner==1 }">selected</c:if> >外调</option>
											<option value="2" <c:if test="${car.owner==2 }">selected</c:if> >自有</option>
											<option value="3" <c:if test="${car.owner==3 }">selected</c:if> >租赁</option>
											<option value="3" <c:if test="${car.owner==3 }">selected</c:if> >其他</option>
										</select>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="phone">备注</label>
									<div class="controls">
										<input type="text" class="span6" id="remark" name="remark" value="${car.remark }">
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">	
									${msg }										
								</div> 
								<div class="form-actions" style="text-align: center;">
									<input type="hidden" id="carId" name="carId" value="${car.id }">
									<input type="hidden" id="id" name="id" value="${fleet.id }">
									<button type="button"  id="sub" class="btn btn-primary">提交</button> 
									<button type="button" class="btn" onclick="location.href='<%=path%>/car/query.html'">取消</button>
								</div> <!-- /form-actions -->
						</form>
					</div>
				</div>
			 </div> <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
  <br>
  <br>
  <br>
</div>
<!-- /main -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<%=path %>/js/jquery-1.7.2.min.js"></script> 
<script src="<%=path %>/js/excanvas.min.js"></script>
<script src="<%=path %>/js/bootstrap.js"></script>
<script src="<%=path %>/js/base.js"></script>
<script src="<%=path %>/js/bootstrap-select.min.js"></script>
<script src="<%=path %>/js/bootstrap-datetimepicker.min.js"></script>
<script src="<%=path %>/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script type="text/javascript">
var minheight = $(window).height();
$("#body").css({
  'min-height':minheight +'px'
})

$('.selectpicker').selectpicker({
       'selectedText': 'cat'
});

$("#brand").change(function(){
	$.ajax({
		   url:'<%=path%>/car/queryModel?brandId='+$("#brand").val(),
		   type:'GET', //GET
		   async:true,    //或false,是否异步
		   success:function(data){
		       if(data.resCode=="0"){
		    	  $("#model").empty();
		    	  var jarray = data.data;
		    	  for(var o in jarray){
		    		  var str="";
		    		  if(jarray[o].id == $("#cmid").val()){
		    			  str = "selected";
		    		  }
		    		  $("#model").append("<option value='"+jarray[o].id+"' "+str+" >"+jarray[o].name+"</option>"); 
		    	  }
			   }
			},
			error : function(xhr) {
				console.log('错误');
				console.log(xhr);
			},
		});
});


var carFiTyres = ${car.carFiTyreJson };
var carFiTyresArray = new Array();
$.each(carFiTyres, function (i) {
	carFiTyresArray.push(carFiTyres[i].tyreId);
});
$('#frTyreSize').selectpicker('val', carFiTyresArray);//默认选中
$('#frTyreSize').selectpicker('refresh');


var carSeTyres = ${car.carSeTyreJson };
var carSeTyresArray = new Array();
$.each(carSeTyres, function (i) {
	carSeTyresArray.push(carSeTyres[i].tyreId);
});
$('#seTyreSize').selectpicker('val', carSeTyresArray);//默认选中
$('#seTyreSize').selectpicker('refresh');



var carThTyres = ${car.carThTyreJson };
var carThTyresArray = new Array();
$.each(carThTyres, function (i) {
	carThTyresArray.push(carThTyres[i].tyreId);
});
$('#thTyreSize').selectpicker('val', carThTyresArray);//默认选中
$('#thTyreSize').selectpicker('refresh');

$('#licensingDate').datetimepicker({  
	format: 'yyyy-mm-dd',  
    autoclose: true,
    todayBtn: true,
    language:'zh-CN',
    minView: "month"
});  
$('#insureDate').datetimepicker({  
    format: 'yyyy-mm-dd',  
    autoclose: true,
    todayBtn: true,
    language:'zh-CN',
    minView: "month"
});  
$('#checkDate').datetimepicker({  
	format: 'yyyy-mm-dd',  
    autoclose: true,
    todayBtn: true,
    language:'zh-CN',
    minView: "month"
});
$('#scrapDate').datetimepicker({  
	format: 'yyyy-mm-dd',  
    autoclose: true,
    todayBtn: true,
    language:'zh-CN',
    minView: "month"
});
$("#sub").click(function(){
	if(""== $("#no").val()){
		alert("请输入车辆编号");
		return;
	}
	if(""== $("#fleetId").val()){
		alert("请选择车队");
		return;
	}
	if(""== $("#licenseNo").val()){
		alert("请输入车牌号");
		return;
	}
	if(""== $("#fleetId").val()){
		alert("请选择第一轴轮胎规格");
		return;
	}
	if(""== $("#typeSize").val()){
		alert("请选择车辆类型");
		return;
	}
	if(""== $("#brand").val()){
		alert("请选择车辆品牌");
		return;
	}
	if(""== $("#model").val()){
		alert("请选择车辆型号");
		return;
	}
	if(""== $("#miles").val()){
		alert("请输入已行驶里程");
		return;
	}
	
	var frTyreSize = $("#frTyreSize").val();
	if(null == frTyreSize){
		alert("请选择第一轴轮胎规格");
		return;
	}
	var frTyreSizeObj = [];
	for(var i=0;i<frTyreSize.length;i++){
		var obj = {tyreId:frTyreSize[i]};
		frTyreSizeObj.push(obj);
	}
	var seTyreSize = $("#seTyreSize").val();
	if(null == seTyreSize){
		alert("请选择第二轴轮胎规格");
		return;
	}
	var seTyreSizeObj = [];
	for(var i=0;i<seTyreSize.length;i++){
		var obj = {tyreId:seTyreSize[i]};
		seTyreSizeObj.push(obj);
	}
	var thTyreSize = $("#thTyreSize").val();
	if(null == thTyreSize){
		alert("请选择第三轴轮胎规格");
		return;
	}
	var thTyreSizeObj = [];
	for(var i=0;i<thTyreSize.length;i++){
		var obj = {tyreId:thTyreSize[i]};
		thTyreSizeObj.push(obj);
	}
	
	if(""== $("#licensingDate").val()){
		alert("请选择上牌日期");
		return;
	}
	if(""== $("#insureDate").val()){
		alert("请选择保险续费日期");
		return;
	}
	if(""== $("#checkDate").val()){
		alert("请选择年检日期");
		return;
	}
	if(""== $("#scrapDate").val()){
		alert("请选择报废日期");
		return;
	}
	
	
	
		var car ={	
					id:$("#carId").val(),
					no: $("#no").val(),
					type:$("#type").val(),
					licenseNo:$("#licenseNo").val(),
					fleetId:$("#fleetId").val(),
					typeSizeId:$("#typeSize").val(),
					brandId:$("#brand").val(),
					modelId:$("#model").val(),
					miles:$("#miles").val(),
					licensingDate:$("#licensingDate").val(),
					insureDate:$("#insureDate").val(),
					checkDate:$("#checkDate").val(),
					scrapDate:$("#scrapDate").val(),
					status:$("#status").val(),
					owner:$("#owner").val(),
					remark:$("#remark").val(),
				    carFiTyres:frTyreSizeObj,
				    carSeTyres:seTyreSizeObj,
				    carThTyres:thTyreSizeObj
				};
				console.log(JSON.stringify(car));
				$.ajax({
				   url:'<%=path%>/car/add',
				   type:'POST', //GET
				   async:true,    //或false,是否异步
				   contentType:"application/json",
				   data:JSON.stringify(car),
				   dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				   success:function(data){
				       console.log(data);
				       if(data.add==true){
				    	if($("#carId").val() == ""){
				    		alert("添加成功！");
				    	}else{
				    		alert("修改成功！");
				    	}
				       	window.location.href="<%=path%>/car/query.html";
					   }
					},
					error : function(xhr) {
						console.log('错误');
						console.log(xhr);
					},
				});
});
</script>
</body>
</html>