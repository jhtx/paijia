package com.yff.tuan.model;

public class CoCompany {
    private Integer id;

    private String name;

    private String no;

    private String address;

    private String contactor;

    private String phone;

    private Integer coUserId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactor() {
        return contactor;
    }

    public void setContactor(String contactor) {
        this.contactor = contactor;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getCoUserId() {
        return coUserId;
    }

    public void setCoUserId(Integer coUserId) {
        this.coUserId = coUserId;
    }
}