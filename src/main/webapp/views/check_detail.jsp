<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (int)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>卡客途安轮卫管家</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<%=path %>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/google.css" rel="stylesheet">
<link href="<%=path %>/css/style.css" rel="stylesheet" type="text/css">
<link href="<%=path %>/css/font-awesome.css" rel="stylesheet">
<link href="<%=path %>/css/bootstrap-select.min.css" rel="stylesheet">
<link rel="apple-touch-icon" sizes="76x76" href="<%=path %>/img/logo.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=path %>/img/logo.png">
<style type="text/css">
html,body{
min-height:100%;
}
</style>
</head>
<body style="margin:0 auto;height: 100%" id="body">
<!-- header -->
<jsp:include page="header.jsp"></jsp:include>
<!-- main -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span12">
          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> 
              <i class="icon-th-list"></i>
              <h3>车辆监控</h3>
              <!-- <button type="submit" class="btn btn-primary" onclick="" style="float: right;margin-top:6px;margin-right:5px;">添加</button> -->
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
            	<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li  class="active">
						    <a href="#veh" data-toggle="tab">${car.licenseNo}<input type="hidden" id="id" value="${car.id}"> </a>
						  </li>
						  <li ><a href="#tire" data-toggle="tab">轮位</a></li>
						  <li id="railli"><a href="#rail" data-toggle="tab">轨迹</a></li>
						 <!--  <li ><a href="#warn" data-toggle="tab">报警</a></li> -->
						</ul>
						<br>
						<div class="tab-content">
							<div class="tab-pane active" id="veh">
								<table class="table table-striped table-bordered">
					                <thead>
					                  <tr>
					                    <th>速度</th>
					                    <th>里程</th>
					                    <th>尾灯</th>
					                    <th>剩余电量</th>
					                  </tr>
					                </thead>
					                <tbody>
					                		<tr>
												<td id="speed"></td>
												<td id="miles"></td>
												<td id="">正常</td>
												<td id="charge"></td>
											</tr>
					                		<tr>
												<td colspan="4"><div id="allmap" style="width:100%;height:500px;"></div>
													<input type="hidden" id="lat" value="">
													<input type="hidden" id="lng" value="">
												</td>
											</tr>
					                </tbody>
					              </table>
							</div>
							<div class="tab-pane" id="tire">
									<table class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>轮胎编号</th>
											<th>检测气压</th>
											<th>检测温度</th>
											<th>压力报警</th>
											<th>温度报警</th>
											<th>漏气报警</th>
											<th>信号丢失报警</th>
										</tr>
									</thead>
									<tbody id="tiredata">
										
									</tbody>
									</table>
							</div>
							<div class="tab-pane" id="rail">
								<div style="width:100%;height:30px;overflow: hidden; position: absolute;z-index:100"><button type="submit" class="btn btn-primary" onclick="replay()" style="float: right;margin-right:5px;">回放</button></div>
								<div id="railmap" style="width:100%;height:500px;margin-top:-30px;"></div></div>
							<!-- <div class="tab-pane" id="warn"></div> -->
					    </div>
				</div>
			 </div> <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
  <br>
  <br>
  <br>
</div>
<!-- /main -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<%=path %>/js/jquery-1.7.2.min.js"></script> 
<script src="<%=path %>/js/excanvas.min.js"></script>
<script src="<%=path %>/js/bootstrap.js"></script>
<%-- <script src="<%=path %>/js/base.js"></script> --%>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.5&ak=N392KHKB3hPkqGYElL3OYoHIzoqtZkHj"></script>
<script type="text/javascript">
/* var minheight = $(window).height();
$("#body").css({
  'min-height':minheight +'px'
}) */
$("#addInput").click(function(){
	$(this).parent().before('<input type="text" style="float: left" class="span6" id="tProductId" name="tProductId" value="">');
});
var  map1 = new BMap.Map("allmap");//创建百度地图实例，这里的allmap是地图容器的id  
function getmap(){
	var  lng = $('#lng').val();
	var  lat = $('#lat').val();
	
	map1.centerAndZoom(new BMap.Point(lng,lat),12);
	map1.enableScrollWheelZoom(true);
	
	map1.clearOverlays(); 
	var new_point = new BMap.Point(lng,lat);
	var marker = new BMap.Marker(new_point);  // 创建标注
	map1.addOverlay(marker);              // 将标注添加到地图中
	map1.panTo(new_point); 
}

var startpointlat;
var startpointlng;
var endpointlat;
var endpointlng;
var points = new Array();
var n = 0;
var fencecheck = ${fencecheck };
var tchecks = ${tchecks};
var vchecks = ${vchecks};
function setVchecks(){
	if(null != vchecks){
		$("#speed").html(vchecks.speed);
		$("#miles").html(vchecks.miles);
		$("#charge").html(vchecks.charge);
		$("#lat").val(vchecks.lat);
		$("#lng").val(vchecks.lng);
	}
};
setVchecks();
addtire(tchecks);
getmap(); 

<%-- // 用经纬度设置地图中心点	
function queryData(){
	$.ajax({
		   url:'<%=path%>/check/data',
		   type:'GET', //GET
		   async:false,    //或false,是否异步
		   contentType:"application/json",
		   data:"id="+$("#id").val(),
		   dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
		   success:function(data){
			   vchecks = data.vchecks;
			   tchecks = data.tchecks;
			  // fencecheck = data.fencecheck;
			   
			   setVchecks();
			   addtire(tchecks);
			   getmap(); 
			},
			error : function(xhr) {
				console.log('错误');
				console.log(xhr);
			},
		});
}; --%>

function addtire(tchecks){
	var str = "";
	for(var i=0;i<tchecks.length;i++){
		var pressureWarning = tchecks[i].pressureWarning;
		var pw = "";
		if(pressureWarning==0){pw="正常"; };
		if(pressureWarning==1){pw="严重亏气"; };
		if(pressureWarning==2){pw="低压报警"; };
		if(pressureWarning==3){pw="低压提醒"; };
		if(pressureWarning==4){pw="高压报警"; };
		if(pressureWarning==5){pw="高压提醒"; };
		if(pressureWarning==99){pw="丢失"; };
		
		var temperatureWarning = tchecks[i].temperatureWarning;
		var tw = "";
		if(temperatureWarning==0){tw="正常"; };
		if(temperatureWarning==1){tw="高温报警"; };
		if(temperatureWarning==2){tw="高温提醒"; };
		if(temperatureWarning==99){tw="丢失"; };
		
		var airWarning = tchecks[i].airWarning;
		var aw = "";
		if(airWarning==0){aw="正常"; };
		if(airWarning==1){aw="漏气"; };
		if(airWarning==99){aw="丢失"; };
		
		
		var signalWarning = tchecks[i].signalWarning;
		var sw = "";
		if(signalWarning==0){sw="正常"; };
		if(signalWarning==1){sw="丢失"; };
		if(pressureWarning==99 || temperatureWarning==99 || airWarning==99){
			sw="丢失";
		}
		
		str += "<tr>";
		str += "<td>"+tchecks[i].tyreNo+"</td>";
		str += "<td>"+tchecks[i].pressure+"</td>";
		str += "<td>"+tchecks[i].temperature+"</td>";
		str += "<td>"+pw+"</td>";
		str += "<td>"+tw+"</td>";
		str += "<td>"+aw+"</td>";
		str += "<td>"+sw+"</td>";
		str += "</tr>";
	}
	$("#tiredata").html(str);
}

//queryData();

/* var points = [
new BMap.Point(114.00100, 22.550000), 
new BMap.Point(114.00130, 22.550000),
new BMap.Point(114.00160, 22.550000), 
new BMap.Point(114.00200, 22.550000),
new BMap.Point(114.00300, 22.550500), 
new BMap.Point(114.00400, 22.550000),
new BMap.Point(114.00500, 22.550000), 
new BMap.Point(114.00505, 22.549800),
new BMap.Point(114.00510, 22.550000), 
new BMap.Point(114.00515, 22.550000),
new BMap.Point(114.00525, 22.550400),
 new BMap.Point(114.00537, 22.549500)
]; */

for(var i=0;i<fencecheck.length;i++){
	points.push(new BMap.Point(fencecheck[i].lng, fencecheck[i].lat));
	if(i==0){
		startpointlat=fencecheck[i].lat;
		startpointlng=fencecheck[i].lng;
	}
	if(i==fencecheck.length-1){
		endpointlat=fencecheck[i].lat;
		endpointlng=fencecheck[i].lng;
	}
}

var map; //百度地图对象
var car; //汽车图标
var centerPoint;
var label; //信息标签
var timer; //定时器
var index = 0; //记录播放到第几个point
function init() {
	//初始化地图,选取第一个点为起始点
	map = new BMap.Map("railmap");
	map.centerAndZoom(points[0], 11);
	map.enableScrollWheelZoom();
	map.addControl(new BMap.NavigationControl());  
    map.addControl(new BMap.ScaleControl());  
    map.addControl(new BMap.OverviewMapControl());  
   // map.addControl(new BMap.MapTypeControl());  

	//通过DrivingRoute获取一条路线的point
	var driving = new BMap.DrivingRoute(map);
	driving.search(new BMap.Point(startpointlng, startpointlat), new BMap.Point(endpointlng, endpointlat));
	driving.setSearchCompleteCallback(function() {
		//得到路线上的所有point
		points = driving.getResults().getPlan(0).getRoute(0).getPath();
		//画面移动到起点和终点的中间
		var lng1 = (points[0].lng + points[points.length - 1].lng)/2;
		var lat1 = (points[0].lat + points[points.length - 1].lat)/2;
		centerPoint = new BMap.Point(lng1,lat1);
		map.panTo(centerPoint);
		//连接所有点
		map.addOverlay(new BMap.Polyline(points, {strokeColor: "green", strokeWeight: 5, strokeOpacity: 1}));

		//显示小车子
		label = new BMap.Label("", {offset: new BMap.Size(-20, -20)});
		label.setStyle({
			width: "32px",
			color: '#fff',
			background: 'url(<%=path%>/img/delivery.png)',
			border: '0px solid "#ff8355"',
			height: "32px",
			top:"1px"
			});
		car = new BMap.Marker(points[0]);
		car.setLabel(label);
		map.addOverlay(car);
	});
}
init();

<%-- var map = new BMap.Map("railmap");
map.centerAndZoom(points[0], 11);

var myP1 = new BMap.Point(startpointlng, startpointlat);    //起点
var myP2 = new BMap.Point(endpointlng, endpointlat);    //终点
var myIcon = new BMap.Icon("<%=path%>/img/delivery.png", new BMap.Size(32, 70), {    //小车图片
	//offset: new BMap.Size(0, -5),    //相当于CSS精灵
	imageOffset: new BMap.Size(0, 0)    //图片的偏移量。为了是图片底部中心对准坐标点。
  });
var driving2 = new BMap.DrivingRoute(map, {renderOptions:{map: map, autoViewport: true}});    //驾车实例
driving2.search(myP1, myP2);    //显示一条公交线路

window.run = function (){
	var driving = new BMap.DrivingRoute(map);    //驾车实例
	driving.search(myP1, myP2);
	driving.setSearchCompleteCallback(function(){
		var pts = points;    //通过驾车实例，获得一系列点的数组
        console.log(pts);
		var paths = pts.length;    //获得有几个点
		
		var carMk = new BMap.Marker(pts[0],{icon:myIcon});
		map.addOverlay(carMk);
		i=0;
		function resetMkPoint(i){
			carMk.setPosition(pts[i]);
			if(i < paths){
				setTimeout(function(){
					i++;
					resetMkPoint(i);
				},100);
			}
		}
		setTimeout(function(){
			resetMkPoint(5);
		},100)

	});
}

setTimeout(function(){
	run();
},1500); --%>

function play(){
	var point = points[index];
	if(index > 0) {
		map.addOverlay(new BMap.Polyline([points[index - 1], point], {strokeColor: "red", strokeWeight: 1, strokeOpacity: 1}));
	}
	car.setPosition(point);
	index++;
	map.panTo(point);
	if(index < points.length) {
		timer = window.setTimeout("play(" + index + ")", 50);
	}else{
		map.panTo(point);
	}
}
function replay(){
	window.clearTimeout(timer);
	index = 0;
	car.setPosition(points[0]);
	map.panTo(centerPoint);
	play();
}
$("#railli").click(function(){
	window.clearTimeout(timer);
	index = 0;
	car.setPosition(points[0]);
	map.panTo(centerPoint);
	play();
}); 

/* var t=setInterval(function(){
	//queryData();
	getmap();
},30000);  */
</script>
</body>
</html>