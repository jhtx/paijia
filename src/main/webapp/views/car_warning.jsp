<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (int)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>卡客途安轮卫管家</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<%=path %>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/google.css" rel="stylesheet">
<link href="<%=path %>/css/style.css" rel="stylesheet" type="text/css">
<link href="<%=path %>/css/font-awesome.css" rel="stylesheet">
<link rel="apple-touch-icon" sizes="76x76" href="<%=path %>/img/logo.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=path %>/img/logo.png">
<style type="text/css">
html,body{
min-height:100%;
}
</style>
</head>
<body style="margin:0 auto;height: 100%" id="body">
<!-- header -->
<jsp:include page="header.jsp"></jsp:include>
<!-- main -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span12">
          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> 
              <i class="icon-th-list"></i>
              <h3>实时报警</h3>
              <div class="controls" style="float: right">
                 <label class="radio inline">
                   <input type="radio" name="radiobtns" onclick="sdwarn(1)" <c:if test="${empty user.warn || user.warn==1  }">checked</c:if> > 打开提醒
                 </label>
                 
                 <label class="radio inline">
                   <input type="radio" name="radiobtns" onclick="sdwarn(0)" <c:if test="${!empty user.warn && user.warn==0  }">checked</c:if> > 关闭提醒
                 </label>
               </div>
              <!-- <button type="submit" class="btn btn-primary" onclick="" style="float: right;margin-top:6px;margin-right:5px;">添加</button> -->
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
            	<div class="tabbable">
					<!-- <ul class="nav nav-tabs">
					  <li>
					    <a href="#formcontrols" data-toggle="tab">Form Controls</a>
					  </li>
					  <li  class="active"><a href="#jscontrols" data-toggle="tab">JS Controls</a></li>
					</ul>
					<br> -->
					<div class="tab-content">
						<form id="form01" class="form-horizontal" method="POST">
							<table class="table table-striped table-bordered">
				                <tbody>
				                	<c:forEach items="${warnings }" var="w">
					                	<c:if test="${! empty w.msg}">
						                	<tr>
							                    <td style="width: 75%">时间：<fmt:formatDate value="${w.warnDate }" pattern="yyyy-MM-dd HH:mm:ss"/><br>${w.msg }</td>
							                    <td style="width: 25%"><button type="submit"  onclick="del(this);"  class="btn btn-primary btn-small"  value="${w.id }"  style="float: left;margin-top:6px;margin-left:5px;">已处理</button>
						                  	</tr>
					                  	</c:if>
				                  	</c:forEach>
				                </tbody>
				              </table>
				              <input type="hidden" name="id" id="id" value="">
				         </form>
					</div>
				</div>
			 </div> <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
  <br>
  <br>
  <br>
</div>
<!-- /main -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<%=path %>/js/jquery-1.7.2.min.js"></script> 
<script src="<%=path %>/js/excanvas.min.js"></script>
<script src="<%=path %>/js/bootstrap.js"></script>
<script src="<%=path %>/js/base.js"></script>
<script type="text/javascript">
var minheight = $(window).height();
$("#body").css({
  'min-height':minheight +'px'
})

$("#add").click(function(){
	location.href="<%=path %>/user/toadd.html"
});
function detail(id){
	$("#id").val(id);
	$("#form01").attr('action',"<%=path %>/user/detail.html");
	$("#form01").submit();
}
function update(id){
	$("#id").val(id);
	$("#form01").attr('action',"<%=path %>/user/toadd.html");
	$("#form01").submit();
}
function rights(id){
	$("#id").val(id);
	$("#form01").attr('action',"<%=path %>/user/rights.html");
	$("#form01").submit();
}
function sdwarn(vl){
	$.ajax({
	    url:'<%=path %>/warn/updateWarn.html?warn='+vl,
	    type:'GET', //GET
	    async:true,    //或false,是否异步
	    success:function(data){
	    	console.log(data);
	    },
	    error:function(xhr){},
	});
}

function del(bt){
	$.ajax({
	    url:'<%=path %>/warn/del.html?id='+$(bt).val(),
	    type:'GET', //GET
	    async:true,    //或false,是否异步
	    success:function(data){
	    	$(bt).parent().parent().remove();
	    },
	    error:function(xhr){},
	});
}

</script>
</body>
</html>