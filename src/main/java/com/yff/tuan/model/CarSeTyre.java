package com.yff.tuan.model;

public class CarSeTyre {
    private Integer id;

    private Integer carId;

    private Integer tyreId;
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getTyreId() {
        return tyreId;
    }

    public void setTyreId(Integer tyreId) {
        this.tyreId = tyreId;
    }

    
    private Integer tyreName;
	public Integer getTyreName() {
		return tyreName;
	}

	public void setTyreName(Integer tyreName) {
		this.tyreName = tyreName;
	}
    
}