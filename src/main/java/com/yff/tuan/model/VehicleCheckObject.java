package com.yff.tuan.model;

import java.util.List;

public class VehicleCheckObject {
	private List<VehicleCheck> vehicleChecks;
	private String licenseNo;
	private String typName;
	private double miles;
	private String productId;
	
	public List<VehicleCheck> getVehicleChecks() {
		return vehicleChecks;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public String getTypName() {
		return typName;
	}
	public void setVehicleChecks(List<VehicleCheck> vehicleChecks) {
		this.vehicleChecks = vehicleChecks;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public void setTypName(String typName) {
		this.typName = typName;
	}
	public double getMiles() {
		return miles;
	}
	public void setMiles(double miles) {
		this.miles = miles;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	
	
}
