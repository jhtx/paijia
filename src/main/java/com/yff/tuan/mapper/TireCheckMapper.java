package com.yff.tuan.mapper;

import com.yff.tuan.model.TireCheck;
import com.yff.tuan.model.TireCheckExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TireCheckMapper {
    int countByExample(TireCheckExample example);

    int deleteByExample(TireCheckExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TireCheck record);

    int insertSelective(TireCheck record);

    List<TireCheck> selectByExample(TireCheckExample example);

    TireCheck selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TireCheck record, @Param("example") TireCheckExample example);

    int updateByExample(@Param("record") TireCheck record, @Param("example") TireCheckExample example);

    int updateByPrimaryKeySelective(TireCheck record);

    int updateByPrimaryKey(TireCheck record);
}