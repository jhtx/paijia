package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarBind;
import com.yff.tuan.model.CarBindExample;
import com.yff.tuan.model.CarBindTyre;
import com.yff.tuan.model.CarBindTyreExample;
import com.yff.tuan.model.CarExample;
import com.yff.tuan.model.Cbrand;
import com.yff.tuan.model.CbrandExample;
import com.yff.tuan.model.Cmodel;
import com.yff.tuan.model.CmodelExample;
import com.yff.tuan.model.TireCheck;
import com.yff.tuan.model.VehicleCheck;
import com.yff.tuan.util.Page;

public interface BindService {
	public abstract CarBind findCarBind(CarBindExample example);
	public abstract int countCarBind(CarBindExample example);
	public abstract List<CarBind> queryCarBind(CarBindExample example);
	public abstract List<CarBindTyre> queryCarBindTyre(CarBindTyreExample example);
	
	public abstract boolean insert(CarBind carBind);
	public abstract boolean insert(List<CarBindTyre> carBindTyres,int carId);
	
	public abstract int updateCarBind(CarBind carBind);
	public abstract int updateCarBindTyre(List<CarBindTyre> carBindTyres);
	
	public abstract List<VehicleCheck> queryVehicleCheck(String productId);
	public abstract VehicleCheck findVehicleCheck(String productId);
	public abstract List<VehicleCheck> findVehicleCheckInFence(String productId);
	public abstract List<TireCheck> queryTireCheck(String productId);
	public abstract TireCheck findTireCheck(String productId);
	
}
