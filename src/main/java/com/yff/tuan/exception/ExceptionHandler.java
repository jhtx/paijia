package com.yff.tuan.exception;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

public class ExceptionHandler extends SimpleMappingExceptionResolver{  
  
    private static final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);  
      
    @Override  
    protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {  
        ModelAndView modelAndView = super.doResolveException(request, response, handler, ex);  
        String url = request.getRequestURL().toString();  
        logger.error("controller error.url=" + url, ex);  
        if (modelAndView == null) {  
            modelAndView = new ModelAndView("/error/error");  
        }
        modelAndView.addObject("error","页面未找到请稍后再试");  
        return modelAndView;  
    }  
} 