package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarFence;
import com.yff.tuan.model.CarFenceExample;
import com.yff.tuan.model.CarExample;
import com.yff.tuan.model.CarFence;
import com.yff.tuan.model.Cbrand;
import com.yff.tuan.model.CbrandExample;
import com.yff.tuan.model.Cmodel;
import com.yff.tuan.model.CmodelExample;
import com.yff.tuan.model.TireCheck;
import com.yff.tuan.model.VehicleCheck;
import com.yff.tuan.util.Page;

public interface FenceService {
	public abstract CarFence findCarFence(CarFenceExample example);
	public abstract CarFence findByPrimaryKey(Integer id);
	public abstract List<CarFence> queryCarFence(CarFenceExample example);
	
	public abstract boolean insert(CarFence carFence);
	
	public abstract boolean update(CarFence carFence);
	
	public abstract boolean delete(Integer id);
	
	public abstract List<VehicleCheck> queryVehicleCheck(String productId);
	public abstract List<TireCheck> queryTireCheck(String productId);
	
}
