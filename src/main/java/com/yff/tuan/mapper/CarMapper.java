package com.yff.tuan.mapper;

import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CarMapper {
    int countByExample(CarExample example);

    int deleteByExample(CarExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Car record);

    int insertSelective(Car record);

    List<Car> selectByExampleWithBLOBs(CarExample example);

    List<Car> selectByExample(CarExample example);

    Car selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Car record, @Param("example") CarExample example);

    int updateByExampleWithBLOBs(@Param("record") Car record, @Param("example") CarExample example);

    int updateByExample(@Param("record") Car record, @Param("example") CarExample example);

    int updateByPrimaryKeySelective(Car record);

    int updateByPrimaryKeyWithBLOBs(Car record);

    int updateByPrimaryKey(Car record);
    
    List<Car>  selectByUserId(Integer userId);
    List<Car>  selectCheckCarByAdminUser(Integer userId);
    List<Car>  selectCheckCarDispatch(Integer userId);
    Car  findCheckCar(Integer carId);
    List<Car>  selectCheckCarByNormalUser(@Param("adminUserId") Integer adminUserId , @Param("userId") Integer userId);
}