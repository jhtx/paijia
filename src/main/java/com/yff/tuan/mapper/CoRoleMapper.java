package com.yff.tuan.mapper;

import com.yff.tuan.model.CoRole;
import com.yff.tuan.model.CoRoleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CoRoleMapper {
    int countByExample(CoRoleExample example);

    int deleteByExample(CoRoleExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CoRole record);

    int insertSelective(CoRole record);

    List<CoRole> selectByExample(CoRoleExample example);

    CoRole selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CoRole record, @Param("example") CoRoleExample example);

    int updateByExample(@Param("record") CoRole record, @Param("example") CoRoleExample example);

    int updateByPrimaryKeySelective(CoRole record);

    int updateByPrimaryKey(CoRole record);
}