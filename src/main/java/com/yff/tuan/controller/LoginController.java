package com.yff.tuan.controller;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yff.tuan.model.CoUser;
import com.yff.tuan.model.CoUserExample;
import com.yff.tuan.service.CoUserService;
import com.yff.tuan.util.AESUtil;
import com.yff.tuan.util.Page;

@Controller
public class LoginController {
	private Logger LOG = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired CoUserService coUserService;
	
	@RequestMapping("/login")
	public String login(Model model,CoUser user,HttpServletRequest request,HttpServletResponse response){
		Object isLgin = request.getSession().getAttribute("isLogin");
		if(null != isLgin && true == (boolean)isLgin ){
			return "/index";
		}
		if(null != user ){
			if(StringUtils.isEmpty(user.getName()) || StringUtils.isEmpty(user.getPassword()) ) {
				return "/login";
			}
			user.setPassword(AESUtil.Encrypt(user.getPassword()));
			user = coUserService.find(user);
			if(null == user) {
				model.addAttribute("msg", "用户名或密码错误！");
				return "/login";
			}
			request.getSession().setAttribute("userId", user.getId());
			request.getSession().setAttribute("userName", user.getName());
			request.getSession().setAttribute("parentId", null==user.getParentId()?0:user.getParentId());
			request.getSession().setAttribute("role", user.getRole());
			request.getSession().setAttribute("isLogin", true);
			LOG.info(user.getName()+"#######################");
			LOG.info(user.getPassword()+"#######################");
			/*model.addAttribute("user", user);*/
			return "/index";
		}
		return "/login";
	}
	
	@RequestMapping("/index")
	public String index(Model model,CoUser user,HttpServletRequest request,HttpServletResponse response){
		Object isLgin = request.getSession().getAttribute("isLogin");
		if(null != isLgin && true == (boolean)isLgin ){
			return "/index";
		}
		return "/login";
	}
	
	@RequestMapping("/logout")
	public Object loginout(Model model,HttpServletRequest request,HttpServletResponse response){
		request.getSession().invalidate();
		return "/login";
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public Object add(Model model,@RequestBody CoUser coUser,HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map = new HashMap<String,Object>();
		int i = coUserService.insert(coUser);
		map.put("add", i>0?true:false);
		return map;
	}
	
	
}
