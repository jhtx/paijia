package com.yff.tuan.model;

import java.util.List;

public class UserCar {
	private Integer userId;
	private List<CoUserCar> cars;
	
	public Integer getUserId() {
		return userId;
	}
	public List<CoUserCar> getCars() {
		return cars;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public void setCars(List<CoUserCar> cars) {
		this.cars = cars;
	}
	
	
}
