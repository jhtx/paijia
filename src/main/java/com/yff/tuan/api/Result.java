package com.yff.tuan.api;

import org.springframework.http.HttpStatus;

public enum Result{

	/** The success. */
	SUCCESS("0", HttpStatus.OK,"SUCCESS"),

	/** The invalid parameters. */
	INVALID_PARAMETERS("1001",HttpStatus.BAD_REQUEST, "参数错误."),
	
	/** The wrong json format. */
	WRONG_JSON_FORMAT("1002", HttpStatus.BAD_REQUEST, "Wrong json format."),
	
	/** The invalid header. */
	INVALID_HEADER("1003",  HttpStatus.BAD_REQUEST, "Invalid header."),
	
	/** The missing header. */
	MISSING_HEADER("1004",  HttpStatus.BAD_REQUEST, "Missing header."),
	
	/** The http method not supported exception. */
	HTTP_METHOD_NOT_SUPPORTED("1005", HttpStatus.METHOD_NOT_ALLOWED, "Http method not supported."),

	MISSING_REQUEST_PARAMETER("1006", HttpStatus.BAD_REQUEST, "参数错误."),
	
	MISSING_REQUIRED_PHONE_NUMBER("1007", HttpStatus.BAD_REQUEST, "参数错误."),
	
	/** The generate token failed. */
	GENERATE_TOKEN_FAILED("2001", HttpStatus.INTERNAL_SERVER_ERROR, "Generate token failed."),
	
	/** The invalid token. */
	INVALID_TOKEN("2002", HttpStatus.FORBIDDEN, "Invalid token."),
	
	/** The expired token. */
	EXPIRED_TOKEN("2007", HttpStatus.FORBIDDEN, "Expired token."),
	
	/** The too frequent request. */
	TOO_FREQUENT_REQUEST("2004", HttpStatus.FORBIDDEN, "Too frequent request."),
	
	/** The invalid token. */
	INVALID_USER("2006", HttpStatus.FORBIDDEN, "Invalid token."),
	
	/** The already registered phone number. */
	ALREADY_REGISTERED_PHONE_NUMBER("3001", HttpStatus.BAD_REQUEST, "Already registered phone number."),
	
	/** The not registered phone number. */
	NOT_REGISTERED_PHONE_NUMBER("3002", HttpStatus.BAD_REQUEST, "Not registered phone number."),
	
	/** The password not equal. */
	PASSWORD_NOT_EQUAL("3003", HttpStatus.BAD_REQUEST, "Password not equal."),
	
	/** The hash password failed. */
	HASH_PASSWORD_FAILED("3004", HttpStatus.INTERNAL_SERVER_ERROR, "Hash password failed."),
	
	/** The wrong phone number. */
	WRONG_PHONE_NUMBER("3005", HttpStatus.BAD_REQUEST, "手机号错误."),
	
	/** The wrong password. */
	WRONG_PASSWORD("3006", HttpStatus.BAD_REQUEST, "密码错误."),
	
	/** The wrong vcode. */
	WRONG_VCODE("3007", HttpStatus.BAD_REQUEST, "Wrong json format."),
	
	NOT_REGISTERED_USER("3016", HttpStatus.BAD_REQUEST, "Not registered user."),
	
	NULL_NOTIFICATION_TOKEN("4001", HttpStatus.BAD_REQUEST, "Null notification token."),
	
	BIND_WECHAT_FAIL("4002", HttpStatus.BAD_REQUEST, "Bind wechat fail."),
	
	ALREADY_BIND_WECHAT("4003", HttpStatus.BAD_REQUEST, "Already bind wechat."),
	
	ALREADY_EXIST_VEHICLENO("4004", HttpStatus.BAD_REQUEST, "车牌号已经存在！"),
	
	ALREADY_EXIST_USER_NAME("5001", HttpStatus.BAD_REQUEST, "用户名已经存在！"),
	
	ALREADY_EXIST_CUSTOMER("5002", HttpStatus.BAD_REQUEST, "客户已经存在！"),
	
	REQUESTED_CANNOT_BE_FOUND("9001",HttpStatus.BAD_REQUEST,"Requested is not available or cannot be found."),
	
	/** The internal server error. */
	INTERNAL_SERVER_ERROR("9999", HttpStatus.INTERNAL_SERVER_ERROR, "内部错误."),
	
	;
	
	
	
	/** The code. */
	private String code;
	
	private HttpStatus status;
	
	/** The status code. */
	private String msg;
	
	/**
	 * Instantiates a new error code.
	 *
	 * @param code the code
	 * @param statusCode the status code
	 */
	private Result(String code,HttpStatus status, String msg) {
		this.code = code;
		this.status = status;
		this.msg = msg;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode(){
		return code;
	}
	
	/**
	  * Gets the status.
	 * @return the status
	 */
	public HttpStatus getStatus() {
		return status;
	}
	
	/**
	  * Gets the msg.
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}
	
}
