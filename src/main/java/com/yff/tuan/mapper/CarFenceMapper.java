package com.yff.tuan.mapper;

import com.yff.tuan.model.CarFence;
import com.yff.tuan.model.CarFenceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CarFenceMapper {
    int countByExample(CarFenceExample example);

    int deleteByExample(CarFenceExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CarFence record);

    int insertSelective(CarFence record);

    List<CarFence> selectByExample(CarFenceExample example);

    CarFence selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CarFence record, @Param("example") CarFenceExample example);

    int updateByExample(@Param("record") CarFence record, @Param("example") CarFenceExample example);

    int updateByPrimaryKeySelective(CarFence record);

    int updateByPrimaryKey(CarFence record);
}