package com.yff.tuan.mapper;

import com.yff.tuan.model.Cposition;
import com.yff.tuan.model.CpositionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CpositionMapper {
    int countByExample(CpositionExample example);

    int deleteByExample(CpositionExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Cposition record);

    int insertSelective(Cposition record);

    List<Cposition> selectByExample(CpositionExample example);

    Cposition selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Cposition record, @Param("example") CpositionExample example);

    int updateByExample(@Param("record") Cposition record, @Param("example") CpositionExample example);

    int updateByPrimaryKeySelective(Cposition record);

    int updateByPrimaryKey(Cposition record);
}