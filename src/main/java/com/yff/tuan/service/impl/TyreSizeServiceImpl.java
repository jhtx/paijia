package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.CarMapper;
import com.yff.tuan.mapper.CtyreSizeMapper;
import com.yff.tuan.mapper.CmodelMapper;
import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarExample;
import com.yff.tuan.model.CarExample.Criteria;
import com.yff.tuan.model.CtyreSize;
import com.yff.tuan.model.CtyreSizeExample;
import com.yff.tuan.model.Cmodel;
import com.yff.tuan.model.CmodelExample;
import com.yff.tuan.service.BrandService;
import com.yff.tuan.service.CarService;
import com.yff.tuan.service.ModelService;
import com.yff.tuan.service.TyreSizeService;
import com.yff.tuan.util.Page;

@Component
public class TyreSizeServiceImpl implements TyreSizeService {
	@Autowired
	CtyreSizeMapper ctyreSizeMapper;
	

	@Override
	public List<CtyreSize> query(CtyreSizeExample example) {
		return ctyreSizeMapper.selectByExample(example);
	}


}
