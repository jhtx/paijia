package com.yff.tuan.constants;

public class CustomerConstants {
	public enum Status{
		TARGET_CUSTOMER(1,"目标客户"),
		FIRST_VISIT(2,"初次拜访"),
		OP_EVA(3,"机会评估"),
		INFO_SEL(4,"信息收集"),
		PLAN_CONFIM(5,"方案确定"),
		PLAN_ACT(6,"方案实施"),
		PIS_NOT_ACT(7,"PISIP未实施"),
		PIS_ACT(8,"PISIP实施"),
		FAIL(9,"生意失败"),
		RE_EVA(10,"重新评估"),		
		;
		private Status(int code,String msg) {
			this.code=code;
			this.msg=msg;
		}
		
		public static String getStatus(int code) {
			for(Status status:values()) {
				if(code == status.getCode()) {
					return status.getMsg();
				}
			}
			return "";
		}
		
		private int code;
		private String msg;
		public int getCode() {
			return code;
		}
		public void setCode(int code) {
			this.code = code;
		}
		public String getMsg() {
			return msg;
		}
		public void setMsg(String msg) {
			this.msg = msg;
		}
	}
	
	
	public enum FailureReason{
		SERVICE(1,"服务"),
		PRODUCT(2,"产品"),
		FINANCE(3,"资金"),
		PSIP(4,"PSIP"),
		RELATION(5,"关系"),
		SIZE(6,"规模"),
		OTHER(7,"其他")		
		;
		private FailureReason(int code,String msg) {
			this.code=code;
			this.msg=msg;
		}
		
		public static String getFailureReason(int code) {
			for(FailureReason failureReason:values()) {
				if(code == failureReason.getCode()) {
					return failureReason.getMsg();
				}
			}
			return "";
		}
		
		private int code;
		private String msg;
		public int getCode() {
			return code;
		}
		public void setCode(int code) {
			this.code = code;
		}
		public String getMsg() {
			return msg;
		}
		public void setMsg(String msg) {
			this.msg = msg;
		}
	}
}
