package com.yff.tuan.mapper;

import com.yff.tuan.model.VehicleCheck;
import com.yff.tuan.model.VehicleCheckExample;

import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface VehicleCheckMapper {
    int countByExample(VehicleCheckExample example);

    int deleteByExample(VehicleCheckExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(VehicleCheck record);

    int insertSelective(VehicleCheck record);

    List<VehicleCheck> selectByExample(VehicleCheckExample example);

    VehicleCheck selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") VehicleCheck record, @Param("example") VehicleCheckExample example);

    int updateByExample(@Param("record") VehicleCheck record, @Param("example") VehicleCheckExample example);

    int updateByPrimaryKeySelective(VehicleCheck record);

    int updateByPrimaryKey(VehicleCheck record);
    
    List<VehicleCheck> selectByProductId(String productId);
    
    List<VehicleCheck> selectDragCarByProductId(@Param("productId") String productId,@Param("startDate") Date startDate,@Param("endDate") Date endDate);
    List<VehicleCheck> selectDrawCarByProductId(String productId);
    List<VehicleCheck> selectWholeCarByProductId(String productId);
}