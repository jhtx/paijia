package com.yff.tuan.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.CarBindMapper;
import com.yff.tuan.mapper.CarFenceMapper;
import com.yff.tuan.mapper.CarMapper;
import com.yff.tuan.mapper.VehicleCheckMapper;
import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarBind;
import com.yff.tuan.model.VehicleCheck;
import com.yff.tuan.model.VehicleCheckExample;
import com.yff.tuan.model.VehicleCheckExample.Criteria;
import com.yff.tuan.model.VehicleCheckObject;
import com.yff.tuan.service.VehicleCheckService;
import com.yff.tuan.util.MathUtil;

@Component
public class VehicleCheckServiceImpl implements VehicleCheckService{
	@Autowired
	VehicleCheckMapper vehicleCheckMapper;
	@Autowired
	CarFenceMapper carFenceMapper;
	@Autowired
	CarMapper carMapper;
	@Autowired
	CarBindMapper carBindMapper;
	
	@Override
	public boolean save(VehicleCheck vehicleCheck) {
			return true;
	}

	@Override
	public List<VehicleCheckObject> queryDrag(Integer userId,Date startTime ,Date endTime,Integer fleetId,String licenseNo,Integer typeSizeId) {
		
		List<VehicleCheckObject> checkObjects = new ArrayList<VehicleCheckObject>();
		CarBind bind = new CarBind();
		bind.setUserId(userId);
		if(null != fleetId) {
			bind.setFleetId(fleetId);
		}
		if(null != licenseNo && !licenseNo.isEmpty()) {
			bind.setLicenseNo(licenseNo);
		}
		if(null != typeSizeId) {
			bind.setTypeSizeId(typeSizeId);
		}
		List<CarBind> carBinds = carBindMapper.selectDragCarByUserId(bind);
		for(CarBind carBind:carBinds) {
			List<VehicleCheck> vehicleChecks = vehicleCheckMapper.selectDragCarByProductId(carBind.getProductId(),startTime,endTime);
			if(!vehicleChecks.isEmpty()) {
				VehicleCheckObject checkObject = new VehicleCheckObject();
				checkObject.setVehicleChecks(vehicleChecks);
				double miles = 0;
				for(VehicleCheck check:vehicleChecks) {
					miles += check.getMile();
				}
				checkObject.setProductId(carBind.getProductId());
				checkObject.setMiles(MathUtil.parseDouble(miles));
				checkObject.setTypName(carBind.getCarType());
				checkObject.setLicenseNo(carBind.getLicenseNo());
				checkObjects.add(checkObject);
			}
		}
		return checkObjects;
	}
	
	@Override
	public VehicleCheckObject findByProductId(Integer userId,String productId,Date startTime ,Date endTime) {
		CarBind bind = new CarBind();
		bind.setUserId(userId);
		bind.setProductId(productId);
		List<CarBind> carBinds = carBindMapper.selectDragCarByUserId(bind);
		VehicleCheckObject checkObject = new VehicleCheckObject();
		if(!carBinds.isEmpty()) {
			List<VehicleCheck> vehicleChecks = vehicleCheckMapper.selectDragCarByProductId(carBinds.get(0).getProductId(),startTime,endTime);
			List<VehicleCheck> newVehicleChecks = new ArrayList<VehicleCheck>();
			if(!vehicleChecks.isEmpty()) {
				double miles = 0;
				for(VehicleCheck check:vehicleChecks) {
					if(check.getMile()>0) {
						miles += check.getMile();
						check.setMile(MathUtil.parseDouble(check.getMile()));
						
						VehicleCheckExample example = new VehicleCheckExample();
						Criteria criteria = example.createCriteria();
						criteria.andProductIdEqualTo(check.getProductId());
						criteria.andMatchProductIdEqualTo(check.getMatchProductId());
						if(startTime != null) {
							criteria.andCheckTimeGreaterThanOrEqualTo(startTime);
						}
						if(endTime != null) {
							criteria.andCheckTimeLessThanOrEqualTo(endTime);
						}
						example.setOrderByClause(" id desc limit 1 ");
						List<VehicleCheck> list = vehicleCheckMapper.selectByExample(example);
						if(null != list && !list.isEmpty()) {
							check.setEndDate(list.get(0).getCheckTime());
						}
						example.setOrderByClause(" id limit 1 ");
						List<VehicleCheck> list2 = vehicleCheckMapper.selectByExample(example);
						if(null != list2 && !list2.isEmpty()) {
							check.setStartDate(list2.get(0).getCheckTime());
						}
						newVehicleChecks.add(check);
					}
				}
				checkObject.setVehicleChecks(newVehicleChecks);
				checkObject.setMiles(MathUtil.parseDouble(miles));
				checkObject.setTypName(carBinds.get(0).getCarType());
				checkObject.setLicenseNo(carBinds.get(0).getLicenseNo());
			}
		}
		return checkObject;
	}

	@Override
	public List<VehicleCheckObject> queryDraw(Integer userId,Date startTime ,Date endTime,Integer fleetId,String licenseNo,Integer typeSizeId) {
		
		List<VehicleCheckObject> checkObjects = new ArrayList<VehicleCheckObject>();
		CarBind bind = new CarBind();
		bind.setUserId(userId);
		if(null != fleetId) {
			bind.setFleetId(fleetId);
		}
		if(null != licenseNo && !licenseNo.isEmpty()) {
			bind.setLicenseNo(licenseNo);
		}
		if(null != typeSizeId) {
			bind.setTypeSizeId(typeSizeId);
		}
		List<CarBind> carBinds = carBindMapper.selectDrawCarByUserId(bind);
		for(CarBind carBind:carBinds) {
			List<VehicleCheck> vehicleChecks = vehicleCheckMapper.selectDragCarByProductId(carBind.getProductId(),startTime,endTime);
			List<VehicleCheck> newVehicleChecks = new ArrayList<VehicleCheck>();
			if(!vehicleChecks.isEmpty()) {
				VehicleCheckObject checkObject = new VehicleCheckObject();
				double miles = 0;
				for(VehicleCheck check:vehicleChecks) {
					if(check.getMile()>0) {
						miles += check.getMile();
						newVehicleChecks.add(check);
					}
				}
				checkObject.setVehicleChecks(newVehicleChecks);
				checkObject.setProductId(carBind.getProductId());
				checkObject.setMiles(MathUtil.parseDouble(miles));
				checkObject.setTypName(carBind.getCarType());
				checkObject.setLicenseNo(carBind.getLicenseNo());
				checkObjects.add(checkObject);
			}
		}
		return checkObjects;
	}
	
	
	@Override
	public List<VehicleCheckObject> queryWhole(Integer userId,Date startTime ,Date endTime,Integer fleetId,String licenseNo,Integer typeSizeId) {
		
		List<VehicleCheckObject> checkObjects = new ArrayList<VehicleCheckObject>();
		CarBind bind = new CarBind();
		bind.setUserId(userId);
		if(null != fleetId) {
			bind.setFleetId(fleetId);
		}
		if(null != licenseNo && !licenseNo.isEmpty()) {
			bind.setLicenseNo(licenseNo);
		}
		if(null != typeSizeId) {
			bind.setTypeSizeId(typeSizeId);
		}
		List<CarBind> carBinds = carBindMapper.selectWholeCarByUserId(bind);
		for(CarBind carBind:carBinds) {
			List<VehicleCheck> vehicleChecks = vehicleCheckMapper.selectDragCarByProductId(carBind.getProductId(),startTime,endTime);
			List<VehicleCheck> newVehicleChecks = new ArrayList<VehicleCheck>();
			if(!vehicleChecks.isEmpty()) {
				VehicleCheckObject checkObject = new VehicleCheckObject();
				double miles = 0;
				for(VehicleCheck check:vehicleChecks) {
					if(check.getMile()>0) {
						miles += check.getMile();
						newVehicleChecks.add(check);
					}
				}
				checkObject.setVehicleChecks(newVehicleChecks);
				checkObject.setProductId(carBind.getProductId());
				checkObject.setMiles(MathUtil.parseDouble(miles));
				checkObject.setTypName(carBind.getCarType());
				checkObject.setLicenseNo(carBind.getLicenseNo());
				checkObjects.add(checkObject);
			}
		}
		return checkObjects;
	}
	
	@Override
	public List<Car> selectCheckCarDispatch(Integer userId) {
		List<Car> cars = carMapper.selectCheckCarDispatch(userId);
		if(null != cars && !cars.isEmpty()) {
			for(Car car:cars) {
				Car c = carMapper.findCheckCar(car.getId());
				if(c != null) {
					car.setState(c.getState());
					car.setMatchLicenseNo(c.getMatchLicenseNo());
					car.setMatchCarId(c.getMatchCarId());
				}
			}
			Collections.sort(cars, new Comparator<Car>(){  
	            public int compare(Car car1, Car car2) {  
	              
	                if(car1.getState() < car2.getState()){  
	                    return 1;  
	                }  
	                if(car1.getState() == car2.getState()){  
	                    return 0;  
	                }  
	                return -1;  
	            }  
	        });  
		}
		return cars;
	}
	
}
