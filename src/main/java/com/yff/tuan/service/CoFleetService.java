package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.CoFleet;
import com.yff.tuan.model.CoFleetExample;
import com.yff.tuan.util.Page;

public interface CoFleetService {
	public abstract CoFleet find(CoFleet coFleet);
	
	public abstract CoFleet findByPrimaryKey(Integer id);

	public abstract int insert(CoFleet coFleet);
	
	public abstract int updateByPrimaryKey(CoFleet coFleet);
	
	public abstract int updateByPrimaryKeySelective(CoFleet coFleet);
	
	public abstract int delByPrimaryKey(Integer id);
	
	public abstract List<CoFleet> query(CoFleetExample example);
	
	
	public abstract Page<CoFleet> queryByPage(CoFleetExample example,Integer pageNum);
}
