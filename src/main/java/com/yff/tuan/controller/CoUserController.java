package com.yff.tuan.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yff.tuan.model.CoUser;
import com.yff.tuan.model.CoUserExample;
import com.yff.tuan.service.CoUserService;
import com.yff.tuan.util.Page;

@Controller
@RequestMapping("/couser")
public class CoUserController {
	@Autowired CoUserService coUserService;
	
	@RequestMapping("/list")
	public String query(Model model,Integer pageNum,HttpServletRequest request,HttpServletResponse response) {
		Page<CoUser> page = coUserService.queryByPage(new CoUserExample(), null == pageNum ? 1 : pageNum);
		model.addAttribute("page", page);
		return "/couser_list";
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public Object add(Model model,@RequestBody CoUser coUser,HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map = new HashMap<String,Object>();
		int i = coUserService.insert(coUser);
		map.put("add", i>0?true:false);
		return map;
	}
	
	
}
