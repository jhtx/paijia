package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.CarBindMapper;
import com.yff.tuan.mapper.CarBindTyreMapper;
import com.yff.tuan.mapper.CarBrandMapper;
import com.yff.tuan.mapper.CarFenceMapper;
import com.yff.tuan.mapper.CarFiTyreMapper;
import com.yff.tuan.mapper.CarMapper;
import com.yff.tuan.mapper.CarModelMapper;
import com.yff.tuan.mapper.CarSeTyreMapper;
import com.yff.tuan.mapper.CarThTyreMapper;
import com.yff.tuan.mapper.CarTypeSizeMapper;
import com.yff.tuan.mapper.TireCheckMapper;
import com.yff.tuan.mapper.VehicleCheckMapper;
import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarBind;
import com.yff.tuan.model.CarBindExample;
import com.yff.tuan.model.CarBindTyre;
import com.yff.tuan.model.CarBindTyreExample;
import com.yff.tuan.model.CarBrand;
import com.yff.tuan.model.CarExample;
import com.yff.tuan.model.CarExample.Criteria;
import com.yff.tuan.model.CarFence;
import com.yff.tuan.model.CarFenceExample;
import com.yff.tuan.model.CarFiTyre;
import com.yff.tuan.model.CarModel;
import com.yff.tuan.model.CarSeTyre;
import com.yff.tuan.model.CarThTyre;
import com.yff.tuan.model.CarTypeSize;
import com.yff.tuan.model.TireCheck;
import com.yff.tuan.model.TireCheckExample;
import com.yff.tuan.model.VehicleCheck;
import com.yff.tuan.model.VehicleCheckExample;
import com.yff.tuan.service.BindService;
import com.yff.tuan.service.CarService;
import com.yff.tuan.service.FenceService;
import com.yff.tuan.util.Page;

@Component
public class FenceServiceImpl implements FenceService {
    @Autowired
    CarFenceMapper fenceMapper;
	
	@Override
	public CarFence findCarFence(CarFenceExample example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CarFence> queryCarFence(CarFenceExample example) {
		return fenceMapper.selectByExample(example);
	}

	@Override
	public boolean insert(CarFence carFence) {
		int insert = fenceMapper.insert(carFence);
		return insert>0?true:false;
	}

	@Override
	public boolean update(CarFence carFence) {
		int update = fenceMapper.updateByPrimaryKey(carFence);
		return update>0?true:false;
	}

	@Override
	public List<VehicleCheck> queryVehicleCheck(String productId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TireCheck> queryTireCheck(String productId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CarFence findByPrimaryKey(Integer id) {
		return fenceMapper.selectByPrimaryKey(id);
	}

	@Override
	public boolean delete(Integer id) {
		return fenceMapper.deleteByPrimaryKey(id)>0?true:false;
	}
	
	
}
