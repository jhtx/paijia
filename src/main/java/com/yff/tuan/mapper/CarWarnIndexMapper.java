package com.yff.tuan.mapper;

import com.yff.tuan.model.CarWarnIndex;
import com.yff.tuan.model.CarWarnIndexExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CarWarnIndexMapper {
    int countByExample(CarWarnIndexExample example);

    int deleteByExample(CarWarnIndexExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CarWarnIndex record);

    int insertSelective(CarWarnIndex record);

    List<CarWarnIndex> selectByExample(CarWarnIndexExample example);

    CarWarnIndex selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CarWarnIndex record, @Param("example") CarWarnIndexExample example);

    int updateByExample(@Param("record") CarWarnIndex record, @Param("example") CarWarnIndexExample example);

    int updateByPrimaryKeySelective(CarWarnIndex record);

    int updateByPrimaryKey(CarWarnIndex record);
}