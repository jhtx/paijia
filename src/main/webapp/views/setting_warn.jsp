<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (int)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>卡客途安轮卫管家</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<%=path %>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/google.css" rel="stylesheet">
<link href="<%=path %>/css/style.css" rel="stylesheet" type="text/css">
<link href="<%=path %>/css/font-awesome.css" rel="stylesheet">
<link rel="apple-touch-icon" sizes="76x76" href="<%=path %>/img/logo.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=path %>/img/logo.png">
<style type="text/css">
html,body{
min-height:100%;
}
</style>
</head>
<body style="margin:0 auto;height: 100%" id="body">
<!-- header -->
<jsp:include page="header.jsp"></jsp:include>
<!-- main -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span12">
          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> 
              <i class="icon-th-list"></i>
              <h3>胎温胎压电压</h3>
              <!-- <button type="submit" class="btn btn-primary" onclick="" style="float: right;margin-top:6px;margin-right:5px;">添加</button> -->
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
            	<div class="tabbable">
					<div class="tab-content">
						<form id="form01" class="form-horizontal" method="POST">
								<br />
								<div class="control-group">											
									<label class="control-label" for="name">标准胎压<span style="color: red">*</span></label>
									<div class="controls">
										<div class="input-prepend input-append" style="width: 100%">
											<input type="text" class="span6" id="pressure" name="pressure" style="float:left" value="${warn.pressure }"><span class="add-on" style="float:left">Kpa</span>
										</div>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="no">胎温高提醒<span style="color: red">*</span></label>
									<div class="controls">
										<div class="input-prepend input-append" style="width: 100%">
											<input type="text" class="span6" id="temperature" name="temperature" style="float:left" value="${warn.temperature }"><span class="add-on" style="float:left">℃</span>
										</div>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="address">胎温高报警<span style="color: red">*</span></label>
									<div class="controls">
										<div class="input-prepend input-append" style="width: 100%">
											<input type="text" class="span6" id="temperatureWarning" name="temperatureWarning" style="float:left" value="${warn.temperatureWarning }"><span class="add-on" style="float:left">℃</span>
										</div>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="contactor">低电压提醒<span style="color: red">*</span></label>
									<div class="controls">
										<div class="input-prepend input-append" style="width: 100%">
											<input type="text" class="span6" id="charge" name="charge" style="float:left" value="${warn.charge }"><span class="add-on" style="float:left">V</span>
										</div>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">											
									<label class="control-label" for="phone">低电压报警<span style="color: red">*</span></label>
									<div class="controls">
										<div class="input-prepend input-append" style="width: 100%">
											<input type="text" class="span6" id="chargeWarning" name="chargeWarning" style="float:left" value="${warn.chargeWarning }"><span class="add-on" style="float:left">V</span>
										</div>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">	
									${msg }										
								</div> 
								<div class="form-actions" style="text-align: center;">
									<input type="hidden" id="id" name="id" value="${warn.id }">
									<button type="button"  id="sub" class="btn btn-primary">提交</button> 
									<button type="button" class="btn" onclick="location.href='<%=path%>/setting/query.html'">返回</button>
								</div> <!-- /form-actions -->
						</form>
					</div>
				</div>
			 </div> <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
  <br>
  <br>
  <br>
</div>
<!-- /main -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<%=path %>/js/jquery-1.7.2.min.js"></script> 
<script src="<%=path %>/js/excanvas.min.js"></script>
<script src="<%=path %>/js/bootstrap.js"></script>
<script src="<%=path %>/js/base.js"></script>
<script type="text/javascript">
var minheight = $(window).height();
$("#body").css({
  'min-height':minheight +'px'
})
$("#sub").click(function(){
	var patrn = /^\d+(\.\d+)?$/;
	var pressure = $("#pressure").val();
	var temperature = $("#temperature").val();
	var temperatureWarning = $("#temperatureWarning").val();
	var charge = $("#charge").val();
	var chargeWarning = $("#chargeWarning").val();
	if(pressure=="" || !patrn.test(pressure)){
		alert("请正确输入标准胎压");
		return;
	}
	if(temperature=="" || !patrn.test(temperature)){
		alert("请正确输入胎温高提醒");
		return;
	}
	if(temperatureWarning=="" || !patrn.test(temperatureWarning)){
		alert("请正确输入胎温高报警");
		return;
	}
	if(charge=="" || !patrn.test(charge)){
		alert("请正确输入低电压提醒");
		return;
	}
	if(chargeWarning=="" || !patrn.test(chargeWarning)){
		alert("请正确输入低电压报警");
		return;
	}
	$.ajax({
		    url:'<%=path %>/setting/warn/add.html',
		    type:'POST', //GET
		    async:true,    //或false,是否异步
		    data: $("#form01").serializeArray(),
		    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
		    success:function(data){
		        if(data.resCode=="0"){
		        	if($("#id").val() != ""){
		        		alert("修改成功！");
		        	}else{
		        		alert("添加成功！");
		        	}
		        	window.location.href="<%=path %>/setting/query.html";
		        }else{
		        	 $("#msg").html(data.resMsg);
		        }
		    },
		    error:function(xhr){},
		});
})
</script>
</body>
</html>