#!/bin/sh

# $0 - shell file; $1 - test/mock/stage/prod/prodaws

# --- setup ---

TIMESTAMP=`date +%Y%m%d-%H%M`
PROJECT_PATH=`cd \`dirname $0\`;cd ..;pwd`
PACKAGE_NAME="paijia.${1}.${TIMESTAMP}.zip"

cd ${PROJECT_PATH}
rm -rf output
mkdir output


# --- compile and pack ---

mvn clean package -P ${1}


# --- version ---

cp ${PROJECT_PATH}/target/paijia.war output/${PACKAGE_NAME}
zip output/${PACKAGE_NAME} history.txt
cd output

VERSION_INFO=""

if [ -d "${PROJECT_PATH}/.git" ];then
    GIT_STATUS=`git status | grep branch`
    GIT_LOG=`git log --pretty=oneline -1`
    VERSION_INFO="${GIT_LOG}\n\n${GIT_STATUS}\n\n"
fi

echo "${VERSION_INFO}${PACKAGE_NAME}" >> ./version.txt
zip ${PACKAGE_NAME} version.txt
