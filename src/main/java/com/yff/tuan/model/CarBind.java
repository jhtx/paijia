package com.yff.tuan.model;

import java.util.List;

public class CarBind {
    private Integer id;

    private Integer carId;

    private String productId;
    
    private String licenseNo;
    
    private String carType;
    
    private Integer userId;
    
    private List<CarBindTyre> carBindTyres;
    
    private String lienseNo;
    private Integer typeSizeId;
    private Integer fleetId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

	public List<CarBindTyre> getCarBindTyres() {
		return carBindTyres;
	}

	public void setCarBindTyres(List<CarBindTyre> carBindTyres) {
		this.carBindTyres = carBindTyres;
	}

	public String getLicenseNo() {
		return licenseNo;
	}

	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getLienseNo() {
		return lienseNo;
	}

	public Integer getTypeSizeId() {
		return typeSizeId;
	}

	public Integer getFleetId() {
		return fleetId;
	}

	public void setLienseNo(String lienseNo) {
		this.lienseNo = lienseNo;
	}

	public void setTypeSizeId(Integer typeSizeId) {
		this.typeSizeId = typeSizeId;
	}

	public void setFleetId(Integer fleetId) {
		this.fleetId = fleetId;
	}
    
    
}