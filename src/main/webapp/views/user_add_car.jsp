<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (int)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>卡客途安轮卫管家</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<%=path %>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/google.css" rel="stylesheet">
<link href="<%=path %>/css/style.css" rel="stylesheet" type="text/css">
<link href="<%=path %>/css/font-awesome.css" rel="stylesheet">
<link rel="apple-touch-icon" sizes="76x76" href="<%=path %>/img/logo.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=path %>/img/logo.png">
<style type="text/css">
html,body{
min-height:100%;
}
</style>
</head>
<body style="margin:0 auto;height: 100%" id="body">
<!-- header -->
<jsp:include page="header.jsp"></jsp:include>
<!-- main -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span12">
          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> 
              <i class="icon-th-list"></i>
              <h3>分公司</h3>
              <!-- <button type="submit" class="btn btn-primary" onclick="" style="float: right;margin-top:6px;margin-right:5px;">添加</button> -->
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
            	<div class="tabbable">
					<!-- <ul class="nav nav-tabs">
					  <li>
					    <a href="#formcontrols" data-toggle="tab">Form Controls</a>
					  </li>
					  <li  class="active"><a href="#jscontrols" data-toggle="tab">JS Controls</a></li>
					</ul>
					<br> -->
					<div class="tab-content">
						<form id="form01" class="form-horizontal" method="POST">
								<table class="table table-striped table-bordered">
				                	<tr>
					                    <td style="font: bold;">账号</td>
					                    <td>${user.name }</td>
					                    <td style="font: bold;">姓名</td>
					                    <td>${user.nickName }</td>
				                  	</tr>
				                	<tr>
					                    <td style="font: bold;">车队</td>
					                    <td>${user.fleetName }</td>
					                    <td style="font: bold;">职位</td>
					                    <td>${user.positionName }</td>
				                  	</tr>
				                  	
				                  	<tr>
					                    <td style="font: bold;">请选择权限</td>
					                    <td colspan="3">
					                    	<c:forEach items="${cars }" var="c">
							                    		<div class="control-group">											
				                                            <div class="controls">
					                                            <label class="checkbox inline">
					                                              <input name="checkcar" value="${c.id }"  type="checkbox" <c:if test="${c.checked==1}">checked</c:if>  >${c.licenseNo }
					                                            </label>
				                                          </div>
														</div>
					                    	</c:forEach>
					                    </td>
				                  	</tr>
				                  	<tr>
				                 	 	<td class="td-actions" colspan="4" style="text-align: center;">
				                 	 		<input type="hidden" name="id" id="id" value="${user.id }">
					                    	<button type="button" class="btn btn-small btn-success" id="sub">保存</button>
					                    	<button type="button" class="btn" onclick="location.href='<%=path%>/user/query.html'">取消</button>
				                		<td>
				                	</tr>
				              </table>
						</form>
					</div>
				</div>
			 </div> <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
  <br>
  <br>
  <br>
</div>
<!-- /main -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<%=path %>/js/jquery-1.7.2.min.js"></script> 
<script src="<%=path %>/js/excanvas.min.js"></script>
<script src="<%=path %>/js/bootstrap.js"></script>
<script src="<%=path %>/js/base.js"></script>
<script type="text/javascript">
var minheight = $(window).height();
$("#body").css({
  'min-height':minheight +'px'
})
$("#sub").click(function(){
	var checkcar = $("input[name='checkcar']");
	var userId = $("#id").val();
	var cAdd = [];
	for(var i=0;i<checkcar.length;i++){
		console.log($(checkcar[i]).val());
		console.log($(checkcar[i]).is(':checked'));
		if($(checkcar[i]).is(':checked')){
			var carId = $(checkcar[i]).val();
			var obj = {userId:userId,carId:carId};
			cAdd.push(obj);
		}
	}
	var json = {userId:userId,cars:cAdd};
	console.log(JSON.stringify(json));
	$.ajax({
		url:'<%=path%>/user/rightsSave',
		   type:'POST', //GET
		   async:true,    //或false,是否异步
		   contentType:"application/json",
		   data:JSON.stringify(json),
		   dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
		   success:function(data){
		       console.log(data);
		       if(data.add==true){
		    	alert("保存成功！");
		       	window.location.href="<%=path%>/user/query.html";
			   }
			},
			error : function(xhr) {
				console.log('错误');
				console.log(xhr);
			},
		});
})
</script>
</body>
</html>