package com.yff.tuan.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.google.gson.Gson;
import com.yff.tuan.api.ApiResult;
import com.yff.tuan.model.Car;
import com.yff.tuan.model.Clabel;
import com.yff.tuan.model.ClabelExample;
import com.yff.tuan.model.ClabelType;
import com.yff.tuan.model.ClabelTypeExample;
import com.yff.tuan.model.Cmodel;
import com.yff.tuan.model.CmodelExample;
import com.yff.tuan.model.CoUser;
import com.yff.tuan.model.Report;
import com.yff.tuan.service.CarService;
import com.yff.tuan.service.CoUserService;
import com.yff.tuan.service.LabelService;
import com.yff.tuan.service.ReportService;
import com.yff.tuan.util.DateUtil;
import com.yff.tuan.util.ImageUtil;


@Controller
@RequestMapping("/report")
public class ReportController {
	@Autowired ReportService reportService;
	@Autowired CarService carService;
	@Autowired CoUserService userService;
	@Autowired LabelService labelService;
	
	@Value("${config.ftp_address}")  
    public  String FTP_ADDRESS; 
      
    @Value("${config.ftp_user}")  
    public  String USERNAME;    
      
    @Value("${config.ftp_pass}")  
    public  String PASSWORD;   
	
	
	@RequestMapping("/query")
	public String query(Model model,Integer start,HttpServletRequest request,HttpServletResponse response) {
		int userId = (int) request.getSession().getAttribute("userId");
		Report report = new Report();
		if(null == start) {
			report.setStart(0);
		}else {
			report.setStart(start);
		}
		report.setUserId(userId);
		model.addAttribute("reports", new Gson().toJson(reportService.query(report)));
		model.addAttribute("start", start);
		return "/report";
	}
	
	@RequestMapping("/toadd")
	public String toadd(Model model,HttpServletRequest request,HttpServletResponse response) {
		int userId = (int) request.getSession().getAttribute("userId");
		int role = (int) request.getSession().getAttribute("role");
		model.addAttribute("cars", carService.queryUserCars(role, userId));
		CoUser user = userService.findByPrimaryKey(userId);
		model.addAttribute("user", user);
		ClabelExample example = new ClabelExample();
		if(role==2) {
			example.createCriteria().andUserIdEqualTo(user.getParentId());
		}else {
			example.createCriteria().andUserIdEqualTo(userId);
		}
		List<Clabel> clabels = labelService.query(example);
		model.addAttribute("clabels", clabels);
		List<ClabelType> clabelTypes = new ArrayList<ClabelType>();
		if(null != clabels && !clabels.isEmpty()) {
			clabelTypes = labelService.queryClabelType(clabels.get(0).getId());
		}
		model.addAttribute("clabelTypes", clabelTypes);
		model.addAttribute("msg", new Gson().toJson(false));
		return "/report_add";
	}
	
	@RequestMapping("/queryLabelType")
	@ResponseBody
	public Object queryLabelType(Model model,Integer labelId,HttpServletRequest request,HttpServletResponse response){
		List<ClabelType> clabelTypes = new ArrayList<ClabelType>();
		if(null != labelId) {
			clabelTypes = labelService.queryClabelType(labelId);
		}
		return ApiResult.SUCCESS(clabelTypes);
	}
	
	@RequestMapping("/add")
	public String add(Model model,@RequestParam("carId") Integer carId,
								  @RequestParam("labelId") Integer labelId,
								  @RequestParam("labelTypeId") Integer labelTypeId,
								  @RequestParam("content") String content,
								  @RequestParam("myfile") MultipartFile myfile,
								  HttpServletRequest request,HttpServletResponse response) {
		
		int userId = (int) request.getSession().getAttribute("userId");
		Report report = new Report();
		report.setUserId(userId);
		report.setCarId(carId);
		report.setCheckDate(DateUtil.getDate(new Date(),DateUtil.FORMAT_YYMMDD000000));
		report.setContent(content);
		report.setLabelId(labelId);
		report.setLabelTypeId(labelTypeId);
		if(null != myfile) {
			try {
				report.setFilePath(ImageUtil.getImageStr(myfile.getInputStream()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		report.setState(0);
		int role = (int) request.getSession().getAttribute("role");
		if(role==2){
			CoUser user = userService.findByPrimaryKey(userId);
			report.setParentId(user.getParentId());
		}else{
			report.setParentId(userId);
		}
		model.addAttribute("msg", new Gson().toJson(reportService.insert(report)));
		model.addAttribute("cars", carService.queryUserCars(role, userId));
		CoUser user = userService.findByPrimaryKey(userId);
		model.addAttribute("user", user);
		ClabelExample example = new ClabelExample();
		if(role==2) {
			example.createCriteria().andUserIdEqualTo(user.getParentId());
		}else {
			example.createCriteria().andUserIdEqualTo(userId);
		}
		List<Clabel> clabels = labelService.query(example);
		model.addAttribute("clabels", clabels);
		List<ClabelType> clabelTypes = new ArrayList<ClabelType>();
		if(null != clabels && !clabels.isEmpty()) {
			clabelTypes = labelService.queryClabelType(clabels.get(0).getId());
		}
		model.addAttribute("clabelTypes", clabelTypes);
		return "/report_add";
	}
	@RequestMapping("/detail")
	public String detail(Model model,Integer id, HttpServletRequest request,HttpServletResponse response) {
		
		int userId = (int) request.getSession().getAttribute("userId");
		CoUser user = userService.findByPrimaryKey(userId);
		model.addAttribute("user", user);
		model.addAttribute("report", reportService.findByPrimaryKey(id));
		return "/report_detail";
	}
	
}
