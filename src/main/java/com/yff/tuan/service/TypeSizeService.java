package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.CtypeSize;
import com.yff.tuan.model.CtypeSizeExample;
import com.yff.tuan.util.Page;

public interface TypeSizeService {
	public abstract CtypeSize find(CtypeSize ctypeSize);
	
	public abstract CtypeSize findByPrimaryKey(Integer id);

	public abstract int insert(CtypeSize ctypeSize);
	
	public abstract int updateByPrimaryKeySelective(CtypeSize ctypeSize);
	
	public abstract int delByPrimaryKey(Integer id);
	
	public abstract List<CtypeSize> query(CtypeSizeExample example);
	
	
	public abstract Page<CtypeSize> queryByPage(CtypeSizeExample example,Integer pageNum);
}
