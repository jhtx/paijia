package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarExample;
import com.yff.tuan.model.CoPosition;
import com.yff.tuan.model.CoPositionExample;
import com.yff.tuan.util.Page;

public interface PositionService {
	public abstract CoPosition findByPrimaryKey(Integer id);
	
	public abstract boolean save(CoPosition position);
	
	public abstract boolean update(CoPosition position);
	
	public abstract List<CoPosition> query(CoPositionExample example);
}
