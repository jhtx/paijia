package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.ClabelMapper;
import com.yff.tuan.mapper.ClabelTypeMapper;
import com.yff.tuan.mapper.ReportMapper;
import com.yff.tuan.mapper.ReportRecordMapper;
import com.yff.tuan.model.Clabel;
import com.yff.tuan.model.ClabelExample;
import com.yff.tuan.model.ClabelType;
import com.yff.tuan.model.ClabelTypeExample;
import com.yff.tuan.model.Report;
import com.yff.tuan.model.ReportExample;
import com.yff.tuan.model.ReportRecord;
import com.yff.tuan.model.ReportRecordExample;
import com.yff.tuan.model.ReportRecordExample.Criteria;
import com.yff.tuan.service.LabelService;
import com.yff.tuan.service.ReportService;
import com.yff.tuan.util.DateUtil;

@Component
public class ReportServiceImpl implements ReportService {
	@Autowired
	ReportMapper reportMapper;
	@Autowired
	ReportRecordMapper reportRecordMapper;
	
	
	@Override
	public List<Report> query(Report report) {
		List<Report> reports = reportMapper.query(report);
		for(Report report2:reports){
			if(null != report2.getCheckDate()){
				report2.setTime(DateUtil.getFormatDate(report2.getCheckDate(), DateUtil.FORMAT_YYMMDD));
			}
		}
		return reports;
	}
	@Override
	public Report find(Report report) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Report findByPrimaryKey(Integer id) {
		return reportMapper.selectByPrimaryKey(id);
	}
	@Override
	public Report selectByPrimaryKey(Integer id) {
		return null;
	}
	@Override
	public boolean insert(Report report) {
		int insert = reportMapper.insert(report);
		if(insert>0){
			ReportRecordExample recordExample = new ReportRecordExample();
			Criteria createCriteria = recordExample.createCriteria();
			createCriteria.andParentIdEqualTo(report.getParentId());
			createCriteria.andCheckDateEqualTo(report.getCheckDate());
			int i = reportRecordMapper.countByExample(recordExample);
			if(i==0){
				ReportRecord reportRecord = new ReportRecord();
				reportRecord.setParentId(report.getParentId());
				reportRecord.setCheckDate(report.getCheckDate());
				reportRecordMapper.insert(reportRecord);
			}
		}
		return true;
	}
	@Override
	public List<ReportRecord> query(Integer userId,Integer start) {
		ReportRecordExample example = new ReportRecordExample();
		Criteria createCriteria = example.createCriteria();
		createCriteria.andParentIdEqualTo(userId);
		example.setOrderByClause(" id desc limit "+start+",10 ");
		List<ReportRecord> records = reportRecordMapper.selectByExample(example);
		for(ReportRecord record:records){
			if(null != record.getCheckDate()){
				record.setTime(DateUtil.getFormatDate(record.getCheckDate(), DateUtil.FORMAT_YYMMDD));
			}
		}
		return records;
	}
	
	@Override
	public List<Report> queryUseReports(Integer userId,Integer recordId,Integer fleetId ,ReportRecord record) {
		if(null != record){
			Report report = new Report();
			report.setParentId(userId);
			report.setCheckDate(record.getCheckDate());
			report.setFleetId(fleetId);
			return reportMapper.queryUseReports(report);
		}
		return null;
	}
	@Override
	public ReportRecord findReportRecordByPrimaryKey(Integer id) {
		return reportRecordMapper.selectByPrimaryKey(id);
	}
	

	

}
