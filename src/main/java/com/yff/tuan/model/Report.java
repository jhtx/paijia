package com.yff.tuan.model;

import java.util.Date;

public class Report {
	private Integer id;

    private Integer userId;

    private Integer carId;

    private Integer labelId;

    private Integer labelTypeId;

    private String content;

    private String filePath;

    private Integer parentId;

    private Integer state;

    private Date checkDate;
    
    private String time;
    
    private Integer start;
    private String licenseNo;
    private String labelName;
    private String labelTypeName;
    private String userName;
    private String userPhone;
    private Integer fleetId;
    
	public Integer getId() {
		return id;
	}
	public Integer getUserId() {
		return userId;
	}
	public Integer getCarId() {
		return carId;
	}
	public Integer getLabelId() {
		return labelId;
	}
	public Integer getLabelTypeId() {
		return labelTypeId;
	}
	public String getContent() {
		return content;
	}
	public String getFilePath() {
		return filePath;
	}
	public Integer getParentId() {
		return parentId;
	}
	public Integer getState() {
		return state;
	}
	public Date getCheckDate() {
		return checkDate;
	}
	public Integer getStart() {
		return start;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public void setCarId(Integer carId) {
		this.carId = carId;
	}
	public void setLabelId(Integer labelId) {
		this.labelId = labelId;
	}
	public void setLabelTypeId(Integer labelTypeId) {
		this.labelTypeId = labelTypeId;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getLabelName() {
		return labelName;
	}
	public void setLabelName(String labelName) {
		this.labelName = labelName;
	}
	public String getLabelTypeName() {
		return labelTypeName;
	}
	public void setLabelTypeName(String labelTypeName) {
		this.labelTypeName = labelTypeName;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public Integer getFleetId() {
		return fleetId;
	}
	public void setFleetId(Integer fleetId) {
		this.fleetId = fleetId;
	}
	
}