package com.yff.tuan.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CarExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CarExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNoIsNull() {
            addCriterion("no is null");
            return (Criteria) this;
        }

        public Criteria andNoIsNotNull() {
            addCriterion("no is not null");
            return (Criteria) this;
        }

        public Criteria andNoEqualTo(String value) {
            addCriterion("no =", value, "no");
            return (Criteria) this;
        }

        public Criteria andNoNotEqualTo(String value) {
            addCriterion("no <>", value, "no");
            return (Criteria) this;
        }

        public Criteria andNoGreaterThan(String value) {
            addCriterion("no >", value, "no");
            return (Criteria) this;
        }

        public Criteria andNoGreaterThanOrEqualTo(String value) {
            addCriterion("no >=", value, "no");
            return (Criteria) this;
        }

        public Criteria andNoLessThan(String value) {
            addCriterion("no <", value, "no");
            return (Criteria) this;
        }

        public Criteria andNoLessThanOrEqualTo(String value) {
            addCriterion("no <=", value, "no");
            return (Criteria) this;
        }

        public Criteria andNoLike(String value) {
            addCriterion("no like", value, "no");
            return (Criteria) this;
        }

        public Criteria andNoNotLike(String value) {
            addCriterion("no not like", value, "no");
            return (Criteria) this;
        }

        public Criteria andNoIn(List<String> values) {
            addCriterion("no in", values, "no");
            return (Criteria) this;
        }

        public Criteria andNoNotIn(List<String> values) {
            addCriterion("no not in", values, "no");
            return (Criteria) this;
        }

        public Criteria andNoBetween(String value1, String value2) {
            addCriterion("no between", value1, value2, "no");
            return (Criteria) this;
        }

        public Criteria andNoNotBetween(String value1, String value2) {
            addCriterion("no not between", value1, value2, "no");
            return (Criteria) this;
        }

        public Criteria andFleetIdIsNull() {
            addCriterion("fleet_id is null");
            return (Criteria) this;
        }

        public Criteria andFleetIdIsNotNull() {
            addCriterion("fleet_id is not null");
            return (Criteria) this;
        }

        public Criteria andFleetIdEqualTo(Integer value) {
            addCriterion("fleet_id =", value, "fleetId");
            return (Criteria) this;
        }

        public Criteria andFleetIdNotEqualTo(Integer value) {
            addCriterion("fleet_id <>", value, "fleetId");
            return (Criteria) this;
        }

        public Criteria andFleetIdGreaterThan(Integer value) {
            addCriterion("fleet_id >", value, "fleetId");
            return (Criteria) this;
        }

        public Criteria andFleetIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("fleet_id >=", value, "fleetId");
            return (Criteria) this;
        }

        public Criteria andFleetIdLessThan(Integer value) {
            addCriterion("fleet_id <", value, "fleetId");
            return (Criteria) this;
        }

        public Criteria andFleetIdLessThanOrEqualTo(Integer value) {
            addCriterion("fleet_id <=", value, "fleetId");
            return (Criteria) this;
        }

        public Criteria andFleetIdIn(List<Integer> values) {
            addCriterion("fleet_id in", values, "fleetId");
            return (Criteria) this;
        }

        public Criteria andFleetIdNotIn(List<Integer> values) {
            addCriterion("fleet_id not in", values, "fleetId");
            return (Criteria) this;
        }

        public Criteria andFleetIdBetween(Integer value1, Integer value2) {
            addCriterion("fleet_id between", value1, value2, "fleetId");
            return (Criteria) this;
        }

        public Criteria andFleetIdNotBetween(Integer value1, Integer value2) {
            addCriterion("fleet_id not between", value1, value2, "fleetId");
            return (Criteria) this;
        }

        public Criteria andLicenseNoIsNull() {
            addCriterion("license_no is null");
            return (Criteria) this;
        }

        public Criteria andLicenseNoIsNotNull() {
            addCriterion("license_no is not null");
            return (Criteria) this;
        }

        public Criteria andLicenseNoEqualTo(String value) {
            addCriterion("license_no =", value, "licenseNo");
            return (Criteria) this;
        }

        public Criteria andLicenseNoNotEqualTo(String value) {
            addCriterion("license_no <>", value, "licenseNo");
            return (Criteria) this;
        }

        public Criteria andLicenseNoGreaterThan(String value) {
            addCriterion("license_no >", value, "licenseNo");
            return (Criteria) this;
        }

        public Criteria andLicenseNoGreaterThanOrEqualTo(String value) {
            addCriterion("license_no >=", value, "licenseNo");
            return (Criteria) this;
        }

        public Criteria andLicenseNoLessThan(String value) {
            addCriterion("license_no <", value, "licenseNo");
            return (Criteria) this;
        }

        public Criteria andLicenseNoLessThanOrEqualTo(String value) {
            addCriterion("license_no <=", value, "licenseNo");
            return (Criteria) this;
        }

        public Criteria andLicenseNoLike(String value) {
            addCriterion("license_no like", value, "licenseNo");
            return (Criteria) this;
        }

        public Criteria andLicenseNoNotLike(String value) {
            addCriterion("license_no not like", value, "licenseNo");
            return (Criteria) this;
        }

        public Criteria andLicenseNoIn(List<String> values) {
            addCriterion("license_no in", values, "licenseNo");
            return (Criteria) this;
        }

        public Criteria andLicenseNoNotIn(List<String> values) {
            addCriterion("license_no not in", values, "licenseNo");
            return (Criteria) this;
        }

        public Criteria andLicenseNoBetween(String value1, String value2) {
            addCriterion("license_no between", value1, value2, "licenseNo");
            return (Criteria) this;
        }

        public Criteria andLicenseNoNotBetween(String value1, String value2) {
            addCriterion("license_no not between", value1, value2, "licenseNo");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Integer value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Integer value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Integer value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Integer value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Integer value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Integer> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Integer> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Integer value1, Integer value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeSizeIdIsNull() {
            addCriterion("type_size_id is null");
            return (Criteria) this;
        }

        public Criteria andTypeSizeIdIsNotNull() {
            addCriterion("type_size_id is not null");
            return (Criteria) this;
        }

        public Criteria andTypeSizeIdEqualTo(Integer value) {
            addCriterion("type_size_id =", value, "typeSizeId");
            return (Criteria) this;
        }

        public Criteria andTypeSizeIdNotEqualTo(Integer value) {
            addCriterion("type_size_id <>", value, "typeSizeId");
            return (Criteria) this;
        }

        public Criteria andTypeSizeIdGreaterThan(Integer value) {
            addCriterion("type_size_id >", value, "typeSizeId");
            return (Criteria) this;
        }

        public Criteria andTypeSizeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("type_size_id >=", value, "typeSizeId");
            return (Criteria) this;
        }

        public Criteria andTypeSizeIdLessThan(Integer value) {
            addCriterion("type_size_id <", value, "typeSizeId");
            return (Criteria) this;
        }

        public Criteria andTypeSizeIdLessThanOrEqualTo(Integer value) {
            addCriterion("type_size_id <=", value, "typeSizeId");
            return (Criteria) this;
        }

        public Criteria andTypeSizeIdIn(List<Integer> values) {
            addCriterion("type_size_id in", values, "typeSizeId");
            return (Criteria) this;
        }

        public Criteria andTypeSizeIdNotIn(List<Integer> values) {
            addCriterion("type_size_id not in", values, "typeSizeId");
            return (Criteria) this;
        }

        public Criteria andTypeSizeIdBetween(Integer value1, Integer value2) {
            addCriterion("type_size_id between", value1, value2, "typeSizeId");
            return (Criteria) this;
        }

        public Criteria andTypeSizeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("type_size_id not between", value1, value2, "typeSizeId");
            return (Criteria) this;
        }

        public Criteria andBrandIdIsNull() {
            addCriterion("brand_id is null");
            return (Criteria) this;
        }

        public Criteria andBrandIdIsNotNull() {
            addCriterion("brand_id is not null");
            return (Criteria) this;
        }

        public Criteria andBrandIdEqualTo(Integer value) {
            addCriterion("brand_id =", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotEqualTo(Integer value) {
            addCriterion("brand_id <>", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdGreaterThan(Integer value) {
            addCriterion("brand_id >", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("brand_id >=", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdLessThan(Integer value) {
            addCriterion("brand_id <", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdLessThanOrEqualTo(Integer value) {
            addCriterion("brand_id <=", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdIn(List<Integer> values) {
            addCriterion("brand_id in", values, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotIn(List<Integer> values) {
            addCriterion("brand_id not in", values, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdBetween(Integer value1, Integer value2) {
            addCriterion("brand_id between", value1, value2, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotBetween(Integer value1, Integer value2) {
            addCriterion("brand_id not between", value1, value2, "brandId");
            return (Criteria) this;
        }

        public Criteria andModelIdIsNull() {
            addCriterion("model_id is null");
            return (Criteria) this;
        }

        public Criteria andModelIdIsNotNull() {
            addCriterion("model_id is not null");
            return (Criteria) this;
        }

        public Criteria andModelIdEqualTo(Integer value) {
            addCriterion("model_id =", value, "modelId");
            return (Criteria) this;
        }

        public Criteria andModelIdNotEqualTo(Integer value) {
            addCriterion("model_id <>", value, "modelId");
            return (Criteria) this;
        }

        public Criteria andModelIdGreaterThan(Integer value) {
            addCriterion("model_id >", value, "modelId");
            return (Criteria) this;
        }

        public Criteria andModelIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("model_id >=", value, "modelId");
            return (Criteria) this;
        }

        public Criteria andModelIdLessThan(Integer value) {
            addCriterion("model_id <", value, "modelId");
            return (Criteria) this;
        }

        public Criteria andModelIdLessThanOrEqualTo(Integer value) {
            addCriterion("model_id <=", value, "modelId");
            return (Criteria) this;
        }

        public Criteria andModelIdIn(List<Integer> values) {
            addCriterion("model_id in", values, "modelId");
            return (Criteria) this;
        }

        public Criteria andModelIdNotIn(List<Integer> values) {
            addCriterion("model_id not in", values, "modelId");
            return (Criteria) this;
        }

        public Criteria andModelIdBetween(Integer value1, Integer value2) {
            addCriterion("model_id between", value1, value2, "modelId");
            return (Criteria) this;
        }

        public Criteria andModelIdNotBetween(Integer value1, Integer value2) {
            addCriterion("model_id not between", value1, value2, "modelId");
            return (Criteria) this;
        }

        public Criteria andMilesIsNull() {
            addCriterion("miles is null");
            return (Criteria) this;
        }

        public Criteria andMilesIsNotNull() {
            addCriterion("miles is not null");
            return (Criteria) this;
        }

        public Criteria andMilesEqualTo(Double value) {
            addCriterion("miles =", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesNotEqualTo(Double value) {
            addCriterion("miles <>", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesGreaterThan(Double value) {
            addCriterion("miles >", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesGreaterThanOrEqualTo(Double value) {
            addCriterion("miles >=", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesLessThan(Double value) {
            addCriterion("miles <", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesLessThanOrEqualTo(Double value) {
            addCriterion("miles <=", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesIn(List<Double> values) {
            addCriterion("miles in", values, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesNotIn(List<Double> values) {
            addCriterion("miles not in", values, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesBetween(Double value1, Double value2) {
            addCriterion("miles between", value1, value2, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesNotBetween(Double value1, Double value2) {
            addCriterion("miles not between", value1, value2, "miles");
            return (Criteria) this;
        }

        public Criteria andLicensingDateIsNull() {
            addCriterion("licensing_date is null");
            return (Criteria) this;
        }

        public Criteria andLicensingDateIsNotNull() {
            addCriterion("licensing_date is not null");
            return (Criteria) this;
        }

        public Criteria andLicensingDateEqualTo(Date value) {
            addCriterion("licensing_date =", value, "licensingDate");
            return (Criteria) this;
        }

        public Criteria andLicensingDateNotEqualTo(Date value) {
            addCriterion("licensing_date <>", value, "licensingDate");
            return (Criteria) this;
        }

        public Criteria andLicensingDateGreaterThan(Date value) {
            addCriterion("licensing_date >", value, "licensingDate");
            return (Criteria) this;
        }

        public Criteria andLicensingDateGreaterThanOrEqualTo(Date value) {
            addCriterion("licensing_date >=", value, "licensingDate");
            return (Criteria) this;
        }

        public Criteria andLicensingDateLessThan(Date value) {
            addCriterion("licensing_date <", value, "licensingDate");
            return (Criteria) this;
        }

        public Criteria andLicensingDateLessThanOrEqualTo(Date value) {
            addCriterion("licensing_date <=", value, "licensingDate");
            return (Criteria) this;
        }

        public Criteria andLicensingDateIn(List<Date> values) {
            addCriterion("licensing_date in", values, "licensingDate");
            return (Criteria) this;
        }

        public Criteria andLicensingDateNotIn(List<Date> values) {
            addCriterion("licensing_date not in", values, "licensingDate");
            return (Criteria) this;
        }

        public Criteria andLicensingDateBetween(Date value1, Date value2) {
            addCriterion("licensing_date between", value1, value2, "licensingDate");
            return (Criteria) this;
        }

        public Criteria andLicensingDateNotBetween(Date value1, Date value2) {
            addCriterion("licensing_date not between", value1, value2, "licensingDate");
            return (Criteria) this;
        }

        public Criteria andInsureDateIsNull() {
            addCriterion("insure_date is null");
            return (Criteria) this;
        }

        public Criteria andInsureDateIsNotNull() {
            addCriterion("insure_date is not null");
            return (Criteria) this;
        }

        public Criteria andInsureDateEqualTo(Date value) {
            addCriterion("insure_date =", value, "insureDate");
            return (Criteria) this;
        }

        public Criteria andInsureDateNotEqualTo(Date value) {
            addCriterion("insure_date <>", value, "insureDate");
            return (Criteria) this;
        }

        public Criteria andInsureDateGreaterThan(Date value) {
            addCriterion("insure_date >", value, "insureDate");
            return (Criteria) this;
        }

        public Criteria andInsureDateGreaterThanOrEqualTo(Date value) {
            addCriterion("insure_date >=", value, "insureDate");
            return (Criteria) this;
        }

        public Criteria andInsureDateLessThan(Date value) {
            addCriterion("insure_date <", value, "insureDate");
            return (Criteria) this;
        }

        public Criteria andInsureDateLessThanOrEqualTo(Date value) {
            addCriterion("insure_date <=", value, "insureDate");
            return (Criteria) this;
        }

        public Criteria andInsureDateIn(List<Date> values) {
            addCriterion("insure_date in", values, "insureDate");
            return (Criteria) this;
        }

        public Criteria andInsureDateNotIn(List<Date> values) {
            addCriterion("insure_date not in", values, "insureDate");
            return (Criteria) this;
        }

        public Criteria andInsureDateBetween(Date value1, Date value2) {
            addCriterion("insure_date between", value1, value2, "insureDate");
            return (Criteria) this;
        }

        public Criteria andInsureDateNotBetween(Date value1, Date value2) {
            addCriterion("insure_date not between", value1, value2, "insureDate");
            return (Criteria) this;
        }

        public Criteria andCheckDateIsNull() {
            addCriterion("check_date is null");
            return (Criteria) this;
        }

        public Criteria andCheckDateIsNotNull() {
            addCriterion("check_date is not null");
            return (Criteria) this;
        }

        public Criteria andCheckDateEqualTo(Date value) {
            addCriterion("check_date =", value, "checkDate");
            return (Criteria) this;
        }

        public Criteria andCheckDateNotEqualTo(Date value) {
            addCriterion("check_date <>", value, "checkDate");
            return (Criteria) this;
        }

        public Criteria andCheckDateGreaterThan(Date value) {
            addCriterion("check_date >", value, "checkDate");
            return (Criteria) this;
        }

        public Criteria andCheckDateGreaterThanOrEqualTo(Date value) {
            addCriterion("check_date >=", value, "checkDate");
            return (Criteria) this;
        }

        public Criteria andCheckDateLessThan(Date value) {
            addCriterion("check_date <", value, "checkDate");
            return (Criteria) this;
        }

        public Criteria andCheckDateLessThanOrEqualTo(Date value) {
            addCriterion("check_date <=", value, "checkDate");
            return (Criteria) this;
        }

        public Criteria andCheckDateIn(List<Date> values) {
            addCriterion("check_date in", values, "checkDate");
            return (Criteria) this;
        }

        public Criteria andCheckDateNotIn(List<Date> values) {
            addCriterion("check_date not in", values, "checkDate");
            return (Criteria) this;
        }

        public Criteria andCheckDateBetween(Date value1, Date value2) {
            addCriterion("check_date between", value1, value2, "checkDate");
            return (Criteria) this;
        }

        public Criteria andCheckDateNotBetween(Date value1, Date value2) {
            addCriterion("check_date not between", value1, value2, "checkDate");
            return (Criteria) this;
        }

        public Criteria andScrapDateIsNull() {
            addCriterion("scrap_date is null");
            return (Criteria) this;
        }

        public Criteria andScrapDateIsNotNull() {
            addCriterion("scrap_date is not null");
            return (Criteria) this;
        }

        public Criteria andScrapDateEqualTo(Date value) {
            addCriterion("scrap_date =", value, "scrapDate");
            return (Criteria) this;
        }

        public Criteria andScrapDateNotEqualTo(Date value) {
            addCriterion("scrap_date <>", value, "scrapDate");
            return (Criteria) this;
        }

        public Criteria andScrapDateGreaterThan(Date value) {
            addCriterion("scrap_date >", value, "scrapDate");
            return (Criteria) this;
        }

        public Criteria andScrapDateGreaterThanOrEqualTo(Date value) {
            addCriterion("scrap_date >=", value, "scrapDate");
            return (Criteria) this;
        }

        public Criteria andScrapDateLessThan(Date value) {
            addCriterion("scrap_date <", value, "scrapDate");
            return (Criteria) this;
        }

        public Criteria andScrapDateLessThanOrEqualTo(Date value) {
            addCriterion("scrap_date <=", value, "scrapDate");
            return (Criteria) this;
        }

        public Criteria andScrapDateIn(List<Date> values) {
            addCriterion("scrap_date in", values, "scrapDate");
            return (Criteria) this;
        }

        public Criteria andScrapDateNotIn(List<Date> values) {
            addCriterion("scrap_date not in", values, "scrapDate");
            return (Criteria) this;
        }

        public Criteria andScrapDateBetween(Date value1, Date value2) {
            addCriterion("scrap_date between", value1, value2, "scrapDate");
            return (Criteria) this;
        }

        public Criteria andScrapDateNotBetween(Date value1, Date value2) {
            addCriterion("scrap_date not between", value1, value2, "scrapDate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andOwnerIsNull() {
            addCriterion("owner is null");
            return (Criteria) this;
        }

        public Criteria andOwnerIsNotNull() {
            addCriterion("owner is not null");
            return (Criteria) this;
        }

        public Criteria andOwnerEqualTo(Integer value) {
            addCriterion("owner =", value, "owner");
            return (Criteria) this;
        }

        public Criteria andOwnerNotEqualTo(Integer value) {
            addCriterion("owner <>", value, "owner");
            return (Criteria) this;
        }

        public Criteria andOwnerGreaterThan(Integer value) {
            addCriterion("owner >", value, "owner");
            return (Criteria) this;
        }

        public Criteria andOwnerGreaterThanOrEqualTo(Integer value) {
            addCriterion("owner >=", value, "owner");
            return (Criteria) this;
        }

        public Criteria andOwnerLessThan(Integer value) {
            addCriterion("owner <", value, "owner");
            return (Criteria) this;
        }

        public Criteria andOwnerLessThanOrEqualTo(Integer value) {
            addCriterion("owner <=", value, "owner");
            return (Criteria) this;
        }

        public Criteria andOwnerIn(List<Integer> values) {
            addCriterion("owner in", values, "owner");
            return (Criteria) this;
        }

        public Criteria andOwnerNotIn(List<Integer> values) {
            addCriterion("owner not in", values, "owner");
            return (Criteria) this;
        }

        public Criteria andOwnerBetween(Integer value1, Integer value2) {
            addCriterion("owner between", value1, value2, "owner");
            return (Criteria) this;
        }

        public Criteria andOwnerNotBetween(Integer value1, Integer value2) {
            addCriterion("owner not between", value1, value2, "owner");
            return (Criteria) this;
        }

        public Criteria andCoUserIdIsNull() {
            addCriterion("co_user_id is null");
            return (Criteria) this;
        }

        public Criteria andCoUserIdIsNotNull() {
            addCriterion("co_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andCoUserIdEqualTo(Integer value) {
            addCriterion("co_user_id =", value, "coUserId");
            return (Criteria) this;
        }

        public Criteria andCoUserIdNotEqualTo(Integer value) {
            addCriterion("co_user_id <>", value, "coUserId");
            return (Criteria) this;
        }

        public Criteria andCoUserIdGreaterThan(Integer value) {
            addCriterion("co_user_id >", value, "coUserId");
            return (Criteria) this;
        }

        public Criteria andCoUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("co_user_id >=", value, "coUserId");
            return (Criteria) this;
        }

        public Criteria andCoUserIdLessThan(Integer value) {
            addCriterion("co_user_id <", value, "coUserId");
            return (Criteria) this;
        }

        public Criteria andCoUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("co_user_id <=", value, "coUserId");
            return (Criteria) this;
        }

        public Criteria andCoUserIdIn(List<Integer> values) {
            addCriterion("co_user_id in", values, "coUserId");
            return (Criteria) this;
        }

        public Criteria andCoUserIdNotIn(List<Integer> values) {
            addCriterion("co_user_id not in", values, "coUserId");
            return (Criteria) this;
        }

        public Criteria andCoUserIdBetween(Integer value1, Integer value2) {
            addCriterion("co_user_id between", value1, value2, "coUserId");
            return (Criteria) this;
        }

        public Criteria andCoUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("co_user_id not between", value1, value2, "coUserId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}