package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarExample;
import com.yff.tuan.model.Cmodel;
import com.yff.tuan.model.CmodelExample;
import com.yff.tuan.util.Page;

public interface ModelService {
	/*public abstract Car find(Car car);
	
	public abstract Car findByPrimaryKey(Integer id);

	public abstract int insert(Car car);
	
	public abstract int updateByPrimaryKeySelective(Car car);
	
	public abstract int delByPrimaryKey(Integer id);*/
	
	public abstract List<Cmodel> query(CmodelExample example);
	
}
