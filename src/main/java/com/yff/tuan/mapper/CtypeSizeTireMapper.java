package com.yff.tuan.mapper;

import com.yff.tuan.model.CtypeSizeTire;
import com.yff.tuan.model.CtypeSizeTireExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CtypeSizeTireMapper {
    int countByExample(CtypeSizeTireExample example);

    int deleteByExample(CtypeSizeTireExample example);

    int insert(CtypeSizeTire record);

    int insertSelective(CtypeSizeTire record);

    List<CtypeSizeTire> selectByExample(CtypeSizeTireExample example);

    int updateByExampleSelective(@Param("record") CtypeSizeTire record, @Param("example") CtypeSizeTireExample example);

    int updateByExample(@Param("record") CtypeSizeTire record, @Param("example") CtypeSizeTireExample example);
}