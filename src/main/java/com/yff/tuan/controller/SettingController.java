package com.yff.tuan.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yff.tuan.api.ApiResult;
import com.yff.tuan.model.CarFenceExample;
import com.yff.tuan.model.CbrandExample;
import com.yff.tuan.model.Clabel;
import com.yff.tuan.model.ClabelExample;
import com.yff.tuan.model.CoPosition;
import com.yff.tuan.model.CoPositionExample;
import com.yff.tuan.model.CarFenceExample.Criteria;
import com.yff.tuan.model.CarWarn;
import com.yff.tuan.model.CarWarnExample;
import com.yff.tuan.model.Cbrand;
import com.yff.tuan.service.BrandService;
import com.yff.tuan.service.CarWarnService;
import com.yff.tuan.service.FenceService;
import com.yff.tuan.service.LabelService;
import com.yff.tuan.service.PositionService;


@Controller
@RequestMapping("/setting")
public class SettingController {
	@Autowired BrandService brandService;
	@Autowired FenceService fenceService;
	@Autowired CarWarnService carWarnService;
	@Autowired PositionService positionService;
	@Autowired LabelService labelService;
	
	@RequestMapping("/query")
	public String query(Model model,HttpServletRequest request,HttpServletResponse response) {
		return "/setting";
	}
	
	@RequestMapping("/brand")
	public String brand(Model model,HttpServletRequest request,HttpServletResponse response) {
		int userId = (int) request.getSession().getAttribute("userId");
		CbrandExample example = new  CbrandExample();
		example.createCriteria().andUserIdEqualTo(userId);
		model.addAttribute("brands", brandService.query(example));
		return "/setting_brand";
	}
	
	@RequestMapping("/brand/toadd")
	public String brandToadd(Model model,Integer id,HttpServletRequest request,HttpServletResponse response) {
		if(null != id) {
			model.addAttribute("brand", brandService.find(id));
		}
		return "/setting_brand_add";
	}
	
	@RequestMapping("/brand/add")
	@ResponseBody
	public Object brandadd(Model model,@RequestBody Cbrand cbrand,HttpServletRequest request,HttpServletResponse response) {
		Map<String, Boolean> map = new HashMap<String,Boolean>();
		if(null != cbrand) {
			int userId = (int) request.getSession().getAttribute("userId");
			cbrand.setUserId(userId);
			if(null != cbrand.getId()) {
				brandService.update(cbrand);
			}else {
				brandService.add(cbrand);
			}
			map.put("add", true);
		}
		return map;
	}
	
	/*@RequestMapping("/warn")
	public String warn(Model model,HttpServletRequest request,HttpServletResponse response) {
		int userId = (int) request.getSession().getAttribute("userId");
		CarWarnExample example = new  CarWarnExample();
		example.createCriteria().andUserIdEqualTo(userId);
		model.addAttribute("carWarn", carWarnService.query(example));
		return "/setting_warn";
	}*/
	
	@RequestMapping("/warn")
	public String warnToadd(Model model,HttpServletRequest request,HttpServletResponse response) {
		int userId = (int) request.getSession().getAttribute("userId");
		model.addAttribute("warn", carWarnService.find(userId));
		return "/setting_warn";
	}
	
	@RequestMapping("/warn/add")
	@ResponseBody
	public Object warnadd(Model model,CarWarn carWarn,HttpServletRequest request,HttpServletResponse response) {
		boolean add = false;
		if(null != carWarn) {
			int userId = (int) request.getSession().getAttribute("userId");
			carWarn.setUserId(userId);
			if(null != carWarn.getId()) {
				carWarnService.update(carWarn);
			}else {
				carWarnService.add(carWarn);
			}
			add = true;
		}
		return ApiResult.SUCCESS(add);
	}
	
	@RequestMapping("/model")
	public String model(Model model,HttpServletRequest request,HttpServletResponse response) {
		return "/setting";
	}
	
	@RequestMapping("/position")
	public String position(Model model,HttpServletRequest request,HttpServletResponse response) {
		int userId = (int) request.getSession().getAttribute("userId");
		CoPositionExample example = new CoPositionExample();
		example.createCriteria().andUserIdEqualTo(userId);
		model.addAttribute("positions", positionService.query(example));
		return "/setting_position";
	}
	@RequestMapping("/position/toadd")
	public String positiontoadd(Model model,Integer id,HttpServletRequest request,HttpServletResponse response) {
		model.addAttribute("position", positionService.findByPrimaryKey(id));
		return "/setting_position_add";
		
	}
	@RequestMapping("/position/add")
	@ResponseBody
	public Object positionadd(Model model,CoPosition position,HttpServletRequest request,HttpServletResponse response) {
		boolean add = false;
		if(null != position) {
			int userId = (int) request.getSession().getAttribute("userId");
			position.setUserId(userId);
			if(position.getId() != null) {
				
				positionService.update(position);
			}else {
				positionService.save(position);
			}
			add = true;
		}
		return ApiResult.SUCCESS(add);
	}
	
	
	@RequestMapping("/label")
	public String label(Model model,HttpServletRequest request,HttpServletResponse response) {
		int userId = (int) request.getSession().getAttribute("userId");
		ClabelExample example = new  ClabelExample();
		example.createCriteria().andUserIdEqualTo(userId);
		model.addAttribute("labels", labelService.query(example));
		return "/setting_label";
	}
	
	@RequestMapping("/label/toadd")
	public String labelToadd(Model model,Integer id,HttpServletRequest request,HttpServletResponse response) {
		if(null != id) {
			model.addAttribute("label", labelService.find(id));
		}
		return "/setting_label_add";
	}
	
	@RequestMapping("/label/add")
	@ResponseBody
	public Object labeladd(Model model,@RequestBody Clabel clabel,HttpServletRequest request,HttpServletResponse response) {
		Map<String, Boolean> map = new HashMap<String,Boolean>();
		if(null != clabel) {
			int userId = (int) request.getSession().getAttribute("userId");
			clabel.setUserId(userId);
			if(null != clabel.getId()) {
				labelService.update(clabel);
			}else {
				labelService.add(clabel);
			}
			map.put("add", true);
		}
		return map;
	}
	
}
