<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%	
	String path = request.getContextPath();
	int role = (int) request.getSession().getAttribute("role");
	String uri = (String) request.getSession().getAttribute("uri");
	String  jcActive = "",csActive="",ryActive="",sbActive="",ssActive="",feActive="",daActive="",warnActive="",reportActive="";
	if(uri.startsWith("/company") ||uri.startsWith("/fleet") ||uri.startsWith("/car") ) jcActive = "active";
	if(uri.startsWith("/setting") ) csActive = "active";
	if(uri.startsWith("/user") ) ryActive = "active";
	if(uri.startsWith("/bind") ) sbActive = "active";
	if(uri.startsWith("/check") ) ssActive = "active";
	if(uri.startsWith("/fence") ) feActive = "active";
	if(uri.startsWith("/data") ) daActive = "active";
	if(uri.startsWith("/warn") ) warnActive = "active";
	if(uri.startsWith("/report") ) reportActive = "active";
%>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> 
      <a class="brand" href="<%=path %>/index.html">卡客途安轮卫管家</a>
      <a class="brand" href="<%=path %>/logout.html" style="font-size: 12px;float: right;padding-top: 16px;"><i class="icon-signout"></i><span>退出</span> </a>
      <a class="brand"  href="#passupdate" role="button" data-toggle="modal" style="font-size: 12px;float: right;padding-top: 16px;">修改密码</a>
      <%-- <a class="brand" href="javascript:void(0)" style="font-size: 12px;float: right;padding-top: 16px;"><i class="icon-user"></i>${userName}</a> --%>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
      	<% if( role == 1 ) { %>
        <li class="<%=csActive %>" ><a href="<%=path %>/setting/query.html"><i class="icon-list-alt"></i><span>参数设定</span> </a> </li>
        <li class="<%=jcActive %> dropdown">
          <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> 
          	<i class="icon-cog"></i>
          	<span>基础设置</span>
          	<b class="caret"></b>
          </a>
          <ul class="dropdown-menu">
            <li><a href="<%=path %>/company/query.html">分公司</a></li>
            <li><a href="<%=path %>/fleet/query.html">车队</a></li>
            <li><a href="<%=path %>/car/query.html">车辆</a></li>
          </ul>
        </li>
        <li class="<%=ryActive %>" ><a href="<%=path %>/user/query.html"><i class="icon-user"></i><span>人员权限管理</span> </a> </li>
        <li class="<%=feActive %>" ><a href="<%=path %>/fence/query.html"><i class="icon-map-marker"></i><span>电子围栏</span> </a> </li>
        <li class="<%=sbActive %>" ><a href="<%=path %>/bind/query.html"><i class="icon-sitemap"></i><span>设备绑定</span> </a></li>
        <% } %>
        <li class="<%=ssActive %>" ><a href="<%=path %>/check/query.html"><i class="icon-facetime-video"></i><span>实时监控</span> </a></li>
        <li class="<%=warnActive %>" ><a href="<%=path %>/warn/query.html"><i class=" icon-warning-sign"></i><span>实时报警</span> </a></li>
        <% if( role == 1 ) { %>
       <%--  <li class="<%=daActive %>" ><a href="<%=path %>/data/query.html"><i class="icon-bar-chart"></i><span>统计查询</span> </a></li> --%>
       
        <li class="<%=daActive %> dropdown">
          <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> 
          	<i class="icon-bar-chart"></i>
          	<span>统计查询</span>
          	<b class="caret"></b>
          </a>
          <ul class="dropdown-menu">
            <li ><a href="<%=path %>/data/drag/query.html">挂车里程</a></li>
            <li ><a href="<%=path %>/data/draw/query.html">牵引车里程</a></li>
            <li ><a href="<%=path %>/data/whole/query.html">一体车里程</a></li>
            <li ><a href="<%=path %>/data/drag/dispatch.html">挂车调度查询</a></li>
            <li ><a href="<%=path %>/data/report.html">车辆日检故障汇总</a></li>
          </ul>
        </li> 
         <% } %>
          <li class="<%=reportActive %>" ><a href="<%=path %>/report/query.html"><i class=" icon-list-alt"></i><span>车辆日检调查</span> </a></li>
         
       <!--  <li><a href="shortcodes.html"><i class="icon-code"></i><span>Shortcodes</span> </a> </li> -->
       <!--  <li class="dropdown">
          
        </li> -->
      </ul>
    </div>
    <!-- /container --> 
  </div>
  <%-- <embed id="wavFileId" 
       src="<%=path %>/mis/alarm.mp3"
       width="0"
       height="0"
       loop="false" 
       autostart="false"  play="false" >
	</embed> --%>
  <!-- /subnavbar-inner --> 
	<div id="passupdate" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
         <h3 id="myModalLabel">修改密码</h3>
       </div>
       <div class="modal-body">
       		<form action="" method="POST" id="form001">
        	 <div class="control-group">											
				<label class="control-label" for="upassword">新密码<span style="color: red">*</span></label>
				<div class="controls">
					<input type="text" class="span5" id="upassword" name="upassword" value="" placeholder="请输入新密码">
				</div> <!-- /controls -->				
			</div>
			<div class="control-group">											
				<label class="control-label" for="cupassword">确认密码<span style="color: red">*</span></label>
				<div class="controls">
					<input type="text" class="span5" id="cupassword" value="" placeholder="请确认新密码">
				</div> <!-- /controls -->				
			</div>
			</form>
			<div class="control-group" id="passmsg" style="color: red"></div>
       </div>
       <div class="modal-footer">
         <button class="btn" id="closeBtn" data-dismiss="modal" aria-hidden="true">关闭</button>
         <button class="btn btn-primary" id="sbpass">提交</button>
       </div>
     </div>
</div>
<script src="<%=path %>/js/jquery-1.7.2.min.js"></script> 
<script language="javascript">
$("#sbpass").click(function(){
	if($("#upassword").val()==""){
		$("#passmsg").empty();
		$("#passmsg").html("请输入新密码");
		return;
	}
	if($("#cupassword").val() != $("#upassword").val()){
		$("#passmsg").empty();
		$("#passmsg").html("密码不一致，请从新输入");
		return;
	}
	$.ajax({
	    url:'<%=path %>/user/password',
	    type:'POST', //GET
	    data: $("#form001").serializeArray(),
	    async:true,    //或false,是否异步
	    success:function(data){
	    	if(data.resCode=="0"){
	    		alert("修改成功！");
	    		$("#closeBtn").click();
	    	}
	    },
	    error:function(xhr){},
	});
	
});

function playSound(){
  var borswer = window.navigator.userAgent.toLowerCase();
  if ( borswer.indexOf( "ie" ) >= 0 ){
    //IE内核浏览器
    var strEmbed = '<embed name="embedPlay" src="<%=path %>/mis/alarm.mp3" autostart="true" hidden="true" loop="false"></embed>';
    if ( $( "body" ).find( "embed" ).length <= 0 )
      $( "body" ).append( strEmbed );
    var embed = document.embedPlay;

    //浏览器不支持 audion，则使用 embed 播放
    embed.volume = 100;
    //embed.play();这个不需要
  } else {
    //非IE内核浏览器
    var strAudio = "<audio id='audioPlay' src='<%=path %>/mis/alarm.mp3' hidden='true'>";
    if ( $( "body" ).find( "audio" ).length <= 0 )
      $( "body" ).append( strAudio );
    var audio = document.getElementById( "audioPlay" );
    //浏览器支持 audion
    audio.play();
  }
}

function queryWarn(){
	$.ajax({
	    url:'<%=path %>/warn/find.html',
	    type:'GET', //GET
	    async:true,    //或false,是否异步
	    success:function(data){
	    	if(data.warn == true){
	    		playSound();
	    	}
	    },
	    error:function(xhr){},
	});
}

window.setInterval("queryWarn()",30000); 
</script>