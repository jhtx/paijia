package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.CoFleetMapper;
import com.yff.tuan.model.CoFleet;
import com.yff.tuan.model.CoFleetExample;
import com.yff.tuan.model.CoFleetExample.Criteria;
import com.yff.tuan.service.CoFleetService;
import com.yff.tuan.util.Page;

@Component
public class CoFleetServiceImpl implements CoFleetService {
	@Autowired
	CoFleetMapper fleetMapper;
	
	@Override
	public CoFleet find(CoFleet fleet) {
		CoFleetExample example = new CoFleetExample();
		Criteria createCriteria = example.createCriteria();
		createCriteria.andNameEqualTo(fleet.getName());
		List<CoFleet> fleets = fleetMapper.selectByExample(example);
		return fleets.isEmpty()?null:fleets.get(0);
	}

	@Override
	public CoFleet findByPrimaryKey(Integer id) {
		return fleetMapper.selectByPrimaryKey(id);
	}

	@Override
	public int insert(CoFleet fleet) {
		return fleetMapper.insert(fleet);
	}

	@Override
	public int updateByPrimaryKey(CoFleet fleet) {
		return fleetMapper.updateByPrimaryKey(fleet);
	}
	
	@Override
	public int updateByPrimaryKeySelective(CoFleet fleet) {
		return fleetMapper.updateByPrimaryKeySelective(fleet);
	}

	@Override
	public List<CoFleet> query(CoFleetExample example) {
		return fleetMapper.selectByExample(example);
	}

	@Override
	public Page<CoFleet> queryByPage(CoFleetExample example, Integer pageNum) {
		return null;
	}

	@Override
	public int delByPrimaryKey(Integer id) {
		return fleetMapper.deleteByPrimaryKey(id);
	}
}
