package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarExample;
import com.yff.tuan.model.Cbrand;
import com.yff.tuan.model.CbrandExample;
import com.yff.tuan.model.Cmodel;
import com.yff.tuan.model.CmodelExample;
import com.yff.tuan.util.Page;

public interface BrandService {
	/*public abstract Car find(Car car);
	
	public abstract Car findByPrimaryKey(Integer id);

	public abstract int insert(Car car);
	
	public abstract int updateByPrimaryKeySelective(Car car);
	
	public abstract int delByPrimaryKey(Integer id);*/
	
	public abstract List<Cbrand> query(CbrandExample example);
	
	public abstract Cbrand find(Integer id);
	
	public abstract boolean add(Cbrand cbrand);
	
	public abstract boolean update(Cbrand cbrand);
	
	public abstract List<Cmodel> queryCmodel(Integer brandId);
	
}
