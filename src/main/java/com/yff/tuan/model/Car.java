package com.yff.tuan.model;

import java.util.Date;
import java.util.List;

public class Car {
	private Integer id;

    private String no;

    private Integer fleetId;

    private String licenseNo;

    private Integer type;

    private Integer typeSizeId;

    private Integer brandId;

    private Integer modelId;

    private Double miles;

    private Date licensingDate;

    private Date insureDate;

    private Date checkDate;

    private Date scrapDate;

    private Integer status;

    private Integer owner;

    private Integer coUserId;

    private String remark;
    
    private String fleetName;
    
    private Integer checked;
    
    private CarBind carBind;
    private List<CarBindTyre> carBindTyres;
    private List<CtypeSizeTire> cTypeSizeTire;
    
    private List<CarFiTyre> carFiTyres;
    private String carFiTyreJson;
    private List<CarSeTyre> carSeTyres;
    private String carSeTyreJson;
    private List<CarThTyre> carThTyres;
    private String carThTyreJson;
    
    private int state;
    private int matchCarId;
    private String matchLicenseNo;
    private int  bind;
    
	public Integer getId() {
		return id;
	}
	public String getNo() {
		return no;
	}
	public Integer getFleetId() {
		return fleetId;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public Integer getType() {
		return type;
	}
	public Integer getTypeSizeId() {
		return typeSizeId;
	}
	public Integer getBrandId() {
		return brandId;
	}
	public Integer getModelId() {
		return modelId;
	}
	public Double getMiles() {
		return miles;
	}
	public Date getLicensingDate() {
		return licensingDate;
	}
	public Date getInsureDate() {
		return insureDate;
	}
	public Date getCheckDate() {
		return checkDate;
	}
	public Date getScrapDate() {
		return scrapDate;
	}
	public Integer getStatus() {
		return status;
	}
	public Integer getOwner() {
		return owner;
	}
	public Integer getCoUserId() {
		return coUserId;
	}
	public String getRemark() {
		return remark;
	}
	public String getFleetName() {
		return fleetName;
	}
	public CarBind getCarBind() {
		return carBind;
	}
	public List<CarBindTyre> getCarBindTyres() {
		return carBindTyres;
	}
	public List<CarFiTyre> getCarFiTyres() {
		return carFiTyres;
	}
	public List<CarSeTyre> getCarSeTyres() {
		return carSeTyres;
	}
	public List<CarThTyre> getCarThTyres() {
		return carThTyres;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public void setFleetId(Integer fleetId) {
		this.fleetId = fleetId;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public void setTypeSizeId(Integer typeSizeId) {
		this.typeSizeId = typeSizeId;
	}
	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}
	public void setModelId(Integer modelId) {
		this.modelId = modelId;
	}
	public void setMiles(Double miles) {
		this.miles = miles;
	}
	public void setLicensingDate(Date licensingDate) {
		this.licensingDate = licensingDate;
	}
	public void setInsureDate(Date insureDate) {
		this.insureDate = insureDate;
	}
	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}
	public void setScrapDate(Date scrapDate) {
		this.scrapDate = scrapDate;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public void setOwner(Integer owner) {
		this.owner = owner;
	}
	public void setCoUserId(Integer coUserId) {
		this.coUserId = coUserId;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public void setFleetName(String fleetName) {
		this.fleetName = fleetName;
	}
	public void setCarBind(CarBind carBind) {
		this.carBind = carBind;
	}
	public void setCarBindTyres(List<CarBindTyre> carBindTyres) {
		this.carBindTyres = carBindTyres;
	}
	public void setCarFiTyres(List<CarFiTyre> carFiTyres) {
		this.carFiTyres = carFiTyres;
	}
	public void setCarSeTyres(List<CarSeTyre> carSeTyres) {
		this.carSeTyres = carSeTyres;
	}
	public void setCarThTyres(List<CarThTyre> carThTyres) {
		this.carThTyres = carThTyres;
	}
	public String getCarFiTyreJson() {
		return carFiTyreJson;
	}
	public String getCarSeTyreJson() {
		return carSeTyreJson;
	}
	public String getCarThTyreJson() {
		return carThTyreJson;
	}
	public void setCarFiTyreJson(String carFiTyreJson) {
		this.carFiTyreJson = carFiTyreJson;
	}
	public void setCarSeTyreJson(String carSeTyreJson) {
		this.carSeTyreJson = carSeTyreJson;
	}
	public void setCarThTyreJson(String carThTyreJson) {
		this.carThTyreJson = carThTyreJson;
	}
	public List<CtypeSizeTire> getcTypeSizeTire() {
		return cTypeSizeTire;
	}
	public void setcTypeSizeTire(List<CtypeSizeTire> cTypeSizeTire) {
		this.cTypeSizeTire = cTypeSizeTire;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getMatchLicenseNo() {
		return matchLicenseNo;
	}
	public void setMatchLicenseNo(String matchLicenseNo) {
		this.matchLicenseNo = matchLicenseNo;
	}
	public Integer getChecked() {
		return checked;
	}
	public void setChecked(Integer checked) {
		this.checked = checked;
	}
	public int getMatchCarId() {
		return matchCarId;
	}
	public void setMatchCarId(int matchCarId) {
		this.matchCarId = matchCarId;
	}
	public int getBind() {
		return bind;
	}
	public void setBind(int bind) {
		this.bind = bind;
	}
    
}