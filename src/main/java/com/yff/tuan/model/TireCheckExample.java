package com.yff.tuan.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TireCheckExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TireCheckExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNull() {
            addCriterion("product_id is null");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNotNull() {
            addCriterion("product_id is not null");
            return (Criteria) this;
        }

        public Criteria andProductIdEqualTo(String value) {
            addCriterion("product_id =", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotEqualTo(String value) {
            addCriterion("product_id <>", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThan(String value) {
            addCriterion("product_id >", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThanOrEqualTo(String value) {
            addCriterion("product_id >=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThan(String value) {
            addCriterion("product_id <", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThanOrEqualTo(String value) {
            addCriterion("product_id <=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLike(String value) {
            addCriterion("product_id like", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotLike(String value) {
            addCriterion("product_id not like", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdIn(List<String> values) {
            addCriterion("product_id in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotIn(List<String> values) {
            addCriterion("product_id not in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdBetween(String value1, String value2) {
            addCriterion("product_id between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotBetween(String value1, String value2) {
            addCriterion("product_id not between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andTyreNoIsNull() {
            addCriterion("tyre_no is null");
            return (Criteria) this;
        }

        public Criteria andTyreNoIsNotNull() {
            addCriterion("tyre_no is not null");
            return (Criteria) this;
        }

        public Criteria andTyreNoEqualTo(String value) {
            addCriterion("tyre_no =", value, "tyreNo");
            return (Criteria) this;
        }

        public Criteria andTyreNoNotEqualTo(String value) {
            addCriterion("tyre_no <>", value, "tyreNo");
            return (Criteria) this;
        }

        public Criteria andTyreNoGreaterThan(String value) {
            addCriterion("tyre_no >", value, "tyreNo");
            return (Criteria) this;
        }

        public Criteria andTyreNoGreaterThanOrEqualTo(String value) {
            addCriterion("tyre_no >=", value, "tyreNo");
            return (Criteria) this;
        }

        public Criteria andTyreNoLessThan(String value) {
            addCriterion("tyre_no <", value, "tyreNo");
            return (Criteria) this;
        }

        public Criteria andTyreNoLessThanOrEqualTo(String value) {
            addCriterion("tyre_no <=", value, "tyreNo");
            return (Criteria) this;
        }

        public Criteria andTyreNoLike(String value) {
            addCriterion("tyre_no like", value, "tyreNo");
            return (Criteria) this;
        }

        public Criteria andTyreNoNotLike(String value) {
            addCriterion("tyre_no not like", value, "tyreNo");
            return (Criteria) this;
        }

        public Criteria andTyreNoIn(List<String> values) {
            addCriterion("tyre_no in", values, "tyreNo");
            return (Criteria) this;
        }

        public Criteria andTyreNoNotIn(List<String> values) {
            addCriterion("tyre_no not in", values, "tyreNo");
            return (Criteria) this;
        }

        public Criteria andTyreNoBetween(String value1, String value2) {
            addCriterion("tyre_no between", value1, value2, "tyreNo");
            return (Criteria) this;
        }

        public Criteria andTyreNoNotBetween(String value1, String value2) {
            addCriterion("tyre_no not between", value1, value2, "tyreNo");
            return (Criteria) this;
        }

        public Criteria andPressureIsNull() {
            addCriterion("pressure is null");
            return (Criteria) this;
        }

        public Criteria andPressureIsNotNull() {
            addCriterion("pressure is not null");
            return (Criteria) this;
        }

        public Criteria andPressureEqualTo(Double value) {
            addCriterion("pressure =", value, "pressure");
            return (Criteria) this;
        }

        public Criteria andPressureNotEqualTo(Double value) {
            addCriterion("pressure <>", value, "pressure");
            return (Criteria) this;
        }

        public Criteria andPressureGreaterThan(Double value) {
            addCriterion("pressure >", value, "pressure");
            return (Criteria) this;
        }

        public Criteria andPressureGreaterThanOrEqualTo(Double value) {
            addCriterion("pressure >=", value, "pressure");
            return (Criteria) this;
        }

        public Criteria andPressureLessThan(Double value) {
            addCriterion("pressure <", value, "pressure");
            return (Criteria) this;
        }

        public Criteria andPressureLessThanOrEqualTo(Double value) {
            addCriterion("pressure <=", value, "pressure");
            return (Criteria) this;
        }

        public Criteria andPressureIn(List<Double> values) {
            addCriterion("pressure in", values, "pressure");
            return (Criteria) this;
        }

        public Criteria andPressureNotIn(List<Double> values) {
            addCriterion("pressure not in", values, "pressure");
            return (Criteria) this;
        }

        public Criteria andPressureBetween(Double value1, Double value2) {
            addCriterion("pressure between", value1, value2, "pressure");
            return (Criteria) this;
        }

        public Criteria andPressureNotBetween(Double value1, Double value2) {
            addCriterion("pressure not between", value1, value2, "pressure");
            return (Criteria) this;
        }

        public Criteria andTemperatureIsNull() {
            addCriterion("temperature is null");
            return (Criteria) this;
        }

        public Criteria andTemperatureIsNotNull() {
            addCriterion("temperature is not null");
            return (Criteria) this;
        }

        public Criteria andTemperatureEqualTo(Double value) {
            addCriterion("temperature =", value, "temperature");
            return (Criteria) this;
        }

        public Criteria andTemperatureNotEqualTo(Double value) {
            addCriterion("temperature <>", value, "temperature");
            return (Criteria) this;
        }

        public Criteria andTemperatureGreaterThan(Double value) {
            addCriterion("temperature >", value, "temperature");
            return (Criteria) this;
        }

        public Criteria andTemperatureGreaterThanOrEqualTo(Double value) {
            addCriterion("temperature >=", value, "temperature");
            return (Criteria) this;
        }

        public Criteria andTemperatureLessThan(Double value) {
            addCriterion("temperature <", value, "temperature");
            return (Criteria) this;
        }

        public Criteria andTemperatureLessThanOrEqualTo(Double value) {
            addCriterion("temperature <=", value, "temperature");
            return (Criteria) this;
        }

        public Criteria andTemperatureIn(List<Double> values) {
            addCriterion("temperature in", values, "temperature");
            return (Criteria) this;
        }

        public Criteria andTemperatureNotIn(List<Double> values) {
            addCriterion("temperature not in", values, "temperature");
            return (Criteria) this;
        }

        public Criteria andTemperatureBetween(Double value1, Double value2) {
            addCriterion("temperature between", value1, value2, "temperature");
            return (Criteria) this;
        }

        public Criteria andTemperatureNotBetween(Double value1, Double value2) {
            addCriterion("temperature not between", value1, value2, "temperature");
            return (Criteria) this;
        }

        public Criteria andPressureWarningIsNull() {
            addCriterion("pressure_warning is null");
            return (Criteria) this;
        }

        public Criteria andPressureWarningIsNotNull() {
            addCriterion("pressure_warning is not null");
            return (Criteria) this;
        }

        public Criteria andPressureWarningEqualTo(Integer value) {
            addCriterion("pressure_warning =", value, "pressureWarning");
            return (Criteria) this;
        }

        public Criteria andPressureWarningNotEqualTo(Integer value) {
            addCriterion("pressure_warning <>", value, "pressureWarning");
            return (Criteria) this;
        }

        public Criteria andPressureWarningGreaterThan(Integer value) {
            addCriterion("pressure_warning >", value, "pressureWarning");
            return (Criteria) this;
        }

        public Criteria andPressureWarningGreaterThanOrEqualTo(Integer value) {
            addCriterion("pressure_warning >=", value, "pressureWarning");
            return (Criteria) this;
        }

        public Criteria andPressureWarningLessThan(Integer value) {
            addCriterion("pressure_warning <", value, "pressureWarning");
            return (Criteria) this;
        }

        public Criteria andPressureWarningLessThanOrEqualTo(Integer value) {
            addCriterion("pressure_warning <=", value, "pressureWarning");
            return (Criteria) this;
        }

        public Criteria andPressureWarningIn(List<Integer> values) {
            addCriterion("pressure_warning in", values, "pressureWarning");
            return (Criteria) this;
        }

        public Criteria andPressureWarningNotIn(List<Integer> values) {
            addCriterion("pressure_warning not in", values, "pressureWarning");
            return (Criteria) this;
        }

        public Criteria andPressureWarningBetween(Integer value1, Integer value2) {
            addCriterion("pressure_warning between", value1, value2, "pressureWarning");
            return (Criteria) this;
        }

        public Criteria andPressureWarningNotBetween(Integer value1, Integer value2) {
            addCriterion("pressure_warning not between", value1, value2, "pressureWarning");
            return (Criteria) this;
        }

        public Criteria andTemperatureWarningIsNull() {
            addCriterion("temperature_warning is null");
            return (Criteria) this;
        }

        public Criteria andTemperatureWarningIsNotNull() {
            addCriterion("temperature_warning is not null");
            return (Criteria) this;
        }

        public Criteria andTemperatureWarningEqualTo(Integer value) {
            addCriterion("temperature_warning =", value, "temperatureWarning");
            return (Criteria) this;
        }

        public Criteria andTemperatureWarningNotEqualTo(Integer value) {
            addCriterion("temperature_warning <>", value, "temperatureWarning");
            return (Criteria) this;
        }

        public Criteria andTemperatureWarningGreaterThan(Integer value) {
            addCriterion("temperature_warning >", value, "temperatureWarning");
            return (Criteria) this;
        }

        public Criteria andTemperatureWarningGreaterThanOrEqualTo(Integer value) {
            addCriterion("temperature_warning >=", value, "temperatureWarning");
            return (Criteria) this;
        }

        public Criteria andTemperatureWarningLessThan(Integer value) {
            addCriterion("temperature_warning <", value, "temperatureWarning");
            return (Criteria) this;
        }

        public Criteria andTemperatureWarningLessThanOrEqualTo(Integer value) {
            addCriterion("temperature_warning <=", value, "temperatureWarning");
            return (Criteria) this;
        }

        public Criteria andTemperatureWarningIn(List<Integer> values) {
            addCriterion("temperature_warning in", values, "temperatureWarning");
            return (Criteria) this;
        }

        public Criteria andTemperatureWarningNotIn(List<Integer> values) {
            addCriterion("temperature_warning not in", values, "temperatureWarning");
            return (Criteria) this;
        }

        public Criteria andTemperatureWarningBetween(Integer value1, Integer value2) {
            addCriterion("temperature_warning between", value1, value2, "temperatureWarning");
            return (Criteria) this;
        }

        public Criteria andTemperatureWarningNotBetween(Integer value1, Integer value2) {
            addCriterion("temperature_warning not between", value1, value2, "temperatureWarning");
            return (Criteria) this;
        }

        public Criteria andAirWarningIsNull() {
            addCriterion("air_warning is null");
            return (Criteria) this;
        }

        public Criteria andAirWarningIsNotNull() {
            addCriterion("air_warning is not null");
            return (Criteria) this;
        }

        public Criteria andAirWarningEqualTo(Integer value) {
            addCriterion("air_warning =", value, "airWarning");
            return (Criteria) this;
        }

        public Criteria andAirWarningNotEqualTo(Integer value) {
            addCriterion("air_warning <>", value, "airWarning");
            return (Criteria) this;
        }

        public Criteria andAirWarningGreaterThan(Integer value) {
            addCriterion("air_warning >", value, "airWarning");
            return (Criteria) this;
        }

        public Criteria andAirWarningGreaterThanOrEqualTo(Integer value) {
            addCriterion("air_warning >=", value, "airWarning");
            return (Criteria) this;
        }

        public Criteria andAirWarningLessThan(Integer value) {
            addCriterion("air_warning <", value, "airWarning");
            return (Criteria) this;
        }

        public Criteria andAirWarningLessThanOrEqualTo(Integer value) {
            addCriterion("air_warning <=", value, "airWarning");
            return (Criteria) this;
        }

        public Criteria andAirWarningIn(List<Integer> values) {
            addCriterion("air_warning in", values, "airWarning");
            return (Criteria) this;
        }

        public Criteria andAirWarningNotIn(List<Integer> values) {
            addCriterion("air_warning not in", values, "airWarning");
            return (Criteria) this;
        }

        public Criteria andAirWarningBetween(Integer value1, Integer value2) {
            addCriterion("air_warning between", value1, value2, "airWarning");
            return (Criteria) this;
        }

        public Criteria andAirWarningNotBetween(Integer value1, Integer value2) {
            addCriterion("air_warning not between", value1, value2, "airWarning");
            return (Criteria) this;
        }

        public Criteria andSignalWarningIsNull() {
            addCriterion("signal_warning is null");
            return (Criteria) this;
        }

        public Criteria andSignalWarningIsNotNull() {
            addCriterion("signal_warning is not null");
            return (Criteria) this;
        }

        public Criteria andSignalWarningEqualTo(Integer value) {
            addCriterion("signal_warning =", value, "signalWarning");
            return (Criteria) this;
        }

        public Criteria andSignalWarningNotEqualTo(Integer value) {
            addCriterion("signal_warning <>", value, "signalWarning");
            return (Criteria) this;
        }

        public Criteria andSignalWarningGreaterThan(Integer value) {
            addCriterion("signal_warning >", value, "signalWarning");
            return (Criteria) this;
        }

        public Criteria andSignalWarningGreaterThanOrEqualTo(Integer value) {
            addCriterion("signal_warning >=", value, "signalWarning");
            return (Criteria) this;
        }

        public Criteria andSignalWarningLessThan(Integer value) {
            addCriterion("signal_warning <", value, "signalWarning");
            return (Criteria) this;
        }

        public Criteria andSignalWarningLessThanOrEqualTo(Integer value) {
            addCriterion("signal_warning <=", value, "signalWarning");
            return (Criteria) this;
        }

        public Criteria andSignalWarningIn(List<Integer> values) {
            addCriterion("signal_warning in", values, "signalWarning");
            return (Criteria) this;
        }

        public Criteria andSignalWarningNotIn(List<Integer> values) {
            addCriterion("signal_warning not in", values, "signalWarning");
            return (Criteria) this;
        }

        public Criteria andSignalWarningBetween(Integer value1, Integer value2) {
            addCriterion("signal_warning between", value1, value2, "signalWarning");
            return (Criteria) this;
        }

        public Criteria andSignalWarningNotBetween(Integer value1, Integer value2) {
            addCriterion("signal_warning not between", value1, value2, "signalWarning");
            return (Criteria) this;
        }

        public Criteria andCheckTimeIsNull() {
            addCriterion("check_time is null");
            return (Criteria) this;
        }

        public Criteria andCheckTimeIsNotNull() {
            addCriterion("check_time is not null");
            return (Criteria) this;
        }

        public Criteria andCheckTimeEqualTo(Date value) {
            addCriterion("check_time =", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeNotEqualTo(Date value) {
            addCriterion("check_time <>", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeGreaterThan(Date value) {
            addCriterion("check_time >", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("check_time >=", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeLessThan(Date value) {
            addCriterion("check_time <", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeLessThanOrEqualTo(Date value) {
            addCriterion("check_time <=", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeIn(List<Date> values) {
            addCriterion("check_time in", values, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeNotIn(List<Date> values) {
            addCriterion("check_time not in", values, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeBetween(Date value1, Date value2) {
            addCriterion("check_time between", value1, value2, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeNotBetween(Date value1, Date value2) {
            addCriterion("check_time not between", value1, value2, "checkTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}