package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.CoCompanyMapper;
import com.yff.tuan.model.CoCompany;
import com.yff.tuan.model.CoCompanyExample;
import com.yff.tuan.model.CoCompanyExample.Criteria;
import com.yff.tuan.service.CoCompanyService;
import com.yff.tuan.util.Page;

@Component
public class CoCompanyServiceImpl implements CoCompanyService {
	@Autowired
	CoCompanyMapper companyMapper;
	
	@Override
	public CoCompany find(CoCompany company) {
		CoCompanyExample example = new CoCompanyExample();
		Criteria createCriteria = example.createCriteria();
		createCriteria.andNameEqualTo(company.getName());
		List<CoCompany> companys = companyMapper.selectByExample(example);
		return companys.isEmpty()?null:companys.get(0);
	}

	@Override
	public CoCompany findByPrimaryKey(Integer id) {
		return companyMapper.selectByPrimaryKey(id);
	}

	@Override
	public int insert(CoCompany company) {
		return companyMapper.insert(company);
	}

	@Override
	public int updateByPrimaryKey(CoCompany company) {
		return companyMapper.updateByPrimaryKey(company);
	}
	
	@Override
	public int updateByPrimaryKeySelective(CoCompany company) {
		return companyMapper.updateByPrimaryKeySelective(company);
	}

	@Override
	public List<CoCompany> query(CoCompanyExample example) {
		return companyMapper.selectByExample(example);
	}

	@Override
	public Page<CoCompany> queryByPage(CoCompanyExample example, Integer pageNum) {
		return null;
	}

	@Override
	public int delByPrimaryKey(Integer id) {
		return companyMapper.deleteByPrimaryKey(id);
	}
}
