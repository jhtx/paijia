package com.yff.tuan.model;

public class CtypeSizeTire {
    private Integer id;

    private String name;

    private Integer typeSizeId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTypeSizeId() {
        return typeSizeId;
    }

    public void setTypeSizeId(Integer typeSizeId) {
        this.typeSizeId = typeSizeId;
    }
}