package com.yff.tuan.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.CarBindMapper;
import com.yff.tuan.mapper.CarBindTyreMapper;
import com.yff.tuan.mapper.CarFenceMapper;
import com.yff.tuan.mapper.CarMapper;
import com.yff.tuan.mapper.CarWarningMapper;
import com.yff.tuan.mapper.VehicleCheckMapper;
import com.yff.tuan.model.CarBind;
import com.yff.tuan.model.CarBindTyre;
import com.yff.tuan.model.CarBindTyreExample;
import com.yff.tuan.model.CarWarning;
import com.yff.tuan.model.CarWarningExample;
import com.yff.tuan.model.VehicleCheck;
import com.yff.tuan.model.VehicleCheckExample;
import com.yff.tuan.model.VehicleCheckObject;
import com.yff.tuan.service.VehicleCheckService;
import com.yff.tuan.service.WarningService;

@Component
public class WarningServiceImpl implements WarningService{
	@Autowired
	CarWarningMapper carWarningMapper;
	@Autowired
	CarBindTyreMapper carBindTyreMapper;

	@Override
	public List<CarWarning> queryCarWarning(List<Integer> carIds) {
		CarWarningExample example = new CarWarningExample();
		example.createCriteria().andCarIdIn(carIds);
		example.setOrderByClause(" warn_date desc");
		List<CarWarning> carWarnings = carWarningMapper.selectByExample(example);
		for(CarWarning warning:carWarnings) {
			String productId = warning.getProductId();
			CarBindTyreExample bindTyreExample = new CarBindTyreExample();
			bindTyreExample.createCriteria().andProductIdEqualTo(productId);
			List<CarBindTyre> carBindTyres = carBindTyreMapper.selectByExample(bindTyreExample);
			if(!carBindTyres.isEmpty()) {
				warning.setTireName(carBindTyres.get(0).getTireName());
			}
		}
		return carWarnings;
	}
	
	@Override
	public CarWarning findCarWarning(List<Integer> carIds) {
		CarWarningExample example = new CarWarningExample();
		example.createCriteria().andCarIdIn(carIds);
		example.setOrderByClause(" id desc");
		CarWarning carWarning = carWarningMapper.findByExample(example);
		if(null != carWarning) {
			String productId = carWarning.getProductId();
			CarBindTyreExample bindTyreExample = new CarBindTyreExample();
			bindTyreExample.createCriteria().andProductIdEqualTo(productId);
			List<CarBindTyre> carBindTyres = carBindTyreMapper.selectByExample(bindTyreExample);
			if(!carBindTyres.isEmpty()) {
				carWarning.setTireName(carBindTyres.get(0).getTireName());
			}
		}
		return carWarning;
	}
	
	@Override
	public boolean insert(CarWarning carBind) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int updateCarWarning(CarWarning carBind) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean update(Integer id) {
		return carWarningMapper.deleteByPrimaryKey(id)>0?true:false;
	}
	
	
}
