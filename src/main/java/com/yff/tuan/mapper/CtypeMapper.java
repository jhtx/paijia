package com.yff.tuan.mapper;

import com.yff.tuan.model.Ctype;
import com.yff.tuan.model.CtypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CtypeMapper {
    int countByExample(CtypeExample example);

    int deleteByExample(CtypeExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Ctype record);

    int insertSelective(Ctype record);

    List<Ctype> selectByExample(CtypeExample example);

    Ctype selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Ctype record, @Param("example") CtypeExample example);

    int updateByExample(@Param("record") Ctype record, @Param("example") CtypeExample example);

    int updateByPrimaryKeySelective(Ctype record);

    int updateByPrimaryKey(Ctype record);
}