package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.CoUser;
import com.yff.tuan.model.CoUserCar;
import com.yff.tuan.model.CoUserCarExample;
import com.yff.tuan.model.CoUserExample;
import com.yff.tuan.model.UserCar;
import com.yff.tuan.util.Page;

public interface CoUserService {
	
	public abstract CoUser find(CoUser coUser);
	
	public abstract boolean exist(CoUser coUser);
	
	public abstract CoUser findByPrimaryKey(Integer id);

	public abstract int insert(CoUser coUser);
	
	public abstract boolean delByPrimaryKey(Integer id);
	
	public abstract int updateByPrimaryKey(CoUser coUser);
	
	public abstract int updateByPrimaryKeySelective(CoUser coUser);
	
	public abstract List<CoUser> query(CoUserExample example);
	
	public abstract List<CoUserCar> queryUserCar(CoUserCarExample example);
	
	public abstract boolean saveUserCar(UserCar userCar);
	
	
	public abstract Page<CoUser> queryByPage(CoUserExample example,Integer pageNum);
	
}	
