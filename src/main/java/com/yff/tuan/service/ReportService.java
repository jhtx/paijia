package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.Report;
import com.yff.tuan.model.ReportRecord;

public interface ReportService {
	public abstract List<Report> query(Report report);
	
	public abstract Report find(Report report);
	
	public abstract Report findByPrimaryKey(Integer id);
	
	public abstract Report selectByPrimaryKey(Integer id);
	
	public abstract List<ReportRecord> query(Integer userId,Integer start);
	
	public abstract List<Report> queryUseReports(Integer userId,Integer recordId,Integer fleetId,ReportRecord record);

	public abstract boolean insert(Report report);
	
	public abstract ReportRecord findReportRecordByPrimaryKey(Integer id);
}
