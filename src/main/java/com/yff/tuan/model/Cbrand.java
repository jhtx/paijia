package com.yff.tuan.model;

import java.util.List;

public class Cbrand {
    private Integer id;

    private String name;

    private Integer userId;
    private List<Cmodel> mAdd;
    private List<Cmodel> mUpdate;
    List<Cmodel> cmodels;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

	public List<Cmodel> getCmodels() {
		return cmodels;
	}

	public void setCmodels(List<Cmodel> cmodels) {
		this.cmodels = cmodels;
	}

	public List<Cmodel> getmAdd() {
		return mAdd;
	}

	public List<Cmodel> getmUpdate() {
		return mUpdate;
	}

	public void setmAdd(List<Cmodel> mAdd) {
		this.mAdd = mAdd;
	}

	public void setmUpdate(List<Cmodel> mUpdate) {
		this.mUpdate = mUpdate;
	}
    
    
}