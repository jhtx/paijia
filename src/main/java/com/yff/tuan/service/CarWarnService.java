package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.CarWarn;
import com.yff.tuan.model.CarWarnExample;

public interface CarWarnService {
	
	public abstract List<CarWarn> query(CarWarnExample example);
	
	public abstract CarWarn find(Integer userId);
	
	public abstract boolean add(CarWarn carWarn);
	
	public abstract boolean update(CarWarn carWarn);
	
}
