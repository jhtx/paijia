package com.yff.tuan.util;


/**
 * The Class ApiConstants.
 *
 * @author liuluc
 */
public class ApiConstants {
	
	/** The Constant SHORT_NAME. */
	public static final String SHORT_NAME="yff";
	
	/** The Constant HEADER_RQID. */
	public static final String HEADER_RQID="x-"+SHORT_NAME+"-rqid";
	
	public static final String HEADER_VERSION = "x-"+SHORT_NAME+"-ver";
	
	public static final String HEADER_DUID =  "x-"+SHORT_NAME+"-duid";
	
	
	public static final String RQID=HEADER_RQID;
	
	/** The Constant USER_ID. */
	public static final String USER_ID ="userId";
	
	/** The Constant PHONE_NUMBER_REGEX_PATTERN. */
	public static final String PHONE_NUMBER_REGEX_PATTERN = "^((13|14|15|17|18))\\d{9}$"; 
	
	public static final String PASSWORD_REGEX_PATTERN = "([a-zA-Z0-9+=-_!]){6,20}"; 
	
	public static final String URL_REGEX_PATTERN = "^(http|www|ftp|)?(://)?(\\w+(-\\w+)*)(\\.(\\w+(-\\w+)*))*((:\\d+)?)(/(\\w+(-\\w+)*))*(\\.?(\\w)*)(\\?)?(((\\w*%)*(\\w*\\?)*(\\w*:)*(\\w*\\+)*(\\w*\\.)*(\\w*&)*(\\w*-)*(\\w*=)*(\\w*%)*(\\w*\\?)*(\\w*:)*(\\w*\\+)*(\\w*\\.)*(\\w*&)*(\\w*-)*(\\w*=)*)*(\\w*)*)$";
	public static final String USER_ID_STRING_REGEX_PATTERN = "^(u|U)_\\d+_\\d+_([0-9]|[A-Za-z])+";
	
	/** The Constant VCODE_LENGTH. */
	//MAKE SURE THE LENTH IS MAPPING THE MAX AND MIN
	public static final int VCODE_LENGTH =6;
	
	/** The Constant VCODE_MAX. */
	public static final Integer VCODE_MAX = 999999;
	
	/** The Constant VCODE_MIN. */
	public static final Integer VCODE_MIN = 100000;
	
	public static final String YES ="YES";
	public static final String NO ="NO";
	
	
	//Page
	public static final Integer DEFAULT_PAGE_SIZE =20 ;
	public static final Integer DEFAULT_PAGE_NUM =1 ;
	
	
	 public static void main(String[] args) {
		    String passwd = "aazza44^"; 
		    String pattern = "(?=.*[0-9]|)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,10}";
		    pattern ="(.*[a-zA-Z0-9]).{6,10}";
		    System.out.println(passwd.matches(pattern));
		  }

}
