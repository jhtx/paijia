package com.yff.tuan.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.yff.tuan.mapper.CarBrandMapper;
import com.yff.tuan.mapper.CarFiTyreMapper;
import com.yff.tuan.mapper.CarMapper;
import com.yff.tuan.mapper.CarModelMapper;
import com.yff.tuan.mapper.CarSeTyreMapper;
import com.yff.tuan.mapper.CarThTyreMapper;
import com.yff.tuan.mapper.CarTypeMapper;
import com.yff.tuan.mapper.CarTypeSizeMapper;
import com.yff.tuan.mapper.CoUserMapper;
import com.yff.tuan.mapper.CtypeSizeMapper;
import com.yff.tuan.mapper.CtypeSizeTireMapper;
import com.yff.tuan.mapper.VehicleCheckMapper;
import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarBind;
import com.yff.tuan.model.CarBindExample;
import com.yff.tuan.model.CarBindTyreExample;
import com.yff.tuan.model.CarBrand;
import com.yff.tuan.model.CarExample;
import com.yff.tuan.model.CarExample.Criteria;
import com.yff.tuan.model.CarFiTyre;
import com.yff.tuan.model.CarFiTyreExample;
import com.yff.tuan.model.CarModel;
import com.yff.tuan.model.CarSeTyre;
import com.yff.tuan.model.CarSeTyreExample;
import com.yff.tuan.model.CarThTyre;
import com.yff.tuan.model.CarThTyreExample;
import com.yff.tuan.model.CarTypeSize;
import com.yff.tuan.model.CoUser;
import com.yff.tuan.model.CtypeSize;
import com.yff.tuan.model.CtypeSizeTire;
import com.yff.tuan.model.CtypeSizeTireExample;
import com.yff.tuan.model.VehicleCheck;
import com.yff.tuan.model.VehicleCheckExample;
import com.yff.tuan.service.BindService;
import com.yff.tuan.service.CarService;
import com.yff.tuan.util.Page;

@Component
public class CarServiceImpl implements CarService {
	@Autowired CoUserMapper userMapper;
	@Autowired CarMapper carMapper;
	@Autowired CarBrandMapper carBrandMapper;
	@Autowired CarModelMapper carModelMapper;
	@Autowired CarTypeMapper carTypeMapper;
	@Autowired CarTypeSizeMapper carTypeSizeMapper;
	@Autowired CtypeSizeMapper cTypeSizeMapper;
	@Autowired CtypeSizeTireMapper cTypeSizeTireMapper;
	@Autowired CarFiTyreMapper carFiTyreMapper;
	@Autowired CarSeTyreMapper carSeTyreMapper;
	@Autowired CarThTyreMapper carThTyreMapper;
	@Autowired BindService bindService;
	@Autowired VehicleCheckMapper vehicleCheckMapper;
	
	@Override
	public Car find(Car car) {
		CarExample example = new CarExample();
		Criteria createCriteria = example.createCriteria();
		//createCriteria.andNameEqualTo(car.getName());
		List<Car> cars = carMapper.selectByExample(example);
		return cars.isEmpty()?null:cars.get(0);
	}

	@Override
	public Car findByPrimaryKey(Integer id) {
		Car car = carMapper.selectByPrimaryKey(id);
		if(null != car){
			CarFiTyreExample carFiTyreExample = new CarFiTyreExample();
			carFiTyreExample.createCriteria().andCarIdEqualTo(car.getId());
			car.setCarFiTyreJson(new Gson().toJson(carFiTyreMapper.selectByExample(carFiTyreExample)));
			
			CarSeTyreExample carSeTyreExample = new CarSeTyreExample();
			carSeTyreExample.createCriteria().andCarIdEqualTo(car.getId());
			car.setCarSeTyreJson(new Gson().toJson(carSeTyreMapper.selectByExample(carSeTyreExample)));
			
			CarThTyreExample carThTyreExample = new CarThTyreExample();
			carThTyreExample.createCriteria().andCarIdEqualTo(car.getId());
			car.setCarThTyreJson(new Gson().toJson(carThTyreMapper.selectByExample(carThTyreExample)));
			
			CarBindExample bindExample = new CarBindExample();
			bindExample.createCriteria().andCarIdEqualTo(car.getId());
			car.setCarBind(bindService.findCarBind(bindExample));
			
			CarBindTyreExample bindTyreExample = new CarBindTyreExample();
			bindTyreExample.createCriteria().andCarIdEqualTo(car.getId());
			bindTyreExample.setOrderByClause("id");
			car.setCarBindTyres(bindService.queryCarBindTyre(bindTyreExample));
		}
		return car;
	}
	
	@Override
	public Car selectByPrimaryKey(Integer id) {
		Car car = carMapper.selectByPrimaryKey(id);
		if(null != car){
			CarBindExample bindExample = new CarBindExample();
			bindExample.createCriteria().andCarIdEqualTo(car.getId());
			car.setCarBind(bindService.findCarBind(bindExample));
			
			CarBindTyreExample bindTyreExample = new CarBindTyreExample();
			bindTyreExample.createCriteria().andCarIdEqualTo(car.getId());
			car.setCarBindTyres(bindService.queryCarBindTyre(bindTyreExample));
			
			CtypeSizeTireExample tireExample = new CtypeSizeTireExample();
			tireExample.createCriteria().andTypeSizeIdEqualTo(car.getTypeSizeId());
			List<CtypeSizeTire> cTypeSizeTires = cTypeSizeTireMapper.selectByExample(tireExample);
			car.setcTypeSizeTire(cTypeSizeTires);
		}
		return car;
	}

	@Override
	public boolean insert(Car car) {
		if(null != car && car.getTypeSizeId() != null) {
			CtypeSize ctypeSize = cTypeSizeMapper.selectByPrimaryKey(car.getTypeSizeId());
			if(null != ctypeSize) {
				car.setType(ctypeSize.getTypeId());
			}
		}
		int insert = carMapper.insert(car);
		if(car.getCarFiTyres()!=null && !car.getCarFiTyres().isEmpty() ){
			for(CarFiTyre carFiTyre:car.getCarFiTyres()){
				carFiTyre.setCarId(car.getId());
				carFiTyreMapper.insert(carFiTyre);
			}
		}
		if(car.getCarSeTyres()!=null && !car.getCarSeTyres().isEmpty() ){
			for(CarSeTyre carSeTyre:car.getCarSeTyres()){
				carSeTyre.setCarId(car.getId());
				carSeTyreMapper.insert(carSeTyre);
			}
		}
		if(car.getCarThTyres()!=null && !car.getCarThTyres().isEmpty() ){
			for(CarThTyre carThTyre:car.getCarThTyres()){
				carThTyre.setCarId(car.getId());
				carThTyreMapper.insert(carThTyre);
			}
		}
		return true;
	}
	
	@Override
	public boolean update(Car car) {
		if(null != car && car.getTypeSizeId() != null) {
			CtypeSize ctypeSize = cTypeSizeMapper.selectByPrimaryKey(car.getTypeSizeId());
			if(null != ctypeSize) {
				car.setType(ctypeSize.getTypeId());
			}
		}
		int update = carMapper.updateByPrimaryKeySelective(car);
		CarFiTyreExample carFiTyreExample = new CarFiTyreExample();
		carFiTyreExample.createCriteria().andCarIdEqualTo(car.getId());
		carFiTyreMapper.deleteByExample(carFiTyreExample);
		if(car.getCarFiTyres()!=null && !car.getCarFiTyres().isEmpty() ){
			for(CarFiTyre carFiTyre:car.getCarFiTyres()){
				carFiTyre.setCarId(car.getId());
				carFiTyreMapper.insert(carFiTyre);
			}
		}
		CarSeTyreExample carSeTyreExample = new CarSeTyreExample();
		carSeTyreExample.createCriteria().andCarIdEqualTo(car.getId());
		carSeTyreMapper.deleteByExample(carSeTyreExample);
		if(car.getCarSeTyres()!=null && !car.getCarSeTyres().isEmpty() ){
			for(CarSeTyre carSeTyre:car.getCarSeTyres()){
				carSeTyre.setCarId(car.getId());
				carSeTyreMapper.insert(carSeTyre);
			}
		}
		CarThTyreExample carThTyreExample = new CarThTyreExample();
		carThTyreExample.createCriteria().andCarIdEqualTo(car.getId());
		carThTyreMapper.deleteByExample(carThTyreExample);
		if(car.getCarThTyres()!=null && !car.getCarThTyres().isEmpty() ){
			for(CarThTyre carThTyre:car.getCarThTyres()){
				carThTyre.setCarId(car.getId());
				carThTyreMapper.insert(carThTyre);
			}
		}
		return true;
	}
	

	@Override
	public int updateByPrimaryKeySelective(Car car) {
		if(null != car && car.getTypeSizeId() != null) {
			CtypeSize ctypeSize = cTypeSizeMapper.selectByPrimaryKey(car.getTypeSizeId());
			if(null != ctypeSize) {
				car.setType(ctypeSize.getTypeId());
			}
		}
		return carMapper.updateByPrimaryKeySelective(car);
	}

	@Override
	public List<Car> query(CarExample example) {
		/*for(Car car:cars){
			CarBindExample bindExample = new CarBindExample();
			bindExample.createCriteria().andCarIdEqualTo(car.getId());
			CarBind carBind = bindService.findCarBind(bindExample);
			if(null != carBind){
				car.setCarBind(carBind);
			}
		}*/
		return carMapper.selectByExample(example);
	}
	@Override
	public List<Car> queryCarBind(CarExample example) {
		List<Car> cars = carMapper.selectByExample(example);
		for(Car car:cars){
			CarBindExample bindExample = new CarBindExample();
			bindExample.createCriteria().andCarIdEqualTo(car.getId());
			int carBind = bindService.countCarBind(bindExample);
			car.setBind(carBind);
		}
		return cars;
	}
	
	@Override
	public List<Car> queryUserCars(int role ,int userId) {
		List<Car> cars = null;
		if(role == 1) {
			CarExample example = new CarExample();
			example.createCriteria().andCoUserIdEqualTo(userId);
			cars = carMapper.selectByExample(example);
		}else {
			cars = carMapper.selectByUserId(userId);
		}
		return cars;
	}
	@Override
	public List<Car> queryBindCars(int role,int userId) {
		List<Car> cars = null;
		if(role == 1) {
			cars = carMapper.selectCheckCarByAdminUser(userId);
		}else {
			CoUser user = userMapper.selectByPrimaryKey(userId);
			cars = carMapper.selectCheckCarByNormalUser(user.getParentId(), userId);
		}
		if(null != cars && !cars.isEmpty()) {
			for(Car car:cars) {
				Car c = carMapper.findCheckCar(car.getId());
				if(c != null) {
					car.setState(c.getState());
					car.setMatchLicenseNo(c.getMatchLicenseNo());
					car.setMatchCarId(c.getMatchCarId());
				}
			}
			
			Collections.sort(cars, new Comparator<Car>(){  
	            /*  
	             * int compare(Student o1, Student o2) 返回一个基本类型的整型，  
	             * 返回负数表示：o1 小于o2，  
	             * 返回0 表示：o1和o2相等，  
	             * 返回正数表示：o1大于o2。  
	             */  
	            public int compare(Car car1, Car car2) {  
	              
	                //按照学生的年龄进行升序排列  
	                if(car1.getState() < car2.getState()){  
	                    return 1;  
	                }  
	                if(car1.getState() == car2.getState()){  
	                    return 0;  
	                }  
	                return -1;  
	            }  
	        });  
		}
		return cars;
	}
	
	@Override
	public Page<Car> queryByPage(CarExample example, Integer pageNum) {
		return null;
	}

	@Override
	public int delByPrimaryKey(Integer id) {
		return carMapper.deleteByPrimaryKey(id);
	}

}
