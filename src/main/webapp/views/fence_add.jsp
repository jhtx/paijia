<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (int)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>卡客途安轮卫管家</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<%=path %>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/google.css" rel="stylesheet">
<link href="<%=path %>/css/style.css" rel="stylesheet" type="text/css">
<link href="<%=path %>/css/font-awesome.css" rel="stylesheet">
<link href="<%=path %>/css/bootstrap-select.min.css" rel="stylesheet">
<link rel="apple-touch-icon" sizes="76x76" href="<%=path %>/img/logo.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=path %>/img/logo.png">
<style type="text/css">
html,body{
min-height:100%;
}
</style>
</head>
<body style="margin:0 auto;height: 100%" id="body">
<!-- header -->
<jsp:include page="header.jsp"></jsp:include>
<!-- main -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span12">
          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> 
              <i class="icon-th-list"></i>
              <h3>电子围栏</h3>
              <button type="submit" class="btn btn-primary" id="cel" style="float: right;margin-top:6px;margin-right:5px;">返回</button>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
            	<div class="tabbable">
						<div class="tab-content">
							<div class="tab-pane active" id="veh">
								<table class="table table-striped table-bordered">
									<tr>
											<td>
												<input type="text" class="span4" id="address" style="float: left;" value="${fence.street }">
												<button type="submit" class="btn btn-primary" id="searchPosition" style="float: left;margin-left:15px;">定位</button>
												
												<input type="text" class="span4" id="fenceName" style="float: left;margin-left:80px;"  value="${fence.name }">
												<button type="submit" class="btn btn-primary" id="save" style="float: left;margin-left:15px;">保存</button>
											</td>
									</tr>
								</table>
								<form action="" id="form01" method="post">
								<table class="table table-striped table-bordered">
					                <tbody>
				                		<tr>
											<td><div id="allmap" style="width:100%;height:500px;"></div>
												<input type="hidden" id="lats" value="${fence.lat }">
												<input type="hidden" id="lngs" value="${fence.lng }">
												<input type="hidden" id="lat" name="lat" value="${fence.lat }">
												<input type="hidden" id="lng" name="lng" value="${fence.lng }">
												<input type="hidden" id="street" name="street" value="${fence.street }">
												<input type="hidden" id="name" name="name" value="${fence.name }">
												<input type="hidden" id="id" name="id" value="${fence.id }">
											</td>
										</tr>
					                </tbody>
					              </table>
					              </form>
							</div>
					    </div>
				</div>
			 </div> <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
  <br>
  <br>
  <br>
</div>
<!-- /main -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<%=path %>/js/jquery-1.7.2.min.js"></script> 
<script src="<%=path %>/js/excanvas.min.js"></script>
<script src="<%=path %>/js/bootstrap.js"></script>
<script src="<%=path %>/js/base.js"></script>
<script src="<%=path %>/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="http://api.map.baidu.com/getscript?v=2.0&ak=N392KHKB3hPkqGYElL3OYoHIzoqtZkHj"></script>
<script type="text/javascript">
var minheight = $(window).height();
$("#body").css({
  'min-height':minheight +'px'
})


function theLocation(lng,lat){
	map.clearOverlays(); 
	var new_point = new BMap.Point(lng,lat);
	var marker = new BMap.Marker(new_point);  // 创建标注
	map.addOverlay(marker);              // 将标注添加到地图中
	map.panTo(new_point);
	
	var east =  new_point.lng+0.01141;
	var west =  new_point.lng-0.01141;
	var north =  new_point.lat+0.00899;
	var south =  new_point.lat-0.00899;
	
	var polygon = new BMap.Polygon([
		new BMap.Point(west,north),
		new BMap.Point(east,north),
		new BMap.Point(east,south),
		new BMap.Point(west,south)
		], {strokeColor:"blue", strokeWeight:6, strokeOpacity:0.5});
	map.addOverlay(polygon);
};

$("#cel").click(function(){
	window.location.href="<%=path %>/fence/query.html";
});

var  lng = $('#lngs').val();
var  lat = $('#lats').val();
var map = new BMap.Map("allmap");//创建百度地图实例，这里的allmap是地图容器的id  
map.addControl(new BMap.NavigationControl());  //添加默认缩放平移控件
map.addControl(new BMap.OverviewMapControl()); //添加默认缩略地图控件
map.addControl(new BMap.OverviewMapControl({ isOpen: true, anchor: BMAP_ANCHOR_BOTTOM_RIGHT })); 
if(lng != "" && lat != ""){
	map.centerAndZoom(new BMap.Point(lng,lat),15);
	theLocation(lng,lat);
}else{
	map.centerAndZoom(new BMap.Point(116.404, 39.915),5);
}
map.enableScrollWheelZoom(true);
//theLocation(lng,lat);
// 用经纬度设置地图中心点	


$("#searchPosition").click(function(){
	var address = $("#address").val();
	$("#street").val(address);
	// 创建地址解析器实例
	var myGeo = new BMap.Geocoder();
	// 将地址解析结果显示在地图上,并调整地图视野
	myGeo.getPoint(address, function(point){
		if (point) {
			map.centerAndZoom(point, 16);
			map.clearOverlays();
			map.addOverlay(new BMap.Marker(point));
			$("#lat").val(point.lat);
			$("#lng").val(point.lng);
			
			var east =  point.lng+0.01141;
			var west =  point.lng-0.01141;
			var north =  point.lat+0.00899;
			var south =  point.lat-0.00899;
			
			var polygon = new BMap.Polygon([
				new BMap.Point(west,north),
				new BMap.Point(east,north),
				new BMap.Point(east,south),
				new BMap.Point(west,south)
				], {strokeColor:"blue", strokeWeight:6, strokeOpacity:0.5});
			map.addOverlay(polygon);
			
		}else{
			alert("您选择地址没有解析到结果!");
		}
	}, "");
	
	
});



map.addEventListener("click",function(e){
	theLocation(e.point.lng,e.point.lat);
	$("#lat").val(e.point.lat);
	$("#lng").val(e.point.lng);
	var pt = e.point;
	var geoc = new BMap.Geocoder();
    
	geoc.getLocation(pt, function(rs){
		var addComp = rs.addressComponents;
		$("#address").val(addComp.province + ", " + addComp.city + ", " + addComp.district + ", " + addComp.street + ", " + addComp.streetNumber);
		$("#street").val(addComp.province + ", " + addComp.city + ", " + addComp.district + ", " + addComp.street + ", " + addComp.streetNumber);
	});
}); 


$("#save").click(function(){
	if($("#lat").val()=="" || $("#lng").val()==""){
		alert("请用先定位");
		return;
	}
	if($("#fenceName").val()=="" || $("#fenceName").val()==""){
		alert("请输入电子围栏名称");
		return;
	}else{
		$("#name").val($("#fenceName").val())
	}
	
	$.ajax({
		    url:'<%=path %>/fence/add.html',
		    type:'POST', //GET
		    async:true,    //或false,是否异步
		    data: $("#form01").serializeArray(),
		    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
		    success:function(data){
		        if(data.add==true){
		        	if($("#id").val() != ""){
		        		alert("修改成功！");
		        	}else{
		        		alert("添加成功！");
		        	}
		        	window.location.href="<%=path %>/fence/query.html";
		        }else{
		        	 $("#msg").html(data.resMsg);
		        }
		    },
		    error:function(xhr){},
		});
})

</script>
</body>
</html>