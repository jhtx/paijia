<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (int)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>卡客途安轮卫管家</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<%=path %>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/google.css" rel="stylesheet">
<link href="<%=path %>/css/style.css" rel="stylesheet" type="text/css">
<link href="<%=path %>/css/font-awesome.css" rel="stylesheet">
<link href="<%=path %>/css/bootstrap-select.min.css" rel="stylesheet">
<link rel="apple-touch-icon" sizes="76x76" href="<%=path %>/img/logo.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=path %>/img/logo.png">
<style type="text/css">
html,body{
min-height:100%;
}
</style>
</head>
<body style="margin:0 auto;height: 100%" id="body">
<!-- header -->
<jsp:include page="header.jsp"></jsp:include>
<!-- main -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span12">
          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> 
              <i class="icon-th-list"></i>
              <h3>车队</h3>
              <!-- <button type="submit" class="btn btn-primary" onclick="" style="float: right;margin-top:6px;margin-right:5px;">添加</button> -->
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
            	<div class="tabbable">
					<!-- <ul class="nav nav-tabs">
					  <li>
					    <a href="#formcontrols" data-toggle="tab">Form Controls</a>
					  </li>
					  <li  class="active"><a href="#jscontrols" data-toggle="tab">JS Controls</a></li>
					</ul>
					<br> -->
					<div class="tab-content">
						<form id="form01" class="form-horizontal" method="POST">
								<br>
								<div class="control-group">											
									<label class="control-label" for="no">一级标签<span style="color: red">*</span></label>
									<div class="controls">
										<input type="text" class="span6" id="labelName" name="labelName" value="${label.name }">
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">		
									<label class="control-label" for="no">二级标签<span style="color: red">*</span></label>
									<div class="controls">
										<c:if test="${!empty label.clabelTypes }">
											<c:forEach items="${label.clabelTypes }" var="c">
												<span style="width: 100%">
													<input type="text" style="float: left" class="span6" id="cname" name="cname" value="${c.name }">
													<input type="hidden" style="float: left" class="span6" id="cid" name="cid" value="${c.id }">
												</span>
											</c:forEach>
										</c:if>
										<c:if test="${empty brand.clabelTypes }">
											<span style="width: 100%">
												<input type="text" style="float: left" class="span6" id="cname" name="cname" value="">
												<input type="hidden" style="float: left" class="span6" id="cid" name="cid" value="">
											</span>
										</c:if>
										<span style="width: 100%">
											<button type="button" class="btn btn-primary icon-plus-sign span12" style="float: left;" id="addInput"></button>
										</span>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">	
									${msg }										
								</div> 
								<div class="form-actions" style="text-align: center;">
									<input type="hidden" id="id" name="id" value="${label.id }">
									<button type="button"  id="sub" class="btn btn-primary">提交</button> 
									<button type="button" class="btn" onclick="location.href='<%=path%>/setting/label.html'">取消</button>
								</div> <!-- /form-actions -->
						</form>
					</div>
				</div>
			 </div> <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
  <br>
  <br>
  <br>
</div>
<!-- /main -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<%=path %>/js/jquery-1.7.2.min.js"></script> 
<script src="<%=path %>/js/excanvas.min.js"></script>
<script src="<%=path %>/js/bootstrap.js"></script>
<script src="<%=path %>/js/base.js"></script>
<script src="<%=path %>/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
var minheight = $(window).height();
$("#body").css({
  'min-height':minheight +'px'
})
$("#addInput").click(function(){
	$(this).parent().before('<span style="width: 100%"><input type="text" style="float: left" class="span6" id="cname" name="cname" value=""><input type="hidden" style="float: left" class="span6" id="cid" name="cid" value=""></span>');
});

$("#sub").click(function(){
	var id = $("#id").val();
	var labelName = $("#labelName").val();
	if(labelName==""){
		alert("请输入一级标签");
		return;
	}
	var cname = $("input[name='cname']");
	var cid = $("input[name='cid']");
	var cAdd = [];
	var cUpdate = [];
	for(var i=0;i<cname.length;i++){
		if($(cid[i]).val()!=""){
			var obj = {labelId:id,id:$(cid[i]).val(),name:$(cname[i]).val()};
			cUpdate.push(obj);
		}else{
			if($(cname[i]).val()!=""){
				var obj = {name:$(cname[i]).val()};
				cAdd.push(obj);
			}
		}
		
	}
	if(cAdd.length==0 && cUpdate.length==0){
		alert("请输入二级标签");
		return;
	}
    var json = {id:id,name:labelName,cUpdate:cUpdate,cAdd:cAdd};	
	$.ajax({
	   url:'<%=path%>/setting/label/add',
	   type:'POST', //GET
	   async:true,    //或false,是否异步
	   contentType:"application/json",
	   data:JSON.stringify(json),
	   dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
	   success:function(data){
	       if(data.add==true){
	    	   if(id==""){
	    		   alert("添加成功！");
	    	   }else{
	    		   alert("修改成功！");
	    	   }
	       	window.location.href="<%=path%>/setting/label.html";
		   }
		},
		error : function(xhr) {
			console.log('错误');
			console.log(xhr);
		},
	});
})
</script>
</body>
</html>