package com.yff.tuan.mapper;

import com.yff.tuan.model.CarWarn;
import com.yff.tuan.model.CarWarnExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CarWarnMapper {
    int countByExample(CarWarnExample example);

    int deleteByExample(CarWarnExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CarWarn record);

    int insertSelective(CarWarn record);

    List<CarWarn> selectByExample(CarWarnExample example);

    CarWarn selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CarWarn record, @Param("example") CarWarnExample example);

    int updateByExample(@Param("record") CarWarn record, @Param("example") CarWarnExample example);

    int updateByPrimaryKeySelective(CarWarn record);

    int updateByPrimaryKey(CarWarn record);
}