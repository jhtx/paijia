package com.yff.tuan.mapper;

import com.yff.tuan.model.CtypeSize;
import com.yff.tuan.model.CtypeSizeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CtypeSizeMapper {
    int countByExample(CtypeSizeExample example);

    int deleteByExample(CtypeSizeExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CtypeSize record);

    int insertSelective(CtypeSize record);

    List<CtypeSize> selectByExample(CtypeSizeExample example);

    CtypeSize selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CtypeSize record, @Param("example") CtypeSizeExample example);

    int updateByExample(@Param("record") CtypeSize record, @Param("example") CtypeSizeExample example);

    int updateByPrimaryKeySelective(CtypeSize record);

    int updateByPrimaryKey(CtypeSize record);
}