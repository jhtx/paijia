package com.yff.tuan.mapper;

import com.yff.tuan.model.CarBind;
import com.yff.tuan.model.CarBindExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CarBindMapper {
    int countByExample(CarBindExample example);

    int deleteByExample(CarBindExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CarBind record);

    int insertSelective(CarBind record);

    List<CarBind> selectByExample(CarBindExample example);

    CarBind selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CarBind record, @Param("example") CarBindExample example);

    int updateByExample(@Param("record") CarBind record, @Param("example") CarBindExample example);

    int updateByPrimaryKeySelective(CarBind record);

    int updateByPrimaryKey(CarBind record);
    
    List<CarBind> selectDragCarByUserId(CarBind carBind);
    List<CarBind> selectDrawCarByUserId(CarBind carBind);
    List<CarBind> selectWholeCarByUserId(CarBind carBind);
    
     
}