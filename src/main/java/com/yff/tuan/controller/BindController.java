package com.yff.tuan.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarBind;
import com.yff.tuan.model.CarExample;
import com.yff.tuan.model.CbrandExample;
import com.yff.tuan.model.CmodelExample;
import com.yff.tuan.model.CoFleetExample;
import com.yff.tuan.model.CtypeSizeExample;
import com.yff.tuan.model.CtypeSizeTire;
import com.yff.tuan.model.CtypeSizeTireExample;
import com.yff.tuan.model.CtyreSizeExample;
import com.yff.tuan.model.CarExample.Criteria;
import com.yff.tuan.service.BindService;
import com.yff.tuan.service.BrandService;
import com.yff.tuan.service.CarService;
import com.yff.tuan.service.CoFleetService;
import com.yff.tuan.service.ModelService;
import com.yff.tuan.service.TypeSizeService;
import com.yff.tuan.service.TyreSizeService;

@Controller
@RequestMapping("/bind")
public class BindController {
	@Autowired CarService carService;
	@Autowired BindService bindService;
	
	@RequestMapping("/query")
	public String query(Model model,HttpServletRequest request,HttpServletResponse response) {
		int role = (int) request.getSession().getAttribute("role");
		int userId = (int) request.getSession().getAttribute("userId");
		CarExample example = new CarExample();
		Criteria criteria = example.createCriteria();
		criteria.andCoUserIdEqualTo(userId);
		model.addAttribute("cars", carService.queryCarBind(example));
		return "bind";
	}
	
	@RequestMapping("/toadd")
	public String toadd(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		if(null != id){
			model.addAttribute("car", carService.selectByPrimaryKey(id));
		}
		return "bind_add";
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public Object add(Model model,@RequestBody CarBind carBind,HttpServletRequest request,HttpServletResponse response){
		Map<String, Boolean> map = new HashMap<String,Boolean>();
		map.put("add", bindService.insert(carBind));
		return map;
	}
	
	@RequestMapping("/detail")
	public String detail(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		if(null != id){
			model.addAttribute("car", carService.findByPrimaryKey(id));
		}
		return "bind_detail";
	}
}
