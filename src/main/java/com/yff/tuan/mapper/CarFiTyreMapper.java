package com.yff.tuan.mapper;

import com.yff.tuan.model.CarFiTyre;
import com.yff.tuan.model.CarFiTyreExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CarFiTyreMapper {
    int countByExample(CarFiTyreExample example);

    int deleteByExample(CarFiTyreExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CarFiTyre record);

    int insertSelective(CarFiTyre record);

    List<CarFiTyre> selectByExample(CarFiTyreExample example);

    CarFiTyre selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CarFiTyre record, @Param("example") CarFiTyreExample example);

    int updateByExample(@Param("record") CarFiTyre record, @Param("example") CarFiTyreExample example);

    int updateByPrimaryKeySelective(CarFiTyre record);

    int updateByPrimaryKey(CarFiTyre record);
}