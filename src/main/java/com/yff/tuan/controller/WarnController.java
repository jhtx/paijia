package com.yff.tuan.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarWarning;
import com.yff.tuan.model.CoUser;
import com.yff.tuan.service.CarService;
import com.yff.tuan.service.CoUserService;
import com.yff.tuan.service.WarningService;

@Controller
@RequestMapping("/warn")
public class WarnController {
	@Autowired CoUserService userService;
	@Autowired CarService carService;
	@Autowired WarningService warningService;
	
	@RequestMapping("/query")
	private String query(Model model,HttpServletRequest request,HttpServletResponse response) {
		int role = (int) request.getSession().getAttribute("role");
		int userId = (int) request.getSession().getAttribute("userId");
		List<Car> userCars = carService.queryUserCars(role, userId);
		List<Integer> ids = new ArrayList<Integer>();
		for(Car car:userCars) {
			ids.add(car.getId());
		}
		List<CarWarning> warnings = new ArrayList<CarWarning>();
		if(null != ids && !ids.isEmpty()){
			warnings = warningService.queryCarWarning(ids);
			for(CarWarning warning:warnings) {
				if(warning.getCharge() == null) {
					warning.setMsg(checkpressure(warning));
				}else {
					warning.setMsg(checkCharge(warning));
				}
			}
		}
		
		model.addAttribute("warnings", warnings);
		model.addAttribute("user", userService.findByPrimaryKey(userId));
		return "car_warning";
	}
	
	@RequestMapping("/find")
	@ResponseBody
	private Object find(Model model,HttpServletRequest request,HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String,Object>();
		int role = (int) request.getSession().getAttribute("role");
		int userId = (int) request.getSession().getAttribute("userId");
		CoUser user = userService.findByPrimaryKey(userId);
		if(null != user.getWarn() && user.getWarn()==0) {
			map.put("warn", false);
		}else {
			List<Car> userCars = carService.queryUserCars(role, userId);
			List<Integer> ids = new ArrayList<Integer>();
			for(Car car:userCars) {
				ids.add(car.getId());
			}
			CarWarning warnings = null;
			if(null != ids && !ids.isEmpty()){
				warnings = warningService.findCarWarning(ids);
			}
			String msg = checkpressure(warnings)+checkCharge(warnings);
			map.put("warn", StringUtils.isNotEmpty(msg));
		}
		return map;
	}
	
	@RequestMapping("/del")
	@ResponseBody
	private Object find(Model model,Integer id,HttpServletRequest request,HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("add", warningService.update(id));
		return map;
	}
	
	@RequestMapping("/updateWarn")
	@ResponseBody
	private Object updateWarn(Model model,Integer warn,HttpServletRequest request,HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String,Object>();
		int userId = (int) request.getSession().getAttribute("userId");
		CoUser coUser = new CoUser();
		coUser.setId(userId);
		coUser.setWarn(warn);
		map.put("add", userService.updateByPrimaryKeySelective(coUser)>0?true:false);
		return map;
	}
	
	private String checkpressure(CarWarning warning) {
		String  msg = "";
		if(warning.getPressureUnder30Warning()==1) {
			msg += "严重亏气：<span style=\"font-weight: bold;color:#d81d1d;\">"+warning.getLicenseNo()+"</span> "+warning.getTireName()+ " 胎压已低于标准胎压的30%,目前胎压"+warning.getPressureUnder30();
		}else if(warning.getPressureUnder20Warning()==1){
			msg += "胎压报警：<span style=\"font-weight: bold;color:#d81d1d;\">"+warning.getLicenseNo()+"</span> "+warning.getTireName()+ " 胎压已低于标准胎压的20%,目前胎压"+warning.getPressureUnder20();
		}else if(warning.getPressureUpper20Warning()==1){
			msg += "胎压报警：<span style=\"font-weight: bold;color:#d81d1d;\">"+warning.getLicenseNo()+"</span> "+warning.getTireName()+ " 胎压已高于标准胎压的30%,目前胎压"+warning.getPressureUpper20();
		}else if(warning.getPressureUpper10Warning()==1){
			msg += "胎压提醒：<span style=\"font-weight: bold;color:#d81d1d;\">"+warning.getLicenseNo()+"</span> "+warning.getTireName()+ " 胎压已高于标准胎压的20%,目前胎压"+warning.getPressureUpper10();
		}
		
		if(warning.getTemperatureUpper20Warning()==1) {
			if(!msg.isEmpty()) {
				msg += "<br>";
			}
			msg += "胎温高报警：<span style=\"font-weight: bold;color:#d81d1d;\">"+warning.getLicenseNo()+"</span> "+warning.getTireName()+ " 胎温过高,目前胎温"+warning.getTemperatureUpper20();
		}else if(warning.getTemperatureUpper10Warning()==1){
			if(!msg.isEmpty()) {
				msg += "<br>";
			}
			msg += "胎温高提醒：<span style=\"font-weight: bold;color:#d81d1d;\">"+warning.getLicenseNo()+"</span> "+warning.getTireName()+ " 胎温过高,目前胎温"+warning.getTemperatureUpper10();
		}
		return msg;
	}
	
	
	private String checkCharge(CarWarning warning) {
		String  msg = "";
		
		Integer d1 = null==warning.getD1()?0:warning.getD1();
		Integer d2 = null==warning.getD2()?0:warning.getD2();
		Integer d3 = null==warning.getD3()?0:warning.getD3();
		Integer d4 = null==warning.getD4()?0:warning.getD4();
		Integer d5 = null==warning.getD5()?0:warning.getD5();
		Integer d6 = null==warning.getD6()?0:warning.getD6();
		Integer d7 = null==warning.getD7()?0:warning.getD7();
		if((d1 + d2+ d3+ d4+ d5+ d6+ d7)>0) {
			msg += "灯报警：<span style=\"font-weight: bold;color:#d81d1d;\">"+warning.getLicenseNo()+"</span>";
		}
		if(d1>0) {
			msg += " 左转向灯异常";
		}
		if(d2>0) {
			msg += " 刹车灯异常";
		}
		if(d3>0) {
			msg += " 倒车灯异常";
		}
		if(d4>0) {
			msg += " 右转向灯异常";
		}
		if(d5>0) {
			msg += " 夜间灯异常";
		}
		if(d6>0) {
			msg += " 后雾灯异常";
		}
		if(d7>0) {
			msg += " 备用灯异常";
		}
		if(null != warning.getType() && warning.getType()==3) {
			if(null != warning.getChargeAlarmWarning() && warning.getChargeAlarmWarning()==1) {
				if(!msg.isEmpty()) {
					msg += "<br>";
				}
				msg += "低电量报警：<span style=\"font-weight: bold;color:#d81d1d;\">"+warning.getLicenseNo()+"</span> 目前剩余电量"+warning.getCharge();
			}else if(null != warning.getChargeWarning() && warning.getChargeWarning()==1){
				if(!msg.isEmpty()) {
					msg += "<br>";
				}
				msg += "低电量提醒：<span style=\"font-weight: bold;color:#d81d1d;\">"+warning.getLicenseNo()+"</span> 目前剩余电量"+warning.getCharge();
			}
		}
		return msg;
	}
}
