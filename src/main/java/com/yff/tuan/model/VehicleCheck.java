package com.yff.tuan.model;

import java.util.Date;

public class VehicleCheck {
    private Integer id;

    private String productId;

    private Integer carId;

    private String lat;

    private String lng;

    private String matchProductId;

    private Integer matchCarId;

    private Double speed;

    private Double miles;

    private Double oneMile;

    private String taillight;

    private Double charge;

    private Integer state;

    private Date startDate;
    private Date endDate;
    private Date checkTime;
    
    private double mile;
    
    private String licenseNo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getMatchProductId() {
        return matchProductId;
    }

    public void setMatchProductId(String matchProductId) {
        this.matchProductId = matchProductId;
    }

    public Integer getMatchCarId() {
        return matchCarId;
    }

    public void setMatchCarId(Integer matchCarId) {
        this.matchCarId = matchCarId;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Double getMiles() {
        return miles;
    }

    public void setMiles(Double miles) {
        this.miles = miles;
    }

    public Double getOneMile() {
        return oneMile;
    }

    public void setOneMile(Double oneMile) {
        this.oneMile = oneMile;
    }

    public String getTaillight() {
        return taillight;
    }

    public void setTaillight(String taillight) {
        this.taillight = taillight;
    }

    public Double getCharge() {
        return charge;
    }

    public void setCharge(Double charge) {
        this.charge = charge;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

	public double getMile() {
		return mile;
	}

	public String getLicenseNo() {
		return licenseNo;
	}

	public void setMile(double mile) {
		this.mile = mile;
	}

	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}