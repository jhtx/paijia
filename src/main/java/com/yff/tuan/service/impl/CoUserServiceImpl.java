package com.yff.tuan.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.yff.tuan.mapper.CoUserCarMapper;
import com.yff.tuan.mapper.CoUserMapper;
import com.yff.tuan.model.CoUser;
import com.yff.tuan.model.CoUserCar;
import com.yff.tuan.model.CoUserCarExample;
import com.yff.tuan.model.CoUserExample;
import com.yff.tuan.model.CoUserExample.Criteria;
import com.yff.tuan.model.UserCar;
import com.yff.tuan.service.CoUserService;
import com.yff.tuan.util.AESUtil;
import com.yff.tuan.util.Page;

@Component
public class CoUserServiceImpl implements CoUserService {
	@Autowired CoUserMapper coUserMapper;
	@Autowired CoUserCarMapper coUserCarMapper;
	
	@Override
	public CoUser find(CoUser coUser) {
		CoUserExample example = new CoUserExample();
		Criteria createCriteria = example.createCriteria();
		createCriteria.andNameEqualTo(coUser.getName());
		createCriteria.andPasswordEqualTo(coUser.getPassword());
		List<CoUser> coUsers = coUserMapper.selectByExample(example);
		return coUsers.isEmpty()?null:coUsers.get(0);
	}
	
	@Override
	public boolean exist(CoUser coUser) {
		CoUserExample example = new CoUserExample();
		Criteria createCriteria = example.createCriteria();
		createCriteria.andNameEqualTo(coUser.getName());
		if(null != coUser.getId()) {
			createCriteria.andIdNotEqualTo(coUser.getId());
		}
		return coUserMapper.countByExample(example)>0?true:false;
	}
	
	@Override
	public CoUser findByPrimaryKey(Integer id) {
		CoUser coUser = coUserMapper.selectByPrimaryKey(id);
		if(null != coUser) {
			coUser.setPassword(StringUtils.isEmpty(coUser.getPassword())?"":AESUtil.Decrypt(coUser.getPassword()));
		}
		return coUser;
	}

	@Override
	public int insert(CoUser coUser) {
		return coUserMapper.insert(coUser);
	}

	@Override
	public int updateByPrimaryKeySelective(CoUser coUser) {
		return coUserMapper.updateByPrimaryKeySelective(coUser);
	}

	@Override
	public List<CoUser> query(CoUserExample example) {
		return coUserMapper.selectByExample(example);
	}

	@Override
	public Page<CoUser> queryByPage(CoUserExample example, Integer pageNum) {
		// TODO Auto-generated method stub
		return null;
	}
	

	@Override
	public int updateByPrimaryKey(CoUser coUser) {
		return coUserMapper.updateByPrimaryKey(coUser);
	}

	@Override
	public boolean delByPrimaryKey(Integer id) {
		return coUserMapper.deleteByPrimaryKey(id)>0?true:false;
	}

	@Override
	public List<CoUserCar> queryUserCar(CoUserCarExample example) {
		return coUserCarMapper.selectByExample(example);
	}

	@Override
	@Transactional
	public boolean saveUserCar(UserCar userCar) {
		if(null != userCar) {
			CoUserCarExample example = new CoUserCarExample();
			example.createCriteria().andUserIdEqualTo(userCar.getUserId());
			int i = coUserCarMapper.deleteByExample(example);
			List<CoUserCar> coUserCars = userCar.getCars();
			if(null != coUserCars && !coUserCars.isEmpty()) {
				for(CoUserCar coUserCar:coUserCars) {
					coUserCarMapper.insert(coUserCar);
				}
			}
		}
		return true;
	}
}
