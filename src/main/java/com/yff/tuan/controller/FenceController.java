package com.yff.tuan.controller;


import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yff.tuan.api.ApiResult;
import com.yff.tuan.api.Result;
import com.yff.tuan.model.CarFence;
import com.yff.tuan.model.CarFenceExample;
import com.yff.tuan.model.CarFenceExample.Criteria;
import com.yff.tuan.service.CarService;
import com.yff.tuan.service.FenceService;


@Controller
@RequestMapping("/fence")
public class FenceController {
	@Autowired CarService carService;
	@Autowired FenceService fenceService;
	
	@RequestMapping("/query")
	public String fenceQuery(Model model,HttpServletRequest request,HttpServletResponse response) {
		int role = (int) request.getSession().getAttribute("role");
		int userId = (int) request.getSession().getAttribute("userId");
		CarFenceExample example = new CarFenceExample();
		Criteria createCriteria = example.createCriteria();
		createCriteria.andUserIdEqualTo(userId);
		model.addAttribute("fences", fenceService.queryCarFence(example));
		return "fence";
	}
	
	@RequestMapping("/toadd")
	public String fenceToadd(Model model,Integer id,HttpServletRequest request,HttpServletResponse response) {
		int userId = (int) request.getSession().getAttribute("userId");
		if(null != id){
			model.addAttribute("fence", fenceService.findByPrimaryKey(id));
		}
		
		return "fence_add";
	}
	
	@RequestMapping("/detail")
	public String detail(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		if(null != id){
			model.addAttribute("fence", fenceService.findByPrimaryKey(id));
		}
		return "fence_detail";
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	public Object add(Model model,CarFence carFence,HttpServletRequest request,HttpServletResponse response){
		Map<String, Boolean> map = new HashMap<String,Boolean>();
		int userId = (int) request.getSession().getAttribute("userId");
		carFence.setUserId(userId);
		double lat = carFence.getLat();
		double lng = carFence.getLng();
		
		double east =  lng+0.01141;
		double west =  lng-0.01141;
		double north =  lat+0.00899;
		double south =  lat-0.00899;
		carFence.setEast(east);
		carFence.setNorth(north);
		carFence.setSouth(south);
		carFence.setWest(west);
		if(carFence.getId()==null) {
			map.put("add", fenceService.insert(carFence));
		}else {
			map.put("add", fenceService.update(carFence));
		}
		return map;
	}
	
	@RequestMapping("/del")
	@ResponseBody
	public Object del(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		if(null == id) {
			return ApiResult.ERROR(Result.INVALID_PARAMETERS);
		}
		return ApiResult.SUCCESS(fenceService.delete(id));
	}
}
