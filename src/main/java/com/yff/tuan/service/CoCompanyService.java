package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.CoCompany;
import com.yff.tuan.model.CoCompanyExample;
import com.yff.tuan.util.Page;

public interface CoCompanyService {
	public abstract CoCompany find(CoCompany coCompany);
	
	public abstract CoCompany findByPrimaryKey(Integer id);

	public abstract int insert(CoCompany coCompany);
	
	public abstract int updateByPrimaryKey(CoCompany coCompany);
	
	public abstract int updateByPrimaryKeySelective(CoCompany coCompany);
	
	public abstract int delByPrimaryKey(Integer id);
	
	public abstract List<CoCompany> query(CoCompanyExample example);
	
	
	public abstract Page<CoCompany> queryByPage(CoCompanyExample example,Integer pageNum);
}
