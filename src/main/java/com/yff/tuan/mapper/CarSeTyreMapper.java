package com.yff.tuan.mapper;

import com.yff.tuan.model.CarSeTyre;
import com.yff.tuan.model.CarSeTyreExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CarSeTyreMapper {
    int countByExample(CarSeTyreExample example);

    int deleteByExample(CarSeTyreExample example);

    int insert(CarSeTyre record);

    int insertSelective(CarSeTyre record);

    List<CarSeTyre> selectByExample(CarSeTyreExample example);

    int updateByExampleSelective(@Param("record") CarSeTyre record, @Param("example") CarSeTyreExample example);

    int updateByExample(@Param("record") CarSeTyre record, @Param("example") CarSeTyreExample example);
}