<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (int)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>卡客途安轮卫管家</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<%=path %>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/google.css" rel="stylesheet">
<link href="<%=path %>/css/style.css" rel="stylesheet" type="text/css">
<link href="<%=path %>/css/font-awesome.css" rel="stylesheet">
<link rel="apple-touch-icon" sizes="76x76" href="<%=path %>/img/logo.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=path %>/img/logo.png">
<style type="text/css">
html,body{
min-height:100%;
}
</style>
</head>
<body style="margin:0 auto;height: 100%" id="body">
<!-- header -->
<jsp:include page="header.jsp"></jsp:include>
<!-- main -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span12">
          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> 
              <i class="icon-th-list"></i>
              <h3>车辆</h3>
              <!-- <button type="submit" class="btn btn-primary" onclick="" style="float: right;margin-top:6px;margin-right:5px;">添加</button> -->
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
            	<div class="tabbable">
					<!-- <ul class="nav nav-tabs">
					  <li>
					    <a href="#formcontrols" data-toggle="tab">Form Controls</a>
					  </li>
					  <li  class="active"><a href="#jscontrols" data-toggle="tab">JS Controls</a></li>
					</ul>
					<br> -->
					<div class="tab-content">
							<table class="table table-striped table-bordered">
				                <tbody>
				                	<tr>
					                    <td >车辆编号</td>
					                    <td>${car.no }</td>
				                  	</tr>
				                	<tr>
					                    <td>车队</td>
					                    <td>${car.fleetName }</td>
				                  	</tr>
				                	<tr>
					                    <td>车牌号</td>
					                    <td>${car.licenseNo }</td>
				                  	</tr>
				                	<tr>
					                    <td>车辆类型</td>
					                    <td><c:if test="${car.type==1 }">一体车</c:if> 
											<c:if test="${car.type==2 }">牵引车</c:if> 
											<c:if test="${car.type==3 }">拖车</c:if> 
				                  	</tr>
				                	<tr>
					                    <td>已行驶里程</td>
					                    <td>${car.miles }</td>
				                  	</tr>
				                	<tr>
					                    <td>上牌日期</td>
					                    <td><fmt:formatDate value="${car.licensingDate }" pattern="yyyy-MM-dd" /></td>
				                  	</tr>
				                	<tr>
					                    <td>保险续费日期</td>
					                    <td><fmt:formatDate value="${car.insureDate }" pattern="yyyy-MM-dd" /></td>
				                  	</tr>
				                	<tr>
					                    <td>年检日期</td>
					                    <td><fmt:formatDate value="${car.checkDate }" pattern="yyyy-MM-dd" /></td>
				                  	</tr>
				                	<tr>
					                    <td>报废日期</td>
					                    <td><fmt:formatDate value="${car.scrapDate }" pattern="yyyy-MM-dd" /></td>
				                  	</tr>
				                	<tr>
					                    <td>状态</td>
					                    <td>
					                    	<c:if test="${car.status==1 }">营运</c:if> 
											<c:if test="${car.status==2 }">停驶</c:if> 
											<c:if test="${car.status==3 }">已报废</c:if> 
											<c:if test="${car.status==4 }">待出售</c:if> 
											<c:if test="${car.status==5 }">出租</c:if> 
											<c:if test="${car.status==6 }">其他</c:if> 
					                    </td>
				                  	</tr>
				                	<tr>
					                    <td>产权归属</td>
					                    <td>
					                    	<c:if test="${car.status==1 }">外调</c:if> 
											<c:if test="${car.status==2 }">自有</c:if> 
											<c:if test="${car.status==3 }">租赁</c:if> 
											<c:if test="${car.status==4 }">其他</c:if> 
					                    </td>
				                  	</tr>
				                	<tr>
					                    <td>备注</td>
					                    <td>${car.remark }</td>
				                  	</tr>
				                	<tr>
					                    <td colspan="2" style="text-align: center;">
					                    	<button type="button" class="btn" onclick="location.href='<%=path%>/car/query.html'">返回</button>
					                    </td>
				                  	</tr>
				                </tbody>
				              </table>
					</div>
				</div>
			 </div> <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
  <br>
  <br>
  <br>
</div>
<!-- /main -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<%=path %>/js/jquery-1.7.2.min.js"></script> 
<script src="<%=path %>/js/excanvas.min.js"></script>
<script src="<%=path %>/js/bootstrap.js"></script>
<script src="<%=path %>/js/base.js"></script>
<script type="text/javascript">
var minheight = $(window).height();
$("#body").css({
  'min-height':minheight +'px'
})
</script>
</body>
</html>