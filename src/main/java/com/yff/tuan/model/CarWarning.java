package com.yff.tuan.model;

import java.util.Date;

public class CarWarning {
    private Integer id;

    private String licenseNo;
    private String tireName;
    
    private String productId;

    private Integer carId;

    private String pressureUnder10;

    private Integer pressureUnder10Warning;

    private String pressureUnder20;

    private Integer pressureUnder20Warning;

    private String pressureUnder30;

    private Integer pressureUnder30Warning;

    private String pressureUpper10;

    private Integer pressureUpper10Warning;

    private String pressureUpper20;

    private Integer pressureUpper20Warning;

    private String temperatureUpper10;

    private Integer temperatureUpper10Warning;

    private String temperatureUpper20;

    private Integer temperatureUpper20Warning;

    private String charge;

    private Integer chargeWarning;

    private String chargeAlarm;

    private Integer chargeAlarmWarning;

    private Integer d1;

    private Integer d2;

    private Integer d3;

    private Integer d4;

    private Integer d5;

    private Integer d6;

    private Integer d7;

    private Integer warnTime;

    private Date warnDate;
    
    private String msg;
    
    private Integer type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getPressureUnder10() {
        return pressureUnder10;
    }

    public void setPressureUnder10(String pressureUnder10) {
        this.pressureUnder10 = pressureUnder10;
    }

    public Integer getPressureUnder10Warning() {
        return pressureUnder10Warning;
    }

    public void setPressureUnder10Warning(Integer pressureUnder10Warning) {
        this.pressureUnder10Warning = pressureUnder10Warning;
    }

    public String getPressureUnder20() {
        return pressureUnder20;
    }

    public void setPressureUnder20(String pressureUnder20) {
        this.pressureUnder20 = pressureUnder20;
    }

    public Integer getPressureUnder20Warning() {
        return pressureUnder20Warning;
    }

    public void setPressureUnder20Warning(Integer pressureUnder20Warning) {
        this.pressureUnder20Warning = pressureUnder20Warning;
    }

    public String getPressureUnder30() {
        return pressureUnder30;
    }

    public void setPressureUnder30(String pressureUnder30) {
        this.pressureUnder30 = pressureUnder30;
    }

    public Integer getPressureUnder30Warning() {
        return pressureUnder30Warning;
    }

    public void setPressureUnder30Warning(Integer pressureUnder30Warning) {
        this.pressureUnder30Warning = pressureUnder30Warning;
    }

    public String getPressureUpper10() {
        return pressureUpper10;
    }

    public void setPressureUpper10(String pressureUpper10) {
        this.pressureUpper10 = pressureUpper10;
    }

    public Integer getPressureUpper10Warning() {
        return pressureUpper10Warning;
    }

    public void setPressureUpper10Warning(Integer pressureUpper10Warning) {
        this.pressureUpper10Warning = pressureUpper10Warning;
    }

    public String getPressureUpper20() {
        return pressureUpper20;
    }

    public void setPressureUpper20(String pressureUpper20) {
        this.pressureUpper20 = pressureUpper20;
    }

    public Integer getPressureUpper20Warning() {
        return pressureUpper20Warning;
    }

    public void setPressureUpper20Warning(Integer pressureUpper20Warning) {
        this.pressureUpper20Warning = pressureUpper20Warning;
    }

    public String getTemperatureUpper10() {
        return temperatureUpper10;
    }

    public void setTemperatureUpper10(String temperatureUpper10) {
        this.temperatureUpper10 = temperatureUpper10;
    }

    public Integer getTemperatureUpper10Warning() {
        return temperatureUpper10Warning;
    }

    public void setTemperatureUpper10Warning(Integer temperatureUpper10Warning) {
        this.temperatureUpper10Warning = temperatureUpper10Warning;
    }

    public String getTemperatureUpper20() {
        return temperatureUpper20;
    }

    public void setTemperatureUpper20(String temperatureUpper20) {
        this.temperatureUpper20 = temperatureUpper20;
    }

    public Integer getTemperatureUpper20Warning() {
        return temperatureUpper20Warning;
    }

    public void setTemperatureUpper20Warning(Integer temperatureUpper20Warning) {
        this.temperatureUpper20Warning = temperatureUpper20Warning;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public Integer getChargeWarning() {
        return chargeWarning;
    }

    public void setChargeWarning(Integer chargeWarning) {
        this.chargeWarning = chargeWarning;
    }

    public String getChargeAlarm() {
        return chargeAlarm;
    }

    public void setChargeAlarm(String chargeAlarm) {
        this.chargeAlarm = chargeAlarm;
    }

    public Integer getChargeAlarmWarning() {
        return chargeAlarmWarning;
    }

    public void setChargeAlarmWarning(Integer chargeAlarmWarning) {
        this.chargeAlarmWarning = chargeAlarmWarning;
    }

    public Integer getD1() {
        return d1;
    }

    public void setD1(Integer d1) {
        this.d1 = d1;
    }

    public Integer getD2() {
        return d2;
    }

    public void setD2(Integer d2) {
        this.d2 = d2;
    }

    public Integer getD3() {
        return d3;
    }

    public void setD3(Integer d3) {
        this.d3 = d3;
    }

    public Integer getD4() {
        return d4;
    }

    public void setD4(Integer d4) {
        this.d4 = d4;
    }

    public Integer getD5() {
        return d5;
    }

    public void setD5(Integer d5) {
        this.d5 = d5;
    }

    public Integer getD6() {
        return d6;
    }

    public void setD6(Integer d6) {
        this.d6 = d6;
    }

    public Integer getD7() {
        return d7;
    }

    public void setD7(Integer d7) {
        this.d7 = d7;
    }

    public Integer getWarnTime() {
        return warnTime;
    }

    public void setWarnTime(Integer warnTime) {
        this.warnTime = warnTime;
    }

    public Date getWarnDate() {
        return warnDate;
    }

    public void setWarnDate(Date warnDate) {
        this.warnDate = warnDate;
    }

	public String getTireName() {
		return tireName;
	}


	public void setTireName(String tireName) {
		this.tireName = tireName;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getLicenseNo() {
		return licenseNo;
	}

	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
    
    
}