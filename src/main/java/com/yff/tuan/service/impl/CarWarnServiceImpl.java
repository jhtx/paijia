package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.CarBindMapper;
import com.yff.tuan.mapper.CarBindTyreMapper;
import com.yff.tuan.mapper.CarBrandMapper;
import com.yff.tuan.mapper.CarFiTyreMapper;
import com.yff.tuan.mapper.CarMapper;
import com.yff.tuan.mapper.CarModelMapper;
import com.yff.tuan.mapper.CarSeTyreMapper;
import com.yff.tuan.mapper.CarThTyreMapper;
import com.yff.tuan.mapper.CarTypeSizeMapper;
import com.yff.tuan.mapper.CarWarnMapper;
import com.yff.tuan.mapper.TireCheckMapper;
import com.yff.tuan.mapper.VehicleCheckMapper;
import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarBind;
import com.yff.tuan.model.CarBindExample;
import com.yff.tuan.model.CarBindTyre;
import com.yff.tuan.model.CarBindTyreExample;
import com.yff.tuan.model.CarBrand;
import com.yff.tuan.model.CarExample;
import com.yff.tuan.model.CarExample.Criteria;
import com.yff.tuan.model.CarFiTyre;
import com.yff.tuan.model.CarModel;
import com.yff.tuan.model.CarSeTyre;
import com.yff.tuan.model.CarThTyre;
import com.yff.tuan.model.CarTypeSize;
import com.yff.tuan.model.CarWarn;
import com.yff.tuan.model.CarWarnExample;
import com.yff.tuan.model.TireCheck;
import com.yff.tuan.model.TireCheckExample;
import com.yff.tuan.model.VehicleCheck;
import com.yff.tuan.model.VehicleCheckExample;
import com.yff.tuan.service.BindService;
import com.yff.tuan.service.CarService;
import com.yff.tuan.service.CarWarnService;
import com.yff.tuan.util.Page;

@Component
public class CarWarnServiceImpl implements CarWarnService {
	@Autowired CarWarnMapper carWarnMapper;

	@Override
	public List<CarWarn> query(CarWarnExample example) {
		return carWarnMapper.selectByExample(example);
	}

	@Override
	public CarWarn find(Integer userId) {
		CarWarnExample example = new CarWarnExample();
		example.createCriteria().andUserIdEqualTo(userId);
		List<CarWarn> carWarns = carWarnMapper.selectByExample(example);
		if(null != carWarns && !carWarns.isEmpty()) {
			return carWarns.get(0);
		}
		return null;
	}

	@Override
	public boolean add(CarWarn carWarn) {
		return carWarnMapper.insert(carWarn)>0?true:false;
	}

	@Override
	public boolean update(CarWarn carWarn) {
		return carWarnMapper.updateByPrimaryKey(carWarn)>0?true:false;
	}
	
	
	
}
