package com.yff.tuan.controller;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.yff.tuan.api.ApiResult;
import com.yff.tuan.api.Result;
import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarExample;
import com.yff.tuan.model.CarExample.Criteria;
import com.yff.tuan.model.CarFiTyre;
import com.yff.tuan.model.CarSeTyre;
import com.yff.tuan.model.CarThTyre;
import com.yff.tuan.model.Cbrand;
import com.yff.tuan.model.CbrandExample;
import com.yff.tuan.model.Cmodel;
import com.yff.tuan.model.CmodelExample;
import com.yff.tuan.model.CoCompanyExample;
import com.yff.tuan.model.CoFleetExample;
import com.yff.tuan.model.CtypeSizeExample;
import com.yff.tuan.model.CtyreSizeExample;
import com.yff.tuan.service.BrandService;
import com.yff.tuan.service.CarService;
import com.yff.tuan.service.CoCompanyService;
import com.yff.tuan.service.CoFleetService;
import com.yff.tuan.service.ModelService;
import com.yff.tuan.service.TypeSizeService;
import com.yff.tuan.service.TyreSizeService;


@Controller
@RequestMapping("/car")
public class CarController {
	@Autowired CarService carService;
	@Autowired CoFleetService fleetService;
	@Autowired CoCompanyService coCompanyService;
	@Autowired TypeSizeService typeSizeService;
	@Autowired TyreSizeService tyreSizeService;
	@Autowired BrandService brandService;
	@Autowired ModelService modelService;
	
	@RequestMapping("/query")
	public String query(Model model,HttpServletRequest request,HttpServletResponse response) {
		int role = (int) request.getSession().getAttribute("role");
		int userId = (int) request.getSession().getAttribute("userId");
		CarExample example = new CarExample();
		Criteria criteria = example.createCriteria();
		criteria.andCoUserIdEqualTo(userId);
		model.addAttribute("cars", carService.query(example));
		return "car";
	}
	
	@RequestMapping("/detail")
	public String detail(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		if(null != id){
			model.addAttribute("car", carService.findByPrimaryKey(id));
		}
		return "car_detail";
	}
	
	@RequestMapping("/toadd")
	public String toadd(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		int userId = (int) request.getSession().getAttribute("userId");
		Car car = null;
		if(null != id){
			car = carService.findByPrimaryKey(id);
			model.addAttribute("car", car);
		}else {
			car = new Car();
			car.setCarFiTyreJson(new Gson().toJson(new ArrayList<CarFiTyre>()));
			car.setCarSeTyreJson(new Gson().toJson(new ArrayList<CarSeTyre>()));
			car.setCarThTyreJson(new Gson().toJson(new ArrayList<CarThTyre>()));
			model.addAttribute("car", car);
		}
		model.addAttribute("typeSizes", typeSizeService.query(new CtypeSizeExample()));
		CbrandExample example = new CbrandExample();
		example.createCriteria().andUserIdEqualTo(userId);
		List<Cbrand> cbrands = brandService.query(example);
		model.addAttribute("brands", cbrands);
		if(null != car && null != car.getBrandId()){
			CmodelExample examplem=new CmodelExample();
			examplem.createCriteria().andBrandIdEqualTo(car.getBrandId());
			model.addAttribute("models", modelService.query(examplem));
		}else{
			if(null != cbrands && !cbrands.isEmpty()){
				CmodelExample examplec=new CmodelExample();
				examplec.createCriteria().andBrandIdEqualTo(cbrands.get(0).getId());
				model.addAttribute("models", modelService.query(examplec));
			}
		}
		model.addAttribute("tyreSizes", tyreSizeService.query(new CtyreSizeExample()));
		
		CoFleetExample coFleetExample= new CoFleetExample();
		coFleetExample.createCriteria().andCoUserIdEqualTo(userId);
		model.addAttribute("fleets", fleetService.query(coFleetExample));
		model.addAttribute("id", id);
		/*CoCompanyExample coCompanyExample = new CoCompanyExample();
		coCompanyExample.createCriteria().andCoUserIdEqualTo(userId);
		model.addAttribute("companies", coCompanyService.query(coCompanyExample));*/
		return "car_add";
	}
	
	@RequestMapping("/queryModel")
	@ResponseBody
	public Object queryModel(Model model,Integer brandId,HttpServletRequest request,HttpServletResponse response){
		List<Cmodel> models = new ArrayList<Cmodel>();
		if(null != brandId) {
			CmodelExample example=new CmodelExample();
			example.createCriteria().andBrandIdEqualTo(brandId);
			models = modelService.query(example);
		}
		return ApiResult.SUCCESS(models);
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public Object add(Model model,@RequestBody Car car,HttpServletRequest request,HttpServletResponse response){
		Map<String, Boolean> map = new HashMap<String,Boolean>();
		int userId = (int) request.getSession().getAttribute("userId");
		car.setCoUserId(userId);
		if(car.getId()==null) {
			map.put("add", carService.insert(car));
		}else {
			map.put("add", carService.update(car));
		}
		return map;
	}
	
	@RequestMapping("/del")
	@ResponseBody
	public Object add(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		if(null == id) {
			return ApiResult.ERROR(Result.INVALID_PARAMETERS);
		}
		return ApiResult.SUCCESS(carService.delByPrimaryKey(id));
	}
}
