package com.yff.tuan.mapper;

import com.yff.tuan.model.CarThTyre;
import com.yff.tuan.model.CarThTyreExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CarThTyreMapper {
    int countByExample(CarThTyreExample example);

    int deleteByExample(CarThTyreExample example);

    int insert(CarThTyre record);

    int insertSelective(CarThTyre record);

    List<CarThTyre> selectByExample(CarThTyreExample example);

    int updateByExampleSelective(@Param("record") CarThTyre record, @Param("example") CarThTyreExample example);

    int updateByExample(@Param("record") CarThTyre record, @Param("example") CarThTyreExample example);
}