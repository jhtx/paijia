package com.yff.tuan.mapper;

import com.yff.tuan.model.Cbrand;
import com.yff.tuan.model.CbrandExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CbrandMapper {
    int countByExample(CbrandExample example);

    int deleteByExample(CbrandExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Cbrand record);

    int insertSelective(Cbrand record);

    List<Cbrand> selectByExample(CbrandExample example);

    Cbrand selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Cbrand record, @Param("example") CbrandExample example);

    int updateByExample(@Param("record") Cbrand record, @Param("example") CbrandExample example);

    int updateByPrimaryKeySelective(Cbrand record);

    int updateByPrimaryKey(Cbrand record);
}