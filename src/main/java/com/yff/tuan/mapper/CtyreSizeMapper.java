package com.yff.tuan.mapper;

import com.yff.tuan.model.CtyreSize;
import com.yff.tuan.model.CtyreSizeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CtyreSizeMapper {
    int countByExample(CtyreSizeExample example);

    int deleteByExample(CtyreSizeExample example);

    int insert(CtyreSize record);

    int insertSelective(CtyreSize record);

    List<CtyreSize> selectByExample(CtyreSizeExample example);

    int updateByExampleSelective(@Param("record") CtyreSize record, @Param("example") CtyreSizeExample example);

    int updateByExample(@Param("record") CtyreSize record, @Param("example") CtyreSizeExample example);
}