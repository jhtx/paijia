package com.yff.tuan.model;

public class CarTypeSize {
    private Integer id;

    private Integer carId;

    private Integer typeSizeId;
    
    private String typeSizeName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getTypeSizeId() {
        return typeSizeId;
    }

    public void setTypeSizeId(Integer typeSizeId) {
        this.typeSizeId = typeSizeId;
    }

	public String getTypeSizeName() {
		return typeSizeName;
	}

	public void setTypeSizeName(String typeSizeName) {
		this.typeSizeName = typeSizeName;
	}
    
    
}