package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.CoPositionMapper;
import com.yff.tuan.model.CoPosition;
import com.yff.tuan.model.CoPositionExample;
import com.yff.tuan.service.PositionService;


@Component
public class PositionServiceImpl implements PositionService {
	@Autowired CoPositionMapper mapper;
	
	@Override
	public CoPosition findByPrimaryKey(Integer id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public List<CoPosition> query(CoPositionExample example) {
		return mapper.selectByExample(example);
	}

	@Override
	public boolean save(CoPosition position) {
		
		return mapper.insert(position)>0?true:false;
	}
	
	@Override
	public boolean update(CoPosition position) {
		
		return mapper.updateByPrimaryKeySelective(position)>0?true:false;
	}
	

}
