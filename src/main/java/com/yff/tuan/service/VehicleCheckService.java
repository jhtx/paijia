package com.yff.tuan.service;

import java.util.Date;
import java.util.List;

import com.yff.tuan.model.Car;
import com.yff.tuan.model.VehicleCheck;
import com.yff.tuan.model.VehicleCheckObject;

public interface VehicleCheckService {
	public abstract boolean save(VehicleCheck vehicleCheck);
	public abstract List<VehicleCheckObject> queryDrag(Integer userId,Date startTime ,Date endTime,Integer fleetId,String licenseNo,Integer typeSizeId);
	public abstract List<VehicleCheckObject> queryDraw(Integer userId,Date startTime ,Date endTime,Integer fleetId,String licenseNo,Integer typeSizeId);
	public abstract List<VehicleCheckObject> queryWhole(Integer userId,Date startTime ,Date endTime,Integer fleetId,String licenseNo,Integer typeSizeId);
	public abstract List<Car> selectCheckCarDispatch(Integer userId);
	public abstract VehicleCheckObject findByProductId(Integer userId,String productId,Date startTime ,Date endTime);
}
