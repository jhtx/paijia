package com.yff.tuan.mapper;

import com.yff.tuan.model.CoUserCar;
import com.yff.tuan.model.CoUserCarExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CoUserCarMapper {
    int countByExample(CoUserCarExample example);

    int deleteByExample(CoUserCarExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CoUserCar record);

    int insertSelective(CoUserCar record);

    List<CoUserCar> selectByExample(CoUserCarExample example);

    CoUserCar selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CoUserCar record, @Param("example") CoUserCarExample example);

    int updateByExample(@Param("record") CoUserCar record, @Param("example") CoUserCarExample example);

    int updateByPrimaryKeySelective(CoUserCar record);

    int updateByPrimaryKey(CoUserCar record);
}