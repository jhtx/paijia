package com.yff.tuan.controller;


import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yff.tuan.model.CoFleet;
import com.yff.tuan.model.CoFleetExample;
import com.yff.tuan.model.CtypeSizeExample;
import com.yff.tuan.model.ReportRecord;
import com.yff.tuan.model.VehicleCheckObject;
import com.yff.tuan.service.ReportService;
import com.yff.tuan.service.CoFleetService;
import com.yff.tuan.service.TypeSizeService;
import com.yff.tuan.service.VehicleCheckService;
import com.yff.tuan.util.DateUtil;
import com.yff.tuan.util.MathUtil;


@Controller
@RequestMapping("/data")
public class MatchCarController {
	@Autowired VehicleCheckService checkService;
	@Autowired TypeSizeService typeSizeService;
	@Autowired CoFleetService fleetService;
	@Autowired ReportService reportService;
	//@Autowired VeService bindService;
	
	@RequestMapping("/drag/query")
	public String dragQuery(Model model,String startDate ,String endDate,Integer fleetId,String licenseNo,Integer typeSizeId,HttpServletRequest request,HttpServletResponse response) {
		int userId = (int) request.getSession().getAttribute("userId");
		Date start = DateUtil.getDate(startDate, DateUtil.FORMAT_YYMMDD);
		Date end = DateUtil.get24HourDate(DateUtil.getDate(endDate, DateUtil.FORMAT_YYMMDD));
		List<VehicleCheckObject> matches = checkService.queryDrag(userId,start,end,fleetId,licenseNo,typeSizeId);
		double miles = 0;
		for(VehicleCheckObject object:matches) {
			miles += object.getMiles();
		}
		model.addAttribute("matches", matches);
		CtypeSizeExample sizeExample = new CtypeSizeExample();
		sizeExample.createCriteria().andTypeIdEqualTo(3);
		model.addAttribute("typeSizes", typeSizeService.query(sizeExample));
		CoFleetExample fleetExample = new CoFleetExample();
		fleetExample.createCriteria().andCoUserIdEqualTo(userId);
		model.addAttribute("fleets", fleetService.query(fleetExample));
		model.addAttribute("startDate", start);
		model.addAttribute("endDate", end);
		model.addAttribute("fleetId", fleetId);
		model.addAttribute("licenseNo", licenseNo);
		model.addAttribute("typeSizeId", typeSizeId);
		model.addAttribute("miles", MathUtil.parseDouble(miles));
		return "data_drag";
	}
	@RequestMapping("/drag/detail")
	public String dragDetail(Model model,String startDate ,String endDate,String productId,HttpServletRequest request,HttpServletResponse response) {
		int userId = (int) request.getSession().getAttribute("userId");
		Date start = DateUtil.getDate(startDate, DateUtil.FORMAT_YYMMDD);
		Date end = DateUtil.get24HourDate(DateUtil.getDate(endDate, DateUtil.FORMAT_YYMMDD));
		VehicleCheckObject object = checkService.findByProductId(userId,productId,start,end);
		model.addAttribute("match", object);
		model.addAttribute("startDate", start);
		model.addAttribute("endDate", end);
		return "data_drag_detail";
	}
	@RequestMapping("drag/dispatch")
	public String dragDispatch(Model model,String productId,HttpServletRequest request,HttpServletResponse response) {
		int userId = (int) request.getSession().getAttribute("userId");
		model.addAttribute("cars", checkService.selectCheckCarDispatch(userId));
		return "data_drag_dispatch";
	}
	@RequestMapping("/draw/query")
	public String drawQuery(Model model,String startDate ,String endDate,Integer fleetId,String licenseNo,Integer typeSizeId,HttpServletRequest request,HttpServletResponse response) {
		int userId = (int) request.getSession().getAttribute("userId");
		Date start = DateUtil.getDate(startDate, DateUtil.FORMAT_YYMMDD);
		Date end = DateUtil.get24HourDate(DateUtil.getDate(endDate, DateUtil.FORMAT_YYMMDD));
		List<VehicleCheckObject> matches = checkService.queryDraw(userId,start,end,fleetId,licenseNo,typeSizeId);
		double miles = 0;
		for(VehicleCheckObject object:matches) {
			miles += object.getMiles();
		}
		model.addAttribute("matches", matches);
		CtypeSizeExample sizeExample = new CtypeSizeExample();
		sizeExample.createCriteria().andTypeIdEqualTo(2);
		model.addAttribute("typeSizes", typeSizeService.query(sizeExample));
		CoFleetExample fleetExample = new CoFleetExample();
		fleetExample.createCriteria().andCoUserIdEqualTo(userId);
		model.addAttribute("fleets", fleetService.query(fleetExample));
		model.addAttribute("startDate", start);
		model.addAttribute("endDate", end);
		model.addAttribute("fleetId", fleetId);
		model.addAttribute("licenseNo", licenseNo);
		model.addAttribute("typeSizeId", typeSizeId);
		model.addAttribute("miles", MathUtil.parseDouble(miles));
		return "data_draw";
	}
	@RequestMapping("/whole/query")
	public String wholeQuery(Model model,String startDate ,String endDate,Integer fleetId,String licenseNo,Integer typeSizeId,HttpServletRequest request,HttpServletResponse response) {
		int userId = (int) request.getSession().getAttribute("userId");
		Date start = DateUtil.getDate(startDate, DateUtil.FORMAT_YYMMDD);
		Date end = DateUtil.get24HourDate(DateUtil.getDate(endDate, DateUtil.FORMAT_YYMMDD));
		List<VehicleCheckObject> matches = checkService.queryWhole(userId,start,end,fleetId,licenseNo,typeSizeId);
		double miles = 0;
		for(VehicleCheckObject object:matches) {
			miles += object.getMiles();
		}
		model.addAttribute("matches", matches);
		CtypeSizeExample sizeExample = new CtypeSizeExample();
		sizeExample.createCriteria().andTypeIdEqualTo(1);
		model.addAttribute("typeSizes", typeSizeService.query(sizeExample));
		CoFleetExample fleetExample = new CoFleetExample();
		fleetExample.createCriteria().andCoUserIdEqualTo(userId);
		model.addAttribute("fleets", fleetService.query(fleetExample));
		model.addAttribute("startDate", start);
		model.addAttribute("endDate", end);
		model.addAttribute("fleetId", fleetId);
		model.addAttribute("licenseNo", licenseNo);
		model.addAttribute("typeSizeId", typeSizeId);
		model.addAttribute("miles", MathUtil.parseDouble(miles));
		return "data_whole";
	}
	
	@RequestMapping("/report")
	public String report(Model model,Integer start,HttpServletRequest request,HttpServletResponse response){
		int userId = (int) request.getSession().getAttribute("userId");
		model.addAttribute("reports", reportService.query(userId, null == start?0:start));
		return "report_record";
	}
	
	@RequestMapping("/report/query")
	public String reportQuery(Model model,Integer id,Integer fleetId,HttpServletRequest request,HttpServletResponse response){
		int userId = (int) request.getSession().getAttribute("userId");
		CoFleetExample fleetExample = new CoFleetExample();
		fleetExample.createCriteria().andCoUserIdEqualTo(userId);
		List<CoFleet> fleets = fleetService.query(fleetExample);
		model.addAttribute("fleets", fleets);
		if(null != fleets && !fleets.isEmpty() && fleetId ==null) {
			fleetId = fleets.get(0).getId();
		}
		ReportRecord record = reportService.findReportRecordByPrimaryKey(id);
		model.addAttribute("reports", reportService.queryUseReports(userId,id,fleetId,record));
		model.addAttribute("fleetId", fleetId);
		model.addAttribute("id", id);
		if(null != record) {
			model.addAttribute("checkDate", DateUtil.getFormatDate(record.getCheckDate(), DateUtil.FORMAT_YYMMDD));
		}
		return "report_query";
	}
	/*@RequestMapping("/report/print")
	public String reportprint(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		int userId = (int) request.getSession().getAttribute("userId");
		model.addAttribute("reports", reportService.queryUseReports(userId,id));
		return "report_print";
	}*/
	/*
	@RequestMapping("/data")
	@ResponseBody
	public Object data(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		LinkedHashMap<String, Object> map = new LinkedHashMap<String,Object>();
		if(null != id){
			Car car = carService.findByPrimaryKey(id);
			map.put("car", carService.findByPrimaryKey(id));
			if(null != car && car.getCarBind() != null){
				map.put("vchecks", bindService.findVehicleCheck(car.getCarBind().getProductId()));
				map.put("fencecheck", bindService.findVehicleCheckInFence(car.getCarBind().getProductId()));
			}
			if(null != car && car.getCarBindTyres() != null && !car.getCarBindTyres().isEmpty()){
				List<TireCheck> list = new ArrayList<TireCheck>();
				for(CarBindTyre carBindTyre:car.getCarBindTyres()){
					TireCheck check = bindService.findTireCheck(carBindTyre.getProductId());
					if(null == check){
						check = new TireCheck();
					}
					check.setTyreNo(carBindTyre.getTireName());
					list.add(check);
				}
				map.put("tchecks", list);
			}
		}
		return map;
	}*/
}
