package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.Clabel;
import com.yff.tuan.model.ClabelExample;
import com.yff.tuan.model.ClabelType;
import com.yff.tuan.model.Cmodel;

public interface LabelService {
	
	public abstract List<Clabel> query(ClabelExample example);
	
	public abstract Clabel find(Integer id);
	
	public abstract boolean add(Clabel clabel);
	
	public abstract boolean update(Clabel clabel);
	
	public abstract List<ClabelType> queryClabelType(Integer labelTypeId);
	
}
