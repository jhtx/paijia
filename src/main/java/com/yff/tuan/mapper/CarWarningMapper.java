package com.yff.tuan.mapper;

import com.yff.tuan.model.CarWarning;
import com.yff.tuan.model.CarWarningExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CarWarningMapper {
    int countByExample(CarWarningExample example);

    int deleteByExample(CarWarningExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CarWarning record);

    int insertSelective(CarWarning record);

    List<CarWarning> selectByExample(CarWarningExample example);
    
    CarWarning findByExample(CarWarningExample example);

    CarWarning selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CarWarning record, @Param("example") CarWarningExample example);

    int updateByExample(@Param("record") CarWarning record, @Param("example") CarWarningExample example);

    int updateByPrimaryKeySelective(CarWarning record);

    int updateByPrimaryKey(CarWarning record);
}