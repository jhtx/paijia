package com.yff.tuan.mapper;

import com.yff.tuan.model.Cmodel;
import com.yff.tuan.model.CmodelExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CmodelMapper {
    int countByExample(CmodelExample example);

    int deleteByExample(CmodelExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Cmodel record);

    int insertSelective(Cmodel record);

    List<Cmodel> selectByExample(CmodelExample example);

    Cmodel selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Cmodel record, @Param("example") CmodelExample example);

    int updateByExample(@Param("record") Cmodel record, @Param("example") CmodelExample example);

    int updateByPrimaryKeySelective(Cmodel record);

    int updateByPrimaryKey(Cmodel record);
}