package com.yff.tuan.mapper;

import com.yff.tuan.model.Ctyre;
import com.yff.tuan.model.CtyreExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CtyreMapper {
    int countByExample(CtyreExample example);

    int deleteByExample(CtyreExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Ctyre record);

    int insertSelective(Ctyre record);

    List<Ctyre> selectByExample(CtyreExample example);

    Ctyre selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Ctyre record, @Param("example") CtyreExample example);

    int updateByExample(@Param("record") Ctyre record, @Param("example") CtyreExample example);

    int updateByPrimaryKeySelective(Ctyre record);

    int updateByPrimaryKey(Ctyre record);
}