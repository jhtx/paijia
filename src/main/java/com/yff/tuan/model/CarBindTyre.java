package com.yff.tuan.model;

public class CarBindTyre {
    private Integer id;

    private Integer carId;

    private Integer tireId;

    private String productId;
    private String tireName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getTireId() {
        return tireId;
    }

    public void setTireId(Integer tireId) {
        this.tireId = tireId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

	public String getTireName() {
		return tireName;
	}

	public void setTireName(String tireName) {
		this.tireName = tireName;
	}
    
}