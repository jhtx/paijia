package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarExample;
import com.yff.tuan.util.Page;

public interface CarService {
	public abstract Car find(Car car);
	
	public abstract Car findByPrimaryKey(Integer id);
	
	public abstract Car selectByPrimaryKey(Integer id);

	public abstract boolean insert(Car car);
	
	public abstract boolean update(Car car);
	
	public abstract int updateByPrimaryKeySelective(Car car);
	
	public abstract int delByPrimaryKey(Integer id);
	
	public abstract List<Car> query(CarExample example);
	public abstract List<Car> queryCarBind(CarExample example);
	
	public abstract List<Car> queryBindCars(int role,int userId);
	
	public abstract List<Car> queryUserCars(int role ,int userId);
	
	public abstract Page<Car> queryByPage(CarExample example,Integer pageNum);
}
