package com.yff.tuan.mapper;

import com.yff.tuan.model.CarTypeSize;
import com.yff.tuan.model.CarTypeSizeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CarTypeSizeMapper {
    int countByExample(CarTypeSizeExample example);

    int deleteByExample(CarTypeSizeExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CarTypeSize record);

    int insertSelective(CarTypeSize record);

    List<CarTypeSize> selectByExample(CarTypeSizeExample example);

    CarTypeSize selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CarTypeSize record, @Param("example") CarTypeSizeExample example);

    int updateByExample(@Param("record") CarTypeSize record, @Param("example") CarTypeSizeExample example);

    int updateByPrimaryKeySelective(CarTypeSize record);

    int updateByPrimaryKey(CarTypeSize record);
}