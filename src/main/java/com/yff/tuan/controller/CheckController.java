package com.yff.tuan.controller;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.yff.tuan.api.ApiResult;
import com.yff.tuan.api.Result;
import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarBindTyre;
import com.yff.tuan.model.CarExample;
import com.yff.tuan.model.TireCheck;
import com.yff.tuan.model.CarExample.Criteria;
import com.yff.tuan.model.CheckObj;
import com.yff.tuan.service.BindService;
import com.yff.tuan.service.CarService;


@Controller
@RequestMapping("/check")
public class CheckController {
	@Autowired CarService carService;
	@Autowired BindService bindService;
	
	@RequestMapping("/query")
	public String query(Model model,HttpServletRequest request,HttpServletResponse response) {
		int role = (int) request.getSession().getAttribute("role");
		int userId = (int) request.getSession().getAttribute("userId");
		model.addAttribute("cars", carService.queryBindCars(role,userId));
		return "check";
	}
	
	@RequestMapping("/detail")
	public String detail(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		if(null != id){
			Car car = carService.findByPrimaryKey(id);
			model.addAttribute("car", carService.findByPrimaryKey(id));
			if(null != car && car.getCarBind() != null){
				model.addAttribute("vchecks", new Gson().toJson(bindService.findVehicleCheck(car.getCarBind().getProductId())));
				model.addAttribute("fencecheck", new Gson().toJson(bindService.findVehicleCheckInFence(car.getCarBind().getProductId())).toString());
			}
			if(null != car && car.getCarBindTyres() != null && !car.getCarBindTyres().isEmpty()){
				List<TireCheck> list = new ArrayList<TireCheck>();
				for(CarBindTyre carBindTyre:car.getCarBindTyres()){
					TireCheck check = bindService.findTireCheck(carBindTyre.getProductId());
					if(null == check){
						check = new TireCheck();
					}
					check.setTyreNo(carBindTyre.getTireName());
					list.add(check);
				}
				model.addAttribute("tchecks", new Gson().toJson(list));
			}
		}
		return "check_detail";
	}
	
	@RequestMapping("/data")
	@ResponseBody
	public Object data(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		LinkedHashMap<String, Object> map = new LinkedHashMap<String,Object>();
		if(null != id){
			Car car = carService.findByPrimaryKey(id);
			map.put("car", carService.findByPrimaryKey(id));
			if(null != car && car.getCarBind() != null){
				map.put("vchecks", bindService.findVehicleCheck(car.getCarBind().getProductId()));
				map.put("fencecheck", bindService.findVehicleCheckInFence(car.getCarBind().getProductId()));
			}
			if(null != car && car.getCarBindTyres() != null && !car.getCarBindTyres().isEmpty()){
				List<TireCheck> list = new ArrayList<TireCheck>();
				for(CarBindTyre carBindTyre:car.getCarBindTyres()){
					TireCheck check = bindService.findTireCheck(carBindTyre.getProductId());
					if(null == check){
						check = new TireCheck();
					}
					check.setTyreNo(carBindTyre.getTireName());
					list.add(check);
				}
				map.put("tchecks", list);
			}
		}
		return map;
	}
}
