package com.yff.tuan.mapper;

import com.yff.tuan.model.Clabel;
import com.yff.tuan.model.ClabelExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ClabelMapper {
    int countByExample(ClabelExample example);

    int deleteByExample(ClabelExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Clabel record);

    int insertSelective(Clabel record);

    List<Clabel> selectByExample(ClabelExample example);

    Clabel selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Clabel record, @Param("example") ClabelExample example);

    int updateByExample(@Param("record") Clabel record, @Param("example") ClabelExample example);

    int updateByPrimaryKeySelective(Clabel record);

    int updateByPrimaryKey(Clabel record);
}