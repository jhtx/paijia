package com.yff.tuan.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yff.tuan.api.ApiResult;
import com.yff.tuan.api.Result;
import com.yff.tuan.model.CoCompany;
import com.yff.tuan.model.CoCompanyExample;
import com.yff.tuan.model.CoCompanyExample.Criteria;
import com.yff.tuan.service.CoCompanyService;


@Controller
@RequestMapping("/company")
public class CompanyController {
	@Autowired CoCompanyService coCompanyService;
	
	@RequestMapping("/query")
	public String query(Model model,HttpServletRequest request,HttpServletResponse response) {
		int role = (int) request.getSession().getAttribute("role");
		int userId = (int) request.getSession().getAttribute("userId");
		CoCompanyExample example = new CoCompanyExample();
		Criteria criteria = example.createCriteria();
		criteria.andCoUserIdEqualTo(userId);
		model.addAttribute("company", coCompanyService.query(example));
		return "company";
	}
	
	@RequestMapping("/detail")
	public String detail(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		if(null != id){
			model.addAttribute("company", coCompanyService.findByPrimaryKey(id));
		}
		return "company_detail";
	}
	
	@RequestMapping("/toadd")
	public String toadd(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		if(null != id){
			model.addAttribute("company", coCompanyService.findByPrimaryKey(id));
		}
		return "company_add";
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public Object add(Model model,CoCompany coCompany,HttpServletRequest request,HttpServletResponse response){
		if(null == coCompany) {
			return ApiResult.ERROR(Result.INVALID_PARAMETERS);
		}
		int userId = (int) request.getSession().getAttribute("userId");
		coCompany.setCoUserId(userId);
		if(null  != coCompany.getId()){
			return ApiResult.SUCCESS(coCompanyService.updateByPrimaryKey(coCompany));
		}else{
			return ApiResult.SUCCESS(coCompanyService.insert(coCompany));
		}
	}
	
	@RequestMapping("/del")
	@ResponseBody
	public Object add(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		if(null == id) {
			return ApiResult.ERROR(Result.INVALID_PARAMETERS);
		}
		return ApiResult.SUCCESS(coCompanyService.delByPrimaryKey(id));
	}
}
