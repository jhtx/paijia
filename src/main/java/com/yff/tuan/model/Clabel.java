package com.yff.tuan.model;

import java.util.List;

public class Clabel {
    private Integer id;

    private Integer userId;

    private String name;
    
    private List<ClabelType> cAdd;
    private List<ClabelType> cUpdate;
    List<ClabelType> clabelTypes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public List<ClabelType> getcAdd() {
		return cAdd;
	}

	public List<ClabelType> getcUpdate() {
		return cUpdate;
	}

	public List<ClabelType> getClabelTypes() {
		return clabelTypes;
	}

	public void setcAdd(List<ClabelType> cAdd) {
		this.cAdd = cAdd;
	}

	public void setcUpdate(List<ClabelType> cUpdate) {
		this.cUpdate = cUpdate;
	}

	public void setClabelTypes(List<ClabelType> clabelTypes) {
		this.clabelTypes = clabelTypes;
	}
    
    
}