package com.yff.tuan.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {
	private static Long DAY_TIMES = 24 * 60 * 60 * 1000l;
	private static Long HOUR_TIMES = 60 * 60 * 1000l;
	private static Long MIN_TIMES = 60 * 1000l;
	private static Long SECOND_TIMES = 1000l;

	public static String FORMAT_YYMMDD = "yyyy-MM-dd";
	public static String FORMAT_YYMMDDHHMMSS = "yyyy-MM-dd HH:mm:ss";
	public static String FORMAT_YYMMDD000000 = "yyyy-MM-dd 00:00:00";
	public static String FORMAT_YYMMDDHHMMSSSSS = "yyyy-MM-dd HH:mm:ss.SSS";
	private static final DateFormat[] ACCEPT_DATE_FORMATS = {
			new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss"),
			new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"),
			new SimpleDateFormat("yyyy-MM-dd"),
			new SimpleDateFormat("yyyy/MM/dd"),
			new SimpleDateFormat("yyyy.MM.dd"),
			new SimpleDateFormat("yyyyMMdd"),
			new SimpleDateFormat("dd/MM/yyyy"),
	// new SimpleDateFormat("yyyy-MM-ddTHH:mm:ss+0800")
	};// 支持转换的日期格�?
	
	
	public static Date getDate(String dateStr) {
		if (dateStr == null || "".equals(dateStr))
			return null;
		for (DateFormat format : ACCEPT_DATE_FORMATS) {
			try {
				return format.parse(dateStr);
			} catch (Exception e) {
				continue;
			}
		}
		return null;
	}
	
	public static String getMillisTime(long millis){
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date(millis));
	}
	
	public static Date getDate(String dateStr, String format) {
		Date d = null;
		try {
			format = StringUtil.isEmpty(format) ? FORMAT_YYMMDD : format;
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			d = sdf.parse(dateStr);
		} catch (Exception e) {
		}
		return d;
	}

	public static Date getDate(Date cdate, String strFormat) {
		String date = getFormatDate(cdate, strFormat);
		return getDate(date, "yyyy-MM-dd HH:mm:ss");
	}

	public static String getFormatDate(Date d, String format) {
		format = StringUtil.isEmpty(format) ? FORMAT_YYMMDD : format;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return (d == null) ? null : sdf.format(d);
	}

	public static Date get00HourDate(Date d) {
		return d != null ? DateUtil.getDate(
				DateUtil.getFormatDate(d, "yyyy-MM-dd 00:00:00"),
				FORMAT_YYMMDDHHMMSS) : null;
	}
	
	public static String get00HourFormatDate(Date d) {
		return d != null ? DateUtil.getFormatDate(d, "yyyy-MM-dd 00:00:00") : null;
	}
	
	public static Date get00MinuteDate(Date d) {
		return d != null ? DateUtil.getDate(
				DateUtil.getFormatDate(d, "yyyy-MM-dd HH:00:00"),
				FORMAT_YYMMDDHHMMSS) : null;
	}

	public static int getWeek(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.WEEK_OF_YEAR);
	}

	public static Date absoluteDate(Date date, int day) {
		if (date == null) {
			return new Date();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_YEAR, day);
		return cal.getTime();
	}

	public static Date absoluteHour(Date date, int hour) {
		if (date == null) {
			return new Date();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR, hour);
		return cal.getTime();
	}

	public static Date absoluteMonth(Date date, int Month) {
		if (date == null) {
			return new Date();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, Month);
		return cal.getTime();

	}

	public static Date absoluteYear(Date date, int Year) {
		if (date == null) {
			return new Date();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, Year);
		return cal.getTime();

	}

	public static int getSeansonNum(Date date) {
		int month = Integer.valueOf(getFormatDate(date, "MM"));
		int[] seansonArr1 = { 1, 2, 3 };
		int[] seansonArr2 = { 4, 5, 6 };
		int[] seansonArr3 = { 7, 8, 9 };
		int[] seansonArr4 = { 10, 11, 12 };
		for (int i = 0; i < seansonArr1.length; i++) {
			if (seansonArr1[i] == month) {
				return 1;
			}
		}
		for (int i = 0; i < seansonArr2.length; i++) {
			if (seansonArr2[i] == month) {
				return 2;
			}
		}
		for (int i = 0; i < seansonArr3.length; i++) {
			if (seansonArr3[i] == month) {
				return 3;
			}
		}
		for (int i = 0; i < seansonArr4.length; i++) {
			if (seansonArr4[i] == month) {
				return 4;
			}
		}
		return 0;
	}

	public static Date absoluteWeek(Date date, int Week) {
		if (date == null) {
			return new Date();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.WEEK_OF_YEAR, Week);
		return cal.getTime();

	}

	public static boolean beforeDate(Date currentDate, Date comDate) {
		if (comDate == null || currentDate == null) {
			return false;
		} else {
			return currentDate.before(comDate);
		}
	}

	public static Date get24HourDate(Date d) {
		return d != null ? DateUtil.getDate(
				DateUtil.getFormatDate(d, "yyyy-MM-dd 23:59:59.999"),
				FORMAT_YYMMDDHHMMSSSSS) : null;
	}
	public static String get24HourFormatDate(Date d) {
		return d != null ? DateUtil.getFormatDate(d, "yyyy-MM-dd 23:59:59.999") : null;
	}

	public static Long getNumbersByTwoDate(Date bDate, Date eDate,
			Long parameter) {
		parameter = StringUtil.isEmpty(parameter) ? DAY_TIMES : parameter;
		return (eDate.getTime() - bDate.getTime()) / parameter;
	}

	public static Long getDayBytwoDate(Date bDate, Date eDate) {
		return getNumbersByTwoDate(bDate, eDate, DAY_TIMES);
	}

	public static Long getHourByTwoDate(Date bDate, Date eDate) {
		return getNumbersByTwoDate(bDate, eDate, HOUR_TIMES);
	}

	public static Long getMinByTwoDate(Date bDate, Date eDate) {
		return getNumbersByTwoDate(bDate, eDate, MIN_TIMES);
	}

	public static Long getSecondByTwoDate(Date bDate, Date eDate) {
		return getNumbersByTwoDate(bDate, eDate, SECOND_TIMES);
	}

	public static Date getFristDayOfWeek() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_WEEK, 1);
		return c.getTime();
	}

	public static Date getFistDayOfMonth() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 1);
		return c.getTime();
	}
	public static Date getFistDayOfLastMonth() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -1);
		c.set(Calendar.DAY_OF_MONTH, 1);
		return c.getTime();
	}

	public static Date getFistDayOfYear() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_YEAR, 1);
		return c.getTime();
	}

	public static Date getAddNumbersDay(Date date, int i, Long parameter) {
		if (date == null) {
			return null;
		} else {
			parameter = StringUtil.isEmpty(parameter) ? DAY_TIMES : parameter;
			date.setTime(date.getTime() + i * parameter);
			return date;
		}
	}

	public static Date getAddDayByDay(Date date, int i) {
		return getAddNumbersDay(date, i, DAY_TIMES);
	}

	public static Date getAddHourByDate(Date date, int i) {
		return getAddNumbersDay(date, i, HOUR_TIMES);
	}

	public static Date getAddMinByDate(Date date, int i) {
		return getAddNumbersDay(date, i, MIN_TIMES);
	}

	public static String timeStampString() {
		return DateUtil.getFormatDate(new Date(), "yyyyMMddHHmmss");
	}

	public static int getMinute() {
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.MINUTE);
	}
	
	/** 
    * 判断给定日期是否为月末的�?�? 
    * 
    * @param date 
    * @return true:是|false:不是 
    */ 
    public static boolean isLastDayOfMonth(Date date) { 
        Calendar calendar = Calendar.getInstance(); 
        calendar.setTime(date); 
        calendar.set(Calendar.DATE, (calendar.get(Calendar.DATE) + 1)); 
        if (calendar.get(Calendar.DAY_OF_MONTH) == 1) { 
            return true; 
        } 
        return false; 
    }
    /**  
     * 计算两个日期之间相差的天�?  
     * @param smdate 较小的时�? 
     * @param bdate  较大的时�? 
     * @return 相差天数 
     * @throws ParseException  
     */    
    public static int daysBetween(String beginTime,String endTime){  
    	try {
    		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
    		Date smdate=sdf.parse(sdf.format(sdf.parse(beginTime)));
    		Date bdate=sdf.parse(sdf.format(sdf.parse(endTime)));  
            Calendar cal = Calendar.getInstance();    
            cal.setTime(smdate);    
            long time1 = cal.getTimeInMillis();                 
            cal.setTime(bdate);    
            long time2 = cal.getTimeInMillis();         
            long between_days=(time2-time1)/(1000*3600*24); 
            return Integer.parseInt(String.valueOf(between_days));
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return 0;
    }  
    
    public static Date getEndDate(Date bedate) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(bedate);
		calendar.add(Calendar.DATE, 1);// 把日期往后增加一�?.整数�?后推,负数�?前移�?
		return calendar.getTime();
	}
    
	public static void main(String[] args) {

	}

}


