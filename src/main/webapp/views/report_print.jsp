<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (int)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>卡客途安轮卫管家</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<%=path %>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/font-awesome.css" rel="stylesheet">
<link href="<%=path %>/css/google.css" rel="stylesheet">
<link href="<%=path %>/css/style.css" rel="stylesheet" type="text/css">
<link href="<%=path %>/css/pages/signin.css" rel="stylesheet" type="text/css">
<link href="<%=path %>/css/reveal.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="76x76" href="<%=path %>/img/logo.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=path %>/img/logo.png">
<style type="text/css">
html,body{
min-height:100%;
}
</style>
</head>
<body style="margin:0 auto;height: 100%" id="body">
<!-- main -->
<div class="main" id="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span12">
          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>车辆日检调查</h3>
              <button type="submit" class="btn btn-primary" id="go_back" style="float: right;margin-top:6px;margin-right:5px;">返回</button>
              <button type="submit" class="btn btn-primary" onclick="jQuery.print('#ele3')" style="float: right;margin-top:6px;margin-right:5px;">打印</button>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th width="15%">车牌号</th>
                    <th width="10%">姓名</th>
                    <th width="10%">手机号</th>
                    <th width="10%">一级标签</th>
                    <th width="10%">二级标签</th>
                    <th width="40%">内容</th>
                  </tr>
                </thead>
                <tbody id="tb">
                	<c:forEach items="${reports }" var="r" varStatus="status">
                	<tr>
	                    <td>${r.licenseNo }</<td>
	                    <td>${r.userName }</<td>
	                    <td>${r.userPhone }</<td>
	                    <td>${r.labelName }</<td>
	                    <td>${r.labelTypeName }</<td>
	                    <td>${r.content }</<td>
                   </tr>
                   </c:forEach>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <div id="myModal" class="reveal-modal">
			<img width="400px;" height="300px" id="mg" src="data:image/jpeg;base64,${report.filePath }">
			<a class="close-reveal-modal">&#215;</a>
	</div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
  <br>
  <br>
  <br>
</div>
<!-- /main -->
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<%=path %>/js/jquery-1.7.2.min.js"></script> 
<script src="<%=path %>/js/excanvas.min.js"></script> 
<script src="<%=path %>/js/chart.min.js" type="text/javascript"></script> 
<script src="<%=path %>/js/bootstrap.js"></script>
<script src="<%=path %>/js/base.js"></script> 
<script src="<%=path %>/js/jquery.reveal.js"></script>
<script type="text/javascript">
var minheight = $(window).height();
$("#body").css({
  'min-height':minheight +'px'
})
$("#go_back").click(function(){
	window.location.href="<%=path %>/data/report";
});
$("#print").click(function(){
        //var a = $('a')[0];
        var a = $("<a href='<%=path %>/data/report/print' target='_blank'>打印</a>").get(0);
        var e = document.createEvent('MouseEvents');
        e.initEvent( 'click', true, true );
        a.dispatchEvent(e);
});
function show(id){
	$("#mg").attr('src',"data:image/jpeg;base64,"+$("#fp"+id).val()); 
}
</script>
</body>
</html>