package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.CtypeSizeMapper;
import com.yff.tuan.mapper.CtypeSizeMapper;
import com.yff.tuan.model.CtypeSize;
import com.yff.tuan.model.CtypeSizeExample;
import com.yff.tuan.model.CtypeSizeExample.Criteria;
import com.yff.tuan.service.TypeSizeService;
import com.yff.tuan.util.Page;

@Component
public class TypeSizeServiceImpl implements TypeSizeService {
	@Autowired
	CtypeSizeMapper ctypeSizeMapper;
	
	@Override
	public CtypeSize find(CtypeSize ctypeSize) {
		CtypeSizeExample example = new CtypeSizeExample();
		Criteria createCriteria = example.createCriteria();
		//createCriteria.andNameEqualTo(ctypeSize.getName());
		List<CtypeSize> ctypeSizes = ctypeSizeMapper.selectByExample(example);
		return ctypeSizes.isEmpty()?null:ctypeSizes.get(0);
	}

	@Override
	public CtypeSize findByPrimaryKey(Integer id) {
		return ctypeSizeMapper.selectByPrimaryKey(id);
	}

	@Override
	public int insert(CtypeSize ctypeSize) {
		return ctypeSizeMapper.insert(ctypeSize);
	}

	@Override
	public int updateByPrimaryKeySelective(CtypeSize ctypeSize) {
		return ctypeSizeMapper.updateByPrimaryKeySelective(ctypeSize);
	}

	@Override
	public List<CtypeSize> query(CtypeSizeExample example) {
		return ctypeSizeMapper.selectByExample(example);
	}

	@Override
	public Page<CtypeSize> queryByPage(CtypeSizeExample example, Integer pageNum) {
		return null;
	}

	@Override
	public int delByPrimaryKey(Integer id) {
		return ctypeSizeMapper.deleteByPrimaryKey(id);
	}
}
