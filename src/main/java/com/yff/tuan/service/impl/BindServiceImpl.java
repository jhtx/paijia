package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.CarBindMapper;
import com.yff.tuan.mapper.CarBindTyreMapper;
import com.yff.tuan.mapper.CarBrandMapper;
import com.yff.tuan.mapper.CarFiTyreMapper;
import com.yff.tuan.mapper.CarMapper;
import com.yff.tuan.mapper.CarModelMapper;
import com.yff.tuan.mapper.CarSeTyreMapper;
import com.yff.tuan.mapper.CarThTyreMapper;
import com.yff.tuan.mapper.CarTypeSizeMapper;
import com.yff.tuan.mapper.TireCheckMapper;
import com.yff.tuan.mapper.VehicleCheckMapper;
import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarBind;
import com.yff.tuan.model.CarBindExample;
import com.yff.tuan.model.CarBindTyre;
import com.yff.tuan.model.CarBindTyreExample;
import com.yff.tuan.model.CarBrand;
import com.yff.tuan.model.CarExample;
import com.yff.tuan.model.CarExample.Criteria;
import com.yff.tuan.model.CarFiTyre;
import com.yff.tuan.model.CarModel;
import com.yff.tuan.model.CarSeTyre;
import com.yff.tuan.model.CarThTyre;
import com.yff.tuan.model.CarTypeSize;
import com.yff.tuan.model.TireCheck;
import com.yff.tuan.model.TireCheckExample;
import com.yff.tuan.model.VehicleCheck;
import com.yff.tuan.model.VehicleCheckExample;
import com.yff.tuan.service.BindService;
import com.yff.tuan.service.CarService;
import com.yff.tuan.util.Page;

@Component
public class BindServiceImpl implements BindService {
	@Autowired CarBindMapper carBindMapper;
	@Autowired CarBindTyreMapper carBindTyreMapper;
	@Autowired VehicleCheckMapper vehicleCheckMapper;
	@Autowired TireCheckMapper tireCheckMapper;
	
	@Override
	public List<CarBind> queryCarBind(CarBindExample example) {
		return carBindMapper.selectByExample(example);
	}
	@Override
	public List<CarBindTyre> queryCarBindTyre(CarBindTyreExample example) {
		return carBindTyreMapper.selectByExample(example);
	}
	@Override
	public boolean insert(CarBind carBind) {
		CarBindExample example = new CarBindExample();
		example.createCriteria().andCarIdEqualTo(carBind.getCarId());
		carBindMapper.deleteByExample(example);
		int i = carBindMapper.insert(carBind);
		if(null != carBind.getCarBindTyres()){
			insert(carBind.getCarBindTyres(), carBind.getCarId());
		}
		return i>0?true:false;
	}
	@Override
	public boolean insert(List<CarBindTyre> carBindTyres,int carId) {
		CarBindTyreExample example = new CarBindTyreExample();
		example.createCriteria().andCarIdEqualTo(carId);
		carBindTyreMapper.deleteByExample(example);
		int i = 0;
		for(CarBindTyre carBindTyre:carBindTyres){
			i+=carBindTyreMapper.insert(carBindTyre);
		}
		return i>0?true:false;
	}
	@Override
	public int updateCarBind(CarBind carBind) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int updateCarBindTyre(List<CarBindTyre> carBindTyres) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public CarBind findCarBind(CarBindExample example) {
		List<CarBind> carBinds = carBindMapper.selectByExample(example);
		if(!carBinds.isEmpty()){
			return carBinds.get(0);
		}
		return null;
	}
	@Override
	public int countCarBind(CarBindExample example) {
		return carBindMapper.countByExample(example);
	}
	@Override
	public List<VehicleCheck> queryVehicleCheck(String productId) {
		VehicleCheckExample example = new VehicleCheckExample();
		example.createCriteria().andProductIdEqualTo(productId);
		example.setOrderByClause(" id desc limit 0,100 ");
		return vehicleCheckMapper.selectByExample(example);
	}
	@Override
	public List<TireCheck> queryTireCheck(String productId) {
		TireCheckExample example = new TireCheckExample();
		example.createCriteria().andProductIdEqualTo(productId);
		example.setOrderByClause(" id desc  limit 0,100 ");
		return tireCheckMapper.selectByExample(example);
	}
	@Override
	public TireCheck findTireCheck(String productId) {
		TireCheckExample example = new TireCheckExample();
		example.createCriteria().andProductIdEqualTo(productId);
		example.setOrderByClause(" id desc  limit 1 ");
		List<TireCheck> list = tireCheckMapper.selectByExample(example);
		if(null != list && !list.isEmpty()){
			return list.get(0);
		}
		return null;
	}
	@Override
	public VehicleCheck findVehicleCheck(String productId) {
		VehicleCheckExample example = new VehicleCheckExample();
		example.createCriteria().andProductIdEqualTo(productId);
		example.setOrderByClause(" id desc limit 1 ");
		List<VehicleCheck> list = vehicleCheckMapper.selectByExample(example);
		if(null != list && !list.isEmpty()){
			return list.get(0);
		}
		return null;
	}
	@Override
	public List<VehicleCheck> findVehicleCheckInFence(String productId) {
		/*VehicleCheckExample example = new VehicleCheckExample();
		example.createCriteria().andProductIdEqualTo(productId);
		example.createCriteria().andStateGreaterThan(0);
		example.setOrderByClause(" id desc limit 1 ");*/
		return vehicleCheckMapper.selectByProductId(productId);
	}
	
	
}
